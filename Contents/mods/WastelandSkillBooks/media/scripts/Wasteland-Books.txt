module WastelandBooks {

	imports {
		Base
	}

    item WastelandStrength1
    {
        DisplayCategory     =   SkillBook,
        NumberOfPages       =   220,
        Weight              =   0.8,
        Type                =   Literature,
        DisplayName         =   Strength Vol. 1,
        Icon                =   StrengthSkillBook,
        SkillTrained        =   Strength,
        LvlSkillTrained     =   1,
        NumLevelsTrained    =   2,
        StaticModel         =   Book,
        WorldStaticModel    =   BookGrey_Ground,
    }

    item WastelandStrength2
    {
        DisplayCategory     =   SkillBook,
        NumberOfPages       =   260,
        Weight              =   0.8,
        Type                =   Literature,
        DisplayName         =   Strength Vol. 2,
        Icon                =   StrengthSkillBook,
        SkillTrained        =   Strength,
        LvlSkillTrained     =   3,
        NumLevelsTrained    =   2,
        StaticModel         =   Book,
        WorldStaticModel    =   BookGrey_Ground,
    }

    item WastelandStrength3
    {
        DisplayCategory     =   SkillBook,
        NumberOfPages       =   300,
        Weight              =   0.8,
        Type                =   Literature,
        DisplayName         =   Strength Vol. 3,
        Icon                =   StrengthSkillBook,
        SkillTrained        =   Strength,
        LvlSkillTrained     =   5,
        NumLevelsTrained    =   2,
        StaticModel         =   Book,
        WorldStaticModel    =   BookGrey_Ground,
    }

    item WastelandStrength4
    {
        DisplayCategory     =   SkillBook,
        NumberOfPages       =   340,
        Weight              =   0.8,
        Type                =   Literature,
        DisplayName         =   Strength Vol. 4,
        Icon                =   StrengthSkillBook,
        SkillTrained        =   Strength,
        LvlSkillTrained     =   7,
        NumLevelsTrained    =   2,
        StaticModel         =   Book,
        WorldStaticModel    =   BookGrey_Ground,
    }

    item WastelandStrength5
    {
        DisplayCategory     =   SkillBook,
        NumberOfPages       =   380,
        Weight              =   0.8,
        Type                =   Literature,
        DisplayName         =   Strength Vol. 5,
        Icon                =   StrengthSkillBook,
        SkillTrained        =   Strength,
        LvlSkillTrained     =   9,
        NumLevelsTrained    =   2,
        StaticModel         =   Book,
        WorldStaticModel    =   BookGrey_Ground,
    }
    
    item WastelandFitness1
    {
        DisplayCategory     =   SkillBook,
        NumberOfPages       =   220,
        Weight              =   0.8,
        Type                =   Literature,
        DisplayName         =   Fitness Vol. 1,
        Icon                =   FitnessSkillBook,
        SkillTrained        =   Fitness,
        LvlSkillTrained     =   1,
        NumLevelsTrained    =   2,
        StaticModel         =   Book,
        WorldStaticModel    =   BookGrey_Ground,
    }

    item WastelandFitness2
    {
        DisplayCategory     =   SkillBook,
        NumberOfPages       =   260,
        Weight              =   0.8,
        Type                =   Literature,
        DisplayName         =   Fitness Vol. 2,
        Icon                =   FitnessSkillBook,
        SkillTrained        =   Fitness,
        LvlSkillTrained     =   3,
        NumLevelsTrained    =   2,
        StaticModel         =   Book,
        WorldStaticModel    =   BookGrey_Ground,
    }

    item WastelandFitness3
    {
        DisplayCategory     =   SkillBook,
        NumberOfPages       =   300,
        Weight              =   0.8,
        Type                =   Literature,
        DisplayName         =   Fitness Vol. 3,
        Icon                =   FitnessSkillBook,
        SkillTrained        =   Fitness,
        LvlSkillTrained     =   5,
        NumLevelsTrained    =   2,
        StaticModel         =   Book,
        WorldStaticModel    =   BookGrey_Ground,
    }

    item WastelandFitness4
    {
        DisplayCategory     =   SkillBook,
        NumberOfPages       =   340,
        Weight              =   0.8,
        Type                =   Literature,
        DisplayName         =   Fitness Vol. 4,
        Icon                =   FitnessSkillBook,
        SkillTrained        =   Fitness,
        LvlSkillTrained     =   7,
        NumLevelsTrained    =   2,
        StaticModel         =   Book,
        WorldStaticModel    =   BookGrey_Ground,
    }

    item WastelandFitness5
    {
        DisplayCategory     =   SkillBook,
        NumberOfPages       =   380,
        Weight              =   0.8,
        Type                =   Literature,
        DisplayName         =   Fitness Vol. 5,
        Icon                =   FitnessSkillBook,
        SkillTrained        =   Fitness,
        LvlSkillTrained     =   9,
        NumLevelsTrained    =   2,
        StaticModel         =   Book,
        WorldStaticModel    =   BookGrey_Ground,
    }
}
