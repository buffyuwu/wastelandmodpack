require "ISUI/ISInventoryPane"

local function doRead(items, playerObj)
    for _, item in ipairs(items) do
        local putBack = false
        local putBackLocation = nil
        local playerInv = playerObj:getInventory()
        if luautils.haveToBeTransfered(playerObj, item) then
            putBack = true
            putBackLocation = item:getContainer()
            ISTimedActionQueue.add(ISInventoryTransferAction:new(playerObj, item, putBackLocation, playerInv))
        end
        ISTimedActionQueue.add(ISReadABook:new(playerObj, item, 150))
        if putBack then
            ISTimedActionQueue.add(ISInventoryTransferAction:new(playerObj, item, playerInv, putBackLocation))
        end
    end
end

local function OnFillInventoryObjectContextMenu(player, context, items)
    local actualItems = ISInventoryPane.getActualItems(items)
    for _, item in ipairs(actualItems) do
        if item:getCategory() ~= "Literature" or item:canBeWrite() then
            return
        end
    end
    local playerObj = getSpecificPlayer(player)
    local readBooks = playerObj:getAlreadyReadBook()
    local items = {}
	for _,item in ipairs(actualItems) do
        local book = SkillBook[item:getSkillTrained()]
        local isIlliterate = playerObj:getTraits():isIlliterate()
        local cantRead = item:getLvlSkillTrained() ~= -1 and book.perk and item:getLvlSkillTrained() > playerObj:getPerkLevel(book.perk) + 1
        local isAlreadyRead = readBooks:contains(item:getFullType())
		if not isIlliterate and not cantRead and not isAlreadyRead then
            table.insert(items, item)
        end
    end
    if #items > 1 then
        context:addOption(getText("ContextMenu_ReadAllUnread"), items, doRead, playerObj)
    end
end

Events.OnFillInventoryObjectContextMenu.Add(OnFillInventoryObjectContextMenu)