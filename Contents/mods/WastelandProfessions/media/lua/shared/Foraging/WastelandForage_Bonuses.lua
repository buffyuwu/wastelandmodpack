require 'Foraging/forageSystem'

Events.preAddSkillDefs.Add(function()

	local tailor = {
		name                    = "tailor",
		type                    = "occupation",
		visionBonus             = 0,
		weatherEffect           = 0,
		darknessEffect          = 0,
		specialisations         = {
			["Clothing"]       		= 50,
		},
	}
	forageSystem.addSkillDef(tailor, false)

	local homeless = {
		name                    = "homeless",
		type                    = "occupation",
		visionBonus             = 2,
		weatherEffect           = 20,
		darknessEffect          = 0,
		specialisations         = {
			["Animals"]             = 10,
			["Medical"]             = 5,
			["Trash"]               = 35,
			["Junk"]                = 20,
			["JunkFood"]            = 35,
			["JunkWeapons"]         = 5,
		},
	}
	forageSystem.addSkillDef(homeless, false)

	local racer = {
		name                    = "racer",
		type                    = "occupation",
		visionBonus             = 0,
		weatherEffect           = 0,
		darknessEffect          = 0,
		specialisations         = {
			["Trash"]               = 5,
			["Junk"]                = 20,
			["JunkWeapons"]         = 5,
		},
	}
	forageSystem.addSkillDef(racer, false)

	local librarian = {
		name                    = "librarian",
		type                    = "occupation",
		visionBonus             = 0,
		weatherEffect           = 0,
		darknessEffect          = 0,
		specialisations         = {
			["Berries"]             = 5,
			["Mushrooms"]           = 5,
			["ForestRarities"]      = 5,
		},
	}
	forageSystem.addSkillDef(librarian, false)

end)