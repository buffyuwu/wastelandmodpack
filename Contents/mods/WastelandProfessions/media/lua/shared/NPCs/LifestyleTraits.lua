--------------------------------------------------------------------------------------------------
--		----	  |			  |			|		 |				|    --    |      ----			--
--		----	  |			  |			|		 |				|    --	   |      ----			--
--		----	  |		-------	   -----|	 ---------		-----          -      ----	   -------
--		----	  |			---			|		 -----		------        --      ----			--
--		----	  |			---			|		 -----		-------	 	 ---      ----			--
--		----	  |		-------	   ----------	 -----		-------		 ---      ----	   -------
--			|	  |		-------			|		 -----		-------		 ---		  |			--
--			|	  |		-------			|	 	 -----		-------		 ---		  |			--
--------------------------------------------------------------------------------------------------

if not getActivatedMods():contains("Lifestyle") then return end

require('NPCs/MainCreationMethods');

local function onGameBoot()

		-- DISCIPLINED
		local disciplined = TraitFactory.addTrait("Disciplined", getText("UI_trait_disciplined"), 8, getText("UI_trait_disciplineddesc"), false, false);
		disciplined:addXPBoost(Perks.Strength, 1)
		disciplined:addXPBoost(Perks.Fitness, 1)
		disciplined:addXPBoost(Perks.Meditation, 4)
		disciplined:addXPBoost(Perks.Nimble, 1)
		
		-- COUCH POTATO
		local couchpotato = TraitFactory.addTrait("CouchPotato", getText("UI_trait_couchpotato"), -4, getText("UI_trait_couchpotatodesc"), false, false);
		couchpotato:addXPBoost(Perks.Strength, -1)
		couchpotato:addXPBoost(Perks.Fitness, -1)

		-- VIRTUOSO
		local virtuoso = TraitFactory.addTrait("Virtuoso", getText("UI_trait_virtuoso"), 4, getText("UI_trait_virtuosodesc"), false, false);
		virtuoso:addXPBoost(Perks.Music, 2)
		
		
		-- TONE DEAF
		local tonedeaf = TraitFactory.addTrait("ToneDeaf", getText("UI_trait_tonedeaf"), -1, getText("UI_trait_tonedeafdesc"), false, false);


		-- MUTUAL EXCLUSIVES
		
		TraitFactory.setMutualExclusive("CouchPotato", "Disciplined");
		TraitFactory.setMutualExclusive("CouchPotato", "Athletic");
		TraitFactory.setMutualExclusive("CouchPotato", "Fit");
		TraitFactory.setMutualExclusive("CouchPotato", "Gymnast");
		TraitFactory.setMutualExclusive("CouchPotato", "Jogger");
		TraitFactory.setMutualExclusive("Disciplined", "Overweight");
		TraitFactory.setMutualExclusive("Disciplined", "Obese");
		TraitFactory.setMutualExclusive("Disciplined", "Out of Shape");
		TraitFactory.setMutualExclusive("Disciplined", "Unfit");
		TraitFactory.setMutualExclusive("Virtuoso", "HardOfHearing");
		TraitFactory.setMutualExclusive("Virtuoso", "Deaf");
		TraitFactory.setMutualExclusive("Virtuoso", "ToneDeaf");
		TraitFactory.setMutualExclusive("ToneDeaf", "Deaf");
		
	end

Events.OnGameBoot.Add(onGameBoot);