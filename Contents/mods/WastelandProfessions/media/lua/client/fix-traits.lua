local function fixTraits(playerIdx, player)
    local traits = player:getTraits();
    local toRemove = {}
    local toAdd = {}
    for i=0,traits:size()-1 do
        local trait = traits:get(i)
        if trait:sub(-1) == "2" then
            table.insert(toRemove, trait)
            print("Removing incorrect trait: " .. trait)
            if not traits:contains(trait:sub(1,-2)) then
                table.insert(toAdd, trait:sub(1,-2))
                print("Adding correct trait: " .. trait:sub(1,-2))
            end
        end
    end
    for _,trait in ipairs(toRemove) do
        traits:remove(trait)
    end
    for _,trait in ipairs(toAdd) do
        traits:add(trait)
    end
end

Events.OnCreatePlayer.Add(fixTraits)
