local unintelligibleStr = "<unintelligible>"

local function shouldMangle(player)
    return player ~= getPlayer() and player:HasTrait("SpeechImpediment")
end

-- replace random words or groups of words with <unintelligible>
local function manglePhrase(message)
    local originalWords = {}
    local finalWords = {}
    local wasLastReplaced = false
    for word in string.gmatch(message, "%S+") do
        table.insert(originalWords, word)
    end
    for _, word in ipairs(originalWords) do
        if ZombRand(100) < 7 then -- 5% chance to replace a word
            if not wasLastReplaced then
                table.insert(finalWords, unintelligibleStr)
                wasLastReplaced = true
            end
        else
            table.insert(finalWords, word)
            wasLastReplaced = false
        end
    end

    message = table.concat(finalWords, " ")
    return message
end

local function mangleMessage(message)
    local chunks = {}
    for chunk in string.gmatch(message, "([^\"]+)") do
        table.insert(chunks, chunk)
    end
    for i, chunk in ipairs(chunks) do
        if i % 2 == 0 then
            chunks[i] = manglePhrase(chunk)
        end
    end
    return table.concat(chunks, "\"")
end

local original_ISChat_addLineInChat = ISChat.addLineInChat;
ISChat.addLineInChat = function(message, tabID)
    local line = message:getTextWithPrefix()
    local player = getPlayerFromUsername(message:getAuthor())

    if not player or
       message:isServerAlert()
       or string.match(line, "%[ASL%]")
       or string.match(line, "IMAGE%:")
       or string.match(line, "Event%) %*%*")
       or not shouldMangle(player)
    then
        original_ISChat_addLineInChat(message, tabID)
        return
    end
    message:setText(mangleMessage(message:getText()))
    original_ISChat_addLineInChat(message, tabID)
end