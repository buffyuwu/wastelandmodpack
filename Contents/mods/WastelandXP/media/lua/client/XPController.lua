---
--- XPController.lua
--- Manages XP gains on Wasteland RP server
--- By Zoomies
--- 10/10/2022
---

WastelandXP_SkillBonus = {
	Agility = 0.5,
	Aiming = 0.0,
	Axe = 0.5,
	Blacksmith = 0.5,
	Blunt = 0.5,
	Brewing = 0.5,
	Combat = 0.5,
	Cooking = 1.0,
	Cultivation = 0.5,
	Doctor = 1.0,
	Electricity = 1.0,
	Farming = 1.0,
	Firearm = 0.5,
	Fishing = 0.5,
	Fitness = 0.5,
	Lifestyle = 0.5,
	Lightfoot = 0.5,
	LongBlade = 0.5,
	Maintenance = 1.0,
	Mechanics = 1.0,
	Meditation = 0.5,
	Melting = 0.5,
	MetalWelding = 1.0,
	Miscellaneous = 0.5,
	Music = 0.5,
	Nimble = 1.0,
	PlantScavenging = 0.5,
	PseudonymousEdPiano = 1.0,
	Reloading = 0.0,
	SmallBlade = 0.5,
	SmallBlunt = 0.5,
	Sneak = 0.5,
	Spear = 0.5,
	Sprinting = 0.5,
	Strength = 0.5,
	Survivalist = 0.5,
	Tailoring = 0.5,
	Trapping = 0.5,
	WineMaking = 0.5,
	Woodwork = 0.5,
}

WastelandXP_CappedPerks = {
    Aiming = true,
    Brewing = true,
    Cooking = true,
    Doctor = true,
    Electricity = true,
    Farming = true,
    Fishing = true,
    Mechanics = true,
    MetalWelding = true,
    PlantScavenging = true,
    Reloading = true,
    Tailoring = true,
    Trapping = true,
    Woodwork = true,
}

---@param character IsoGameCharacter
---@param perk PerkFactory.Perk
---@param xp Float
function giveBonusAimAndReloadXp(character, perk, xp)
    local perkId = perk:getId()
    if(perkId == "Aiming" or perkId == "Reloading") then
        local extraXP = xp * 0.5 -- Adds 50% more, bringing us to 0.75 if server is set to 0.5 XP gain
        local baseXP = extraXP + xp
        local bonusXP = 0
        local perkLevel = character:getPerkLevel(perk)
        local perkBoost = character:getXp():getPerkBoost(perk)
        if(perkId == "Aiming" and perkLevel > 4 and perkBoost ~= 0) then
            bonusXP = (0.21 * perkBoost) * (baseXP / 0.37)
        end

        if(perkId == "Reloading" and perkLevel > 4 and perkBoost ~= 0) then
            bonusXP = (0.25 * perkBoost) * (baseXP / 0.25)
        end

        character:getXp():AddXP(perk, bonusXP + extraXP, false, false, false)
    end
end

---@param character IsoGameCharacter
---@param perk PerkFactory.Perk
---@return number|nil
function getLevelCap(character, perk)
    local perkId = perk:getId()
    if not WastelandXP_CappedPerks[perkId] then
        return nil
    end
    local perkBoost = character:getXp():getPerkBoost(perk)
    if perkId == "Aiming" or perkId == "Reloading" then
        if perkBoost == 0 then
            return 6
        end
    end
    return 4 + (2 * math.min(3, perkBoost))
end

---@param character IsoGameCharacter
---@param perk PerkFactory.Perk
---@return boolean true if capped or false otherwise
function doLevelCapping(character, perk)
    local cap = getLevelCap(character, perk)
    if not cap then
        return false
    end
    local perkLevel = character:getPerkLevel(perk)
    if perkLevel >= cap then  -- If we have hit the cap, set XP to the cap level exactly
        character:getXp():setXPToLevel(perk, cap)
        return true
    end
    return false
end

function giveBonusXP(character, perk, xp)
    local bonusMultiplier = WastelandXP_SkillBonus[perk:getId()]
    if(bonusMultiplier) then
        local bonusXP = xp * bonusMultiplier
        character:getXp():AddXP(perk, bonusXP, false, false, false)
    end
end

---@param character IsoGameCharacter
---@param perk PerkFactory.Perk
---@param xp Float
function onXP(character, perk, xp)

    if(perk:getId() == "Strength" or perk:getId() == "Fitness") then
        return -- We never do anything with these skills and they spam too much
    end

    local capped = doLevelCapping(character, perk)

    if(not capped) then
        giveBonusAimAndReloadXp(character, perk, xp)
        giveBonusXP(character, perk, xp)
    end
end

Events.AddXP.Add(onXP)