local ToDisable = {};

local function doDisable()
    for i=0, getAllRecipes():size()-1 do
        local recipe = getAllRecipes():get(i);
        if recipe ~= nil then
            for _,v in pairs(ToDisable) do
                if recipe:getName() == v then
                    recipe:setNeedToBeLearn(true);
                    recipe:setIsHidden(true);
                    print("Disabling Recipe: " .. v);
                end
            end
        end
    end
end

local function DisableRecipe(recipe)
    table.insert(ToDisable, recipe);
end
Events.OnGameBoot.Add(doDisable);

DisableRecipe("Grind Meat")