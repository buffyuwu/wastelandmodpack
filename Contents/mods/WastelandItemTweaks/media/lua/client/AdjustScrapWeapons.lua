require("ItemTweaker_Core");

if getActivatedMods():contains("ScrapWeapons(new version)") then
    TweakItem("SWeapons.ScrapPickaxe", "ConditionLowerChanceOneIn", 10)
    TweakItem("SWeapons.ScrapPickaxe", "ConditionMax", 8)
    TweakItem("SWeapons.ScrapPickaxe", "MinDamage", 0.8)
    TweakItem("SWeapons.ScrapPickaxe", "MaxDamage", 1.8)

    TweakItem("SWeapons.BigScrapPickaxe", "ConditionLowerChanceOneIn", 15)
    TweakItem("SWeapons.BigScrapPickaxe", "ConditionMax", 10)
    TweakItem("SWeapons.BigScrapPickaxe", "MinDamage", 1.1)
    TweakItem("SWeapons.BigScrapPickaxe", "MaxDamage", 1.8)

    TweakItem("SWeapons.PipewithScissors", "ConditionLowerChanceOneIn", 15)
    TweakItem("SWeapons.PipewithScissors", "ConditionMax", 8)
    TweakItem("SWeapons.PipewithScissors", "MinDamage", 0.8)
    TweakItem("SWeapons.PipewithScissors", "MaxDamage", 1.2)

    TweakItem("SWeapons.TireIronAxe", "ConditionLowerChanceOneIn", 15)
    TweakItem("SWeapons.TireIronAxe", "ConditionMax", 10)
    TweakItem("SWeapons.TireIronAxe", "MinDamage", 0.8)
    TweakItem("SWeapons.TireIronAxe", "MaxDamage", 1.7)

    TweakItem("SWeapons.SalvagedPipeWrench", "ConditionLowerChanceOneIn", 25)
    TweakItem("SWeapons.SalvagedPipeWrench", "ConditionMax", 10)
    TweakItem("SWeapons.SalvagedPipeWrench", "MinDamage", 0.8)
    TweakItem("SWeapons.SalvagedPipeWrench", "MaxDamage", 1.3)

    TweakItem("SWeapons.SalvagedClimbingAxe", "ConditionLowerChanceOneIn", 20)
    TweakItem("SWeapons.SalvagedClimbingAxe", "ConditionMax", 15)
    TweakItem("SWeapons.SalvagedClimbingAxe", "MinDamage", 0.7)
    TweakItem("SWeapons.SalvagedClimbingAxe", "MaxDamage", 1.05)

    TweakItem("SWeapons.SharpenedStopSign", "ConditionLowerChanceOneIn", 10)
    TweakItem("SWeapons.SharpenedStopSign", "ConditionMax", 13)
    TweakItem("SWeapons.SharpenedStopSign", "MinDamage", 3.5)
    TweakItem("SWeapons.SharpenedStopSign", "MaxDamage", 4)

    TweakItem("SWeapons.HugeScrapPickaxe", "ConditionLowerChanceOneIn", 15)
    TweakItem("SWeapons.HugeScrapPickaxe", "ConditionMax", 10)
    TweakItem("SWeapons.HugeScrapPickaxe", "MinDamage", 2)
    TweakItem("SWeapons.HugeScrapPickaxe", "MaxDamage", 3)

    TweakItem("SWeapons.SalvagedCrowbar", "ConditionLowerChanceOneIn", 35)
    TweakItem("SWeapons.SalvagedCrowbar", "ConditionMax", 15)
    TweakItem("SWeapons.SalvagedCrowbar", "MinDamage", 0.7)
    TweakItem("SWeapons.SalvagedCrowbar", "MaxDamage", 1.30)

    TweakItem("SWeapons.BoltBat", "ConditionLowerChanceOneIn", 20)
    TweakItem("SWeapons.BoltBat", "ConditionMax", 15)
    TweakItem("SWeapons.BoltBat", "MinDamage", 1)
    TweakItem("SWeapons.BoltBat", "MaxDamage", 1.5)

    TweakItem("SWeapons.ChainBat", "ConditionLowerChanceOneIn", 20)
    TweakItem("SWeapons.ChainBat", "ConditionMax", 15)
    TweakItem("SWeapons.ChainBat", "MinDamage", 1)
    TweakItem("SWeapons.ChainBat", "MaxDamage", 1.5)

    TweakItem("SWeapons.SalvagedPipe", "ConditionLowerChanceOneIn", 15)
    TweakItem("SWeapons.SalvagedPipe", "ConditionMax", 25)
    TweakItem("SWeapons.SalvagedPipe", "MinDamage", 0.6)
    TweakItem("SWeapons.SalvagedPipe", "MaxDamage", 2)

    TweakItem("SWeapons.SalvagedSledgehammer", "ConditionLowerChanceOneIn", 15)
    TweakItem("SWeapons.SalvagedSledgehammer", "ConditionMax", 10)
    TweakItem("SWeapons.SalvagedSledgehammer", "MinDamage", 2)
    TweakItem("SWeapons.SalvagedSledgehammer", "MaxDamage", 3)

    TweakItem("SWeapons.GearMace", "ConditionLowerChanceOneIn", 15)
    TweakItem("SWeapons.GearMace", "ConditionMax", 15)
    TweakItem("SWeapons.GearMace", "MinDamage", 3)
    TweakItem("SWeapons.GearMace", "MaxDamage", 4)

    TweakItem("SWeapons.WireBat", "ConditionLowerChanceOneIn", 20)
    TweakItem("SWeapons.WireBat", "ConditionMax", 15)
    TweakItem("SWeapons.WireBat", "MinDamage", 1)
    TweakItem("SWeapons.WireBat", "MaxDamage", 1.5)

    TweakItem("SWeapons.Micromaul", "ConditionLowerChanceOneIn", 15)
    TweakItem("SWeapons.Micromaul", "ConditionMax", 10)
    TweakItem("SWeapons.Micromaul", "MinDamage", 4)
    TweakItem("SWeapons.Micromaul", "MaxDamage", 6)

    TweakItem("SWeapons.ScrapSword", "ConditionLowerChanceOneIn", 15)
    TweakItem("SWeapons.ScrapSword", "ConditionMax", 15)
    TweakItem("SWeapons.ScrapSword", "MinDamage", 1.8)
    TweakItem("SWeapons.ScrapSword", "MaxDamage", 2.5)

    TweakItem("SWeapons.ScrapMachete", "ConditionLowerChanceOneIn", 20)
    TweakItem("SWeapons.ScrapMachete", "ConditionMax", 12)
    TweakItem("SWeapons.ScrapMachete", "MinDamage", 1.0)
    TweakItem("SWeapons.ScrapMachete", "MaxDamage", 2.2)

    TweakItem("SWeapons.SalvagedNightstick", "ConditionLowerChanceOneIn", 15)
    TweakItem("SWeapons.SalvagedNightstick", "ConditionMax", 20)
    TweakItem("SWeapons.SalvagedNightstick", "MinDamage", 1.0)
    TweakItem("SWeapons.SalvagedNightstick", "MaxDamage", 2)

    TweakItem("SWeapons.SalvagedMachete", "ConditionLowerChanceOneIn", 20)
    TweakItem("SWeapons.SalvagedMachete", "ConditionMax", 15)
    TweakItem("SWeapons.SalvagedMachete", "MinDamage", 2)
    TweakItem("SWeapons.SalvagedMachete", "MaxDamage", 2.8)

    TweakItem("SWeapons.ScrapBlade", "ConditionLowerChanceOneIn", 15)
    TweakItem("SWeapons.ScrapBlade", "ConditionMax", 15)
    TweakItem("SWeapons.ScrapBlade", "MinDamage", 2)
    TweakItem("SWeapons.ScrapBlade", "MaxDamage", 3.1)

    TweakItem("SWeapons.SalvagedBlade", "ConditionLowerChanceOneIn", 15)
    TweakItem("SWeapons.SalvagedBlade", "ConditionMax", 15)
    TweakItem("SWeapons.SalvagedBlade", "MinDamage", 3.0)
    TweakItem("SWeapons.SalvagedBlade", "MaxDamage", 5.0)

    TweakItem("SWeapons.SalvagedCleaver", "ConditionLowerChanceOneIn", 15)
    TweakItem("SWeapons.SalvagedCleaver", "ConditionMax", 20)
    TweakItem("SWeapons.SalvagedCleaver", "MinDamage", 5.5)
    TweakItem("SWeapons.SalvagedCleaver", "MaxDamage", 8)

    TweakItem("SWeapons.SharpenedScrewdriver", "ConditionLowerChanceOneIn", 4)
    TweakItem("SWeapons.SharpenedScrewdriver", "ConditionMax", 13)
    TweakItem("SWeapons.SharpenedScrewdriver", "MinDamage", 0.3)
    TweakItem("SWeapons.SharpenedScrewdriver", "MaxDamage", 0.7)

    TweakItem("SWeapons.GlassShiv", "ConditionLowerChanceOneIn", 2)
    TweakItem("SWeapons.GlassShiv", "ConditionMax", 13)
    TweakItem("SWeapons.GlassShiv", "MinDamage", 0.3)
    TweakItem("SWeapons.GlassShiv", "MaxDamage", 0.7)

    TweakItem("SWeapons.ScrapShiv", "ConditionLowerChanceOneIn", 4)
    TweakItem("SWeapons.ScrapShiv", "ConditionMax", 10)
    TweakItem("SWeapons.ScrapShiv", "MinDamage", 0.4)
    TweakItem("SWeapons.ScrapShiv", "MaxDamage", 0.8)

    TweakItem("SWeapons.SalvagedShiv", "ConditionLowerChanceOneIn", 6)
    TweakItem("SWeapons.SalvagedShiv", "ConditionMax", 30)
    TweakItem("SWeapons.SalvagedShiv", "MinDamage", 0.4)
    TweakItem("SWeapons.SalvagedShiv", "MaxDamage", 1.8)

    TweakItem("SWeapons.SalvagedShivO", "ConditionLowerChanceOneIn", 6)
    TweakItem("SWeapons.SalvagedShivO", "ConditionMax", 30)
    TweakItem("SWeapons.SalvagedShivO", "MinDamage", 0.9)
    TweakItem("SWeapons.SalvagedShivO", "MaxDamage", 1.5)

    TweakItem("SWeapons.ScrapClub", "ConditionLowerChanceOneIn", 30)
    TweakItem("SWeapons.ScrapClub", "ConditionMax", 11)
    TweakItem("SWeapons.ScrapClub", "MinDamage", 0.6)
    TweakItem("SWeapons.ScrapClub", "MaxDamage", 1.2)

    TweakItem("SWeapons.SalvagedClub", "ConditionLowerChanceOneIn", 30)
    TweakItem("SWeapons.SalvagedClub", "ConditionMax", 15)
    TweakItem("SWeapons.SalvagedClub", "MinDamage", 0.8)
    TweakItem("SWeapons.SalvagedClub", "MaxDamage", 1.7)

    TweakItem("SWeapons.SpearSharpenedScrewdriver", "ConditionLowerChanceOneIn", 4)
    TweakItem("SWeapons.SpearSharpenedScrewdriver", "ConditionMax", 7)
    TweakItem("SWeapons.SpearSharpenedScrewdriver", "MinDamage", 1)
    TweakItem("SWeapons.SpearSharpenedScrewdriver", "MaxDamage", 1.7)

    TweakItem("SWeapons.SpearScrapShiv", "ConditionLowerChanceOneIn", 4)
    TweakItem("SWeapons.SpearScrapShiv", "ConditionMax", 6)
    TweakItem("SWeapons.SpearScrapShiv", "MinDamage", 1.2)
    TweakItem("SWeapons.SpearScrapShiv", "MaxDamage", 1.8)

    TweakItem("SWeapons.SpearScrapMachete", "ConditionLowerChanceOneIn", 10)
    TweakItem("SWeapons.SpearScrapMachete", "ConditionMax", 12)
    TweakItem("SWeapons.SpearScrapMachete", "MinDamage", 1.3)
    TweakItem("SWeapons.SpearScrapMachete", "MaxDamage", 1.8)

    TweakItem("SWeapons.SpearSalvaged", "ConditionLowerChanceOneIn", 12)
    TweakItem("SWeapons.SpearSalvaged", "ConditionMax", 15)
    TweakItem("SWeapons.SpearSalvaged", "MinDamage", 2.3)
    TweakItem("SWeapons.SpearSalvaged", "MaxDamage", 3)

    TweakItem("SWeapons.ScrapSpear", "ConditionLowerChanceOneIn", 8)
    TweakItem("SWeapons.ScrapSpear", "ConditionMax", 12)
    TweakItem("SWeapons.ScrapSpear", "MinDamage", 1.3)
    TweakItem("SWeapons.ScrapSpear", "MaxDamage", 2)
end