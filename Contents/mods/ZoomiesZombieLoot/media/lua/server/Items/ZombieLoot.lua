require 'Items/SuburbsDistributions'

---@param item string
---@param weightBothOrMale number
---@param weightFemale number|nil
local function addLootItem(item, weightBothOrMale, weightFemale)
    if not weightFemale then weightFemale = weightBothOrMale end
    table.insert(SuburbsDistributions["all"]["inventorymale"].items, item);
    table.insert(SuburbsDistributions["all"]["inventorymale"].items, weightBothOrMale);
    table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, item);
    table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, weightFemale);
end

-- Farming
addLootItem("farming.BroccoliBagSeed", 0.05)
addLootItem("farming.CabbageBagSeed", 0.05)
addLootItem("farming.CarrotBagSeed", 0.05)
addLootItem("farming.PotatoBagSeed", 0.05)
addLootItem("farming.RedRadishBagSeed", 0.05)
addLootItem("farming.StrewberrieBagSeed", 0.05)
addLootItem("farming.TomatoBagSeed", 0.05)

if getActivatedMods():contains("WastelandCarParts") then
    addLootItem("Base.RubberBushSeedBag", 0.05)
end

if getActivatedMods():contains("AnaLGiNs_RenewableFoodResources") then
    addLootItem("ANL.SugarBeetBagSeed", 0.05)
end

-- Food and water
addLootItem("Base.Peanuts", 0.2)
addLootItem("Base.Peanuts", 0.2)
addLootItem("Base.SunflowerSeeds", 0.4)
addLootItem("Base.Crisps", 0.1)
addLootItem("Base.Crisps2", 0.1)
addLootItem("Base.Crisps3", 0.1)
addLootItem("Base.MintCandy", 0.2)
addLootItem("Base.WaterBottleFull", 0.1)
addLootItem("Base.WaterBottleEmpty", 0.2)
addLootItem("Base.Chocolate", 0.1)
addLootItem("Base.Lollipop", 0.1)
addLootItem("Base.TortillaChips", 0.1)
addLootItem("Base.GranolaBar", 0.3)
-- addLootItem("Base.BeerBottle", 0.1)
-- addLootItem("Base.BeerCan", 0.05)
-- addLootItem("Base.CookiesOatmeal", 0.3)
-- addLootItem("Base.Wine", 0.1)
-- addLootItem("Base.Wine2", 0.1)


-- Additional Alcohol/Beverages (SapphCooking)
-- if getActivatedMods():contains("sapphcooking") then
--     addLootItem("SapphCooking.SakeFull", 0.1)
--     addLootItem("SapphCooking.VodkaFull", 0.1)
--     addLootItem("SapphCooking.TequillaFull", 0.1)
--     addLootItem("SapphCooking.RumFull", 0.1)
--     addLootItem("SapphCooking.CachaçaFull", 0.1)
--     addLootItem("SapphCooking.ColaBottle", 0.1)
--     addLootItem("SapphCooking.CarbonatedWater",0.1)
-- end

-- Consumables
addLootItem("Base.Vinegar", 0.005)
addLootItem("Base.Battery", 2, 1)
addLootItem("Base.Tissue", 2)
addLootItem("Base.Newspaper", 4, 2)
addLootItem("Base.Nails", 2.2, 0.7)
addLootItem("Base.Screws", 1.3, 1.1)
addLootItem("Base.ScrapMetal", 3)
-- addLootItem("Base.UnusableMetal", 0.2)
addLootItem("Base.EngineParts", 0.01)
addLootItem("Base.PetrolCan", 0.0001)
addLootItem("Base.DuctTape", 0.1)
addLootItem("Base.Glue", 0.5)
addLootItem("Base.Woodglue", 0.2)
addLootItem("Base.Bleach", 0.2)
addLootItem("Base.Matches", 0.5)
addLootItem("Base.Candle", 2)
addLootItem("Base.HottieZ", 0.3, 1)
addLootItem("Base.LightBulb", 3)

-- Medical
addLootItem("Base.WhiskeyFull", 0.3, 0.15)
addLootItem("Base.Tweezers", 0.1)

-- Guns
addLootItem("Base.Pistol", 0.00005) -- M9 Pistol (uses 9mm ammo)
addLootItem("Base.Pistol2", 0.00005) -- M1911 Pistol (uses .45 ammo)
addLootItem("Base.ShotgunSawnoff", 0.00005) -- Sawed-off variety of the JS-2000 shotgun, aka Mossberg 500 in Firearms B41
addLootItem("Base.Revolver_Short", 0.0001) -- M36 Revolver (uses .38 ammo)

-- Ammo
addLootItem("Base.Bullets38", 0.015) -- Interchangeable with .357
addLootItem("Base.ShotgunShells", 0.005)
addLootItem("Base.Bullets9mm", 0.015)
addLootItem("Base.308Bullets", 0.002) -- Interchangeable with 7.62x51mm
addLootItem("Base.Bullets45", 0.0015)

-- B41 Ammo
if getActivatedMods():contains("firearmmod") then
    addLootItem("762x39Bullets", 0.0006)
    addLootItem("762x51Bullets", 0.0006) -- Interchangeable with .308
    addLootItem("223Bullets", 0.0006) -- Interchangeable with 5.56x45mm (556Bullets)
    addLootItem("Bullets3006" , 0.0006)
    addLootItem("Bullets22", 0.0015)
    addLootItem("556Bullets", 0.0006) -- Interchangeable with .223
    addLootItem("Bullets357", 0.0015) -- NOT interchangeable with .38 (there is no recipe for now)
    addLootItem("Bullets4440", 0.0015)
    addLootItem("Bullets44", 0.0015)
end

-- Weapons
addLootItem("Base.Machete", 0.00001)
addLootItem("Base.MeatCleaver", 0.009)


-- Trashy Weapons
addLootItem("Base.BreadKnife", 0.7)
addLootItem("Base.ButterKnife", 3)
addLootItem("Base.Scalpel", 0.5, 0.2)
addLootItem("Base.Stake", 1)
addLootItem("Base.RollingPin", 0.8)

-- Cooking
addLootItem("Base.Pepper", 0.005)
addLootItem("Base.Pot", 0.02)
addLootItem("Base.Pan", 0.03)
addLootItem("Base.Saucepan", 0.04)
addLootItem("Base.KitchenKnife", 0.4, 0.5)
addLootItem("Base.EmptyJar", 0.4, 0.5)
addLootItem("Base.JarLid", 2)


-- Tools
addLootItem("Base.TinOpener", 0.3)
addLootItem("Base.Hammer", 0.1)
addLootItem("Base.PipeWrench", 0.01, 0.03)
addLootItem("Base.Spanner", 0.05, 0.01)
addLootItem("Base.Saw", 0.05)
addLootItem("Base.HandAxe", 0.1)
addLootItem("Base.Axe", 0.0001)
addLootItem("Base.WoodAxe", 0.0001)
addLootItem("Base.Crowbar", 0.0003)
addLootItem("Base.Sledgehammer", 0.0001)
addLootItem("Base.BlowTorch", 0.0005)
addLootItem("Base.Torch", 0.2)
addLootItem("Radio.WalkieTalkie1", 0.1)
addLootItem("Radio.WalkieTalkie2", 0.01)
addLootItem("Base.Earbuds", 0.1)
addLootItem("Base.Pen", 2)
addLootItem("Base.EmptySandbag", 0.02)
addLootItem("Base.Twine", 0.7)
addLootItem("Base.Thread", 4)

-- Containers
addLootItem("Base.Bag_FannyPackBack", 0.002, 0.004)
addLootItem("Base.Plasticbag", 0.7)

-- Warm Clothing
addLootItem("Base.Hat_Beany", 0.05)
addLootItem("Base.Scarf_StripeBlackWhite", 0.02)
addLootItem("Base.Scarf_StripeRedWhite", 0.02)
addLootItem("Base.Scarf_StripeBlueWhite", 0.02)
addLootItem("Base.Scarf_White", 0.02)

-- Misc
addLootItem("Base.Money", 9)
