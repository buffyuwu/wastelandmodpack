
local originalGetActivatedMods = getActivatedMods
function getActivatedMods()
    local mods = originalGetActivatedMods():clone()
    for i = 0, mods:size() - 1 do
        local mod = mods:get(i)
        if mod:sub(-10) == "-Wasteland" then
            mods:add(mod:sub(1, -11))
        end
    end
    return mods
end
