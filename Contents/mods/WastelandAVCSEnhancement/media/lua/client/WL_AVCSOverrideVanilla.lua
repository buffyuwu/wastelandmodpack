require "AVCSOverrideVanilla.lua"
require "TimedActions/ISInventoryTransferAction"

if not AVCS.oISInventoryTransferAction then
    AVCS.oISInventoryTransferAction = ISInventoryTransferAction.isValid
end

function ISInventoryTransferAction:isValid()
    local vehicle = nil
    if self.destContainer:getParent() and instanceof(self.destContainer:getParent(), "BaseVehicle") then
        vehicle = self.destContainer:getParent()
    end
    if self.srcContainer:getParent() and instanceof(self.srcContainer:getParent(), "BaseVehicle") then
        vehicle = self.srcContainer:getParent()
    end

    if vehicle then
        local isPublic = AVCS.getPublicPermission(vehicle, "AllowOpeningTrunk")
        local hasPerm =  AVCS.getSimpleBooleanPermission(AVCS.checkPermission(self.character, vehicle))

        if not isPublic and not hasPerm then
            self.character:setHaloNote(getText("IGUI_AVCS_Vehicle_No_Permission"), 250, 250, 250, 300)
            return false
        end
    end

    return AVCS.oISInventoryTransferAction(self)
end