require("WF_Sprinkler/WFSprinklerUtilities")

local WFSprinklerWorldMenu = {}

function WFSprinklerWorldMenu.doWaterPlants(playerObj, barrel, maxWater)
    local plants = WFSprinklerUtilities.getWaterablePlants(barrel:getSquare(), 3)

    if #plants == 0 then
        playerObj:Say("All the plants look well watered already.")
        return
    end

    local plantSquares = {}
    for _, plant in ipairs(plants) do
        local sq = WFSprinklerUtilities.getStandInSquare(barrel:getSquare(), plant)
        table.insert(plantSquares, {sq, plant})
    end
    table.sort(plantSquares, WFSprinklerUtilities.sortPlants)

    local lastSq = plantSquares[1][1]
    ISTimedActionQueue.add(ISWalkToTimedAction:new(playerObj, lastSq))
    for _, plantSquare in ipairs(plantSquares) do
        local sq = plantSquare[1]
        local plant = plantSquare[2]
        if sq ~= lastSq then
            ISTimedActionQueue.add(ISWalkToTimedAction:new(playerObj, sq))
            lastSq = sq
        end
        local water = WFSprinklerUtilities.getWaterUsedForPlant(maxWater, barrel, plant)
        ISTimedActionQueue.add(WFSprinklerWaterAction:new(playerObj, barrel, plant, water, water * 5))
    end
end

function WFSprinklerWorldMenu.doDetachSprinkler(player, sprinkler)
	local adjacent = AdjacentFreeTileFinder.Find(sprinkler:getSquare(), player)
    if adjacent == nil then
        player:Say("I can't reach that.")
        return
    end
	ISTimedActionQueue.add(ISWalkToTimedAction:new(player, adjacent))
    ISTimedActionQueue.add(WFSprinklerDetachAction:new(player, sprinkler, 200))
end

function WFSprinklerWorldMenu.doAttachSprinkler(player, sprinkler, barrel)
	local adjacent = AdjacentFreeTileFinder.Find(barrel:getSquare(), player)
    if adjacent == nil then
        player:Say("I can't reach that.")
        return
    end
	ISTimedActionQueue.add(ISWalkToTimedAction:new(player, adjacent))
    ISTimedActionQueue.add(WFSprinklerAttachAction:new(player, sprinkler, barrel, 200))
end

function WFSprinklerWorldMenu.fillContext(playerIdx, context, worldobjects, test)
    -- check if and get the barrel
    local barrel = WFSprinklerUtilities.getBarrelObject(worldobjects)
    if not barrel then return end

    -- check if and get the sprinkler in the player's inventory
    local playerObj = getSpecificPlayer(playerIdx)
    local inventorySprinkler = WFSprinklerUtilities.getSprinklerItem(playerObj)

    -- check if and get the sprinkler attached to the barrel
    local worldSprinkler = WFSprinklerUtilities.getBarrelSprinkler(barrel)


    if worldSprinkler then
        -- detach sprinkler
        context:addOption(getText("ContextMenu_WFDetachSprinkler"), playerObj, WFSprinklerWorldMenu.doDetachSprinkler, worldSprinkler)

        -- use sprinkler
        local maxWater = math.floor(math.min(100, WFSprinklerUtilities.getUsableWaterInBarrel(barrel))/10)*10
        if maxWater >= 10 then
            local option = context:addOption(getText("ContextMenu_WFUseSprinkler"), nil, nil)
            local submenu = ISContextMenu:getNew(context)
            context:addSubMenu(option, submenu)
            for i=maxWater, 10, -10 do
                submenu:addOption(tostring(i), playerObj, WFSprinklerWorldMenu.doWaterPlants, barrel, i)
            end
        end
    elseif inventorySprinkler then
        -- attach sprinkler
        context:addOption(getText("ContextMenu_WFAttachSprinkler"), playerObj, WFSprinklerWorldMenu.doAttachSprinkler, inventorySprinkler, barrel)
    end
end

Events.OnFillWorldObjectContextMenu.Add(WFSprinklerWorldMenu.fillContext)
