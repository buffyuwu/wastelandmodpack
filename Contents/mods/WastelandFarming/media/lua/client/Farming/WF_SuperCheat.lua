require 'Farming/ISFarmingMenu'

local function doSuperCheat(worldobjects, plant)
    ISFarmingMenu.onCheatWater(worldobjects, plant)
    ISFarmingMenu.onCheatGrow(worldobjects, plant)
    ISFarmingMenu.onCheat(worldobjects, plant, {var = "nbOfGrow", count = 6 - plant.nbOfGrow})
    ISFarmingMenu.onCheat(worldobjects, plant, {var = "health", count = 100})
    ISFarmingMenu.onCheat(worldobjects, plant, { var = 'fliesLvl', count = -100 })
    ISFarmingMenu.onCheat(worldobjects, plant, { var = 'mildewLvl', count = -100 })
    ISFarmingMenu.onCheat(worldobjects, plant, { var = 'aphidLvl', count = -100 })
end

local original_ISFarmingMenu_doFarmingMenu2 = ISFarmingMenu.doFarmingMenu2
ISFarmingMenu.doFarmingMenu2 = function(player, context, worldobjects, test)
    original_ISFarmingMenu_doFarmingMenu2(player, context, worldobjects, test)
    for i,v in ipairs(worldobjects) do
        local plant = CFarmingSystem.instance:getLuaObjectOnSquare(v:getSquare())
        if ISFarmingMenu.cheat and plant then
            context:addOption("Super Cheat Plant", worldobjects, doSuperCheat, plant)
            return
        end
    end
end