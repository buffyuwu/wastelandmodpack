if isClient() then return end

require 'Farming/SFarmingSystem'
require "Farming/SGFarmingSystem"

if farming_vegetableconf.props["Tea"] then farming_vegetableconf.props["Tea"].seedPerVegVar = {1,5} end
if farming_vegetableconf.props["Coffee"] then farming_vegetableconf.props["Coffee"].seedPerVegVar = {1,5} end
if farming_vegetableconf.props["Latex"] then farming_vegetableconf.props["Latex"].seedPerVegVar = {1,5} end
if farming_vegetableconf.props["Rose"] then farming_vegetableconf.props["Rose"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Carnation"] then farming_vegetableconf.props["Carnation"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Larkspur"] then farming_vegetableconf.props["Larkspur"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Dahlia"] then farming_vegetableconf.props["Dahlia"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Delphi"] then farming_vegetableconf.props["Delphi"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Daisy"] then farming_vegetableconf.props["Daisy"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Penta"] then farming_vegetableconf.props["Penta"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Geranium"] then farming_vegetableconf.props["Geranium"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Bird"] then farming_vegetableconf.props["Bird"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Mutton"] then farming_vegetableconf.props["Mutton"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Pork"] then farming_vegetableconf.props["Pork"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Beef"] then farming_vegetableconf.props["Beef"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Wool"] then farming_vegetableconf.props["Wool"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Milk"] then farming_vegetableconf.props["Milk"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Egg"] then farming_vegetableconf.props["Egg"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Honey"] then farming_vegetableconf.props["Honey"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Leek"] then farming_vegetableconf.props["Leek"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Lettuce"] then farming_vegetableconf.props["Lettuce"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Onion"] then farming_vegetableconf.props["Onion"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["SoyBean"] then farming_vegetableconf.props["SoyBean"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Pumpkin"] then farming_vegetableconf.props["Pumpkin"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Watermelon"] then farming_vegetableconf.props["Watermelon"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Zucchini"] then farming_vegetableconf.props["Zucchini"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Corn"] then farming_vegetableconf.props["Corn"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Ginger"] then farming_vegetableconf.props["Ginger"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Pineapple"] then farming_vegetableconf.props["Pineapple"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Wheat"] then farming_vegetableconf.props["Wheat"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["SugarCane"] then farming_vegetableconf.props["SugarCane"].seedPerVegVar = {1,5} end
if farming_vegetableconf.props["Ginseng"] then farming_vegetableconf.props["Ginseng"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Mushroom"] then farming_vegetableconf.props["Mushroom"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["BellPepper"] then farming_vegetableconf.props["BellPepper"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["BerryBlack"] then farming_vegetableconf.props["BerryBlack"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["BerryBlue"] then farming_vegetableconf.props["BerryBlue"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Lemongrass"] then farming_vegetableconf.props["Lemongrass"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Eggplant"] then farming_vegetableconf.props["Eggplant"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Grape"] then farming_vegetableconf.props["Grape"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Rice"] then farming_vegetableconf.props["Rice"].seedPerVegVar = {1,5} end
if farming_vegetableconf.props["PepperPlant"] then farming_vegetableconf.props["PepperPlant"].seedPerVegVar = {1,5} end
if farming_vegetableconf.props["Hops"] then farming_vegetableconf.props["Hops"].seedPerVegVar = {1,5} end
if farming_vegetableconf.props["Cotton"] then farming_vegetableconf.props["Cotton"].seedPerVegVar = {1,5} end
if farming_vegetableconf.props["Pear"] then farming_vegetableconf.props["Pear"].seedPerVegVar = {1,5} end
if farming_vegetableconf.props["CommonMallow"] then farming_vegetableconf.props["CommonMallow"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Plantain"] then farming_vegetableconf.props["Plantain"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Comfrey"] then farming_vegetableconf.props["Comfrey"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Garlic"] then farming_vegetableconf.props["Garlic"].seedPerVegVar = {1,3} end
if farming_vegetableconf.props["Sage"] then farming_vegetableconf.props["Sage"].seedPerVegVar = {1,3} end


-- Override to attach player to the luaObject temporarily so
-- that we can use the player's farming skill to determine
-- the yield of the plant.
-- Also will adjust the seedsPerVeg if a range is specified.
local original_SFarmingSystem_harvest = SFarmingSystem.harvest
function SFarmingSystem:harvest(luaObject, player)
    luaObject.harvestPlayer = player
    local props = farming_vegetableconf.props[luaObject.typeOfSeed]
    if props.seedPerVegVar ~= nil then
        props.seedPerVeg = ZombRand(props.seedPerVegVar[1], props.seedPerVegVar[2] + 1)
    end
    original_SFarmingSystem_harvest(self, luaObject, player)
    luaObject.harvestPlayer = nil
end

-- Kill plants in winter

local winterPlants = ArrayList.new()
winterPlants:add("Olive")
winterPlants:add("GrapeFruit")
winterPlants:add("Lemon")
winterPlants:add("Orange")
winterPlants:add("Apple")
winterPlants:add("Banana")
winterPlants:add("Cherry")
winterPlants:add("Mango")
winterPlants:add("Pear")
winterPlants:add("Milk")
winterPlants:add("Egg")
winterPlants:add("Wool")
winterPlants:add("Beef")
winterPlants:add("Pork")
winterPlants:add("Mutton")
winterPlants:add("Bird")
winterPlants:add("Honey")
winterPlants:add("Latex")

local function survivesWinter(luaObject)
    return winterPlants:contains(luaObject.typeOfSeed) and luaObject.nbOfGrow >= 4
end

function SFarmingSystem:CheckTemperture()
	local climate = getClimateManager()
    local temp = climate:getTemperature()
    if temp > 0 then return end -- if temp is above 0, then we don't need to check plants
    local damage = temp * 0.5 -- damage is half of temp
    for i=1,self:getLuaObjectCount() do
		local luaObject = self:getLuaObjectByIndex(i)
        if not survivesWinter(luaObject) then
            local square = luaObject:getSquare()
            if square and square:isOutside() and luaObject.state ~= "plow" and luaObject.state ~= "destroy" then
                if damage < -10 then
                    luaObject:rottenThis()
                end
                luaObject.health = luaObject.health + damage -- damage is negative, so we add it
                if luaObject.health <= 0 then
                    luaObject:rottenThis()
                end
            end
        end
    end
end

local function EveryTenMinutes()
	SFarmingSystem.instance:CheckTemperture()
end

Events.EveryTenMinutes.Add(EveryTenMinutes)