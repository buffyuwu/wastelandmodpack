require "Chat/ISChat"
require "WCL/Loadouts"

local FakeMessage = {}
function FakeMessage:new(text)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.text = text
    return o
end
function FakeMessage:getTextWithPrefix()
    return self.text
end

local function rawAddLine(line)
    local message = FakeMessage:new(line)
    local chatText = ISChat.instance.chatText
    local vscroll = chatText.vscroll
    local scrolledToBottom = (chatText:getScrollHeight() <= chatText:getHeight()) or (vscroll and vscroll.pos == 1)
    if #chatText.chatTextLines > ISChat.maxLine then
        local newLines = {};
        for i,v in ipairs(chatText.chatTextLines) do
            if i ~= 1 then
                table.insert(newLines, v);
            end
        end
        table.insert(newLines, line .. " <LINE> ");
        chatText.chatTextLines = newLines;
    else
        table.insert(chatText.chatTextLines, line .. " <LINE> ");
    end
    chatText.text = "";
    local newText = "";
    for i,v in ipairs(chatText.chatTextLines) do
        if i == #chatText.chatTextLines then
            v = string.gsub(v, " <LINE> $", "")
        end
        newText = newText .. v;
    end
    chatText.text = newText;
    table.insert(chatText.chatMessages, message);
    chatText:paginate();
    if scrolledToBottom then
        chatText:setYScroll(-10000);
    end
end

local function errorLine(msg)
    rawAddLine("<RGB:1.0,0.3,0.3>" .. msg)
end

local function infoLine(msg)
    rawAddLine("<RGB:0.5,0.5,1.0>" .. msg)
end

local function mutedLine(msg)
    rawAddLine("<RGB:0.3,0.3,0.3>" .. msg)
end

local function ensureIsStaff()
    local player = getPlayer()
    if player:getAccessLevel() == "None" then
        errorLine("Staff only command.")
        return false
    end
    return true
end

local function ensureIsAdmin()
    local player = getPlayer()
    if player:getAccessLevel() ~= "Admin" then
        errorLine("Admin only command.")
        return false
    end
    return true
end

local function finishChat(command)
    ISChat.instance:unfocus()
    ISChat.instance:logChatCommand(command)
    doKeyPress(false)
    ISChat.instance.timerTextEntry = 20
end

local ctRed = "<RGB:1.0,0.5,0.5>"
local ctGreen = "<RGB:0.5,1.0,0.5>"
local ctBlue = "<RGB:0.5,0.5,1.0>"
local ctLBlue = "<RGB:0.7,0.7,1.0>"
local ctLGrey = "<RGB:0.7,0.7,0.7>"
local ctGrey = "<RGB:0.5,0.5,0.5>"
local ctWhite = "<RGB:1.0,1.0,1.0>"

local setX1 = "<SETX:10>"
local setX2 = "<SETX:30>"
local setX3 = "<SETX:80>"
local setX4 = "<SETX:130>"
local setX5 = "<SETX:175>"

local listCommandText = ctBlue .. setX1 .. "/lo " .. setX2 .. ctLBlue .. " list " .. setX3 .. ctLGrey .. " [-g -p] " .. setX5 .. ctGrey .. " - List all loadouts, -g for global, -p for player"
local wearCommandText = ctBlue .. setX1 .. "/lo " .. setX2 .. ctLBlue .. " wear " .. setX4 .. ctGreen .. " name " .. setX5 .. ctGrey .. " - Wear the specified loadout"
local nakedCommandText = ctBlue .. setX1 .. "/lo " .. setX2 .. ctLBlue .. " naked " .. setX5 .. ctGrey .. " - Remove all clothing"
local saveCommandText = ctBlue .. setX1 .. "/lo " .. setX2 .. ctLBlue .. " save " .. setX3 .. ctLGrey .. " [-g] " .. setX4 .. ctGreen .. " name " .. setX5 .. ctGrey .. " - Save your outfit as a loadout, -g for global"
local deleteCommandText = ctBlue .. setX1 .. "/lo " .. setX2 .. ctLBlue .. " del " .. setX3 .. ctLGrey .. " [-g] " .. setX4 .. ctGreen .. " name " .. setX5 .. ctGrey .. " - Delete the specified loadout, -g for global"

local LoadoutCommands = {}
function LoadoutCommands.help(player, options, arguments)
    infoLine("Wasteland Clothing Loadouts")
    infoLine(listCommandText)
    infoLine(wearCommandText)
    infoLine(nakedCommandText)
    infoLine(saveCommandText)
    infoLine(deleteCommandText)
end

function LoadoutCommands.list(player, options, arguments)
    local names = ""
    local global = options["-g"]
    local player = options["-p"]

    if not global and not player then
        global = true
        player = true
    end

    if global then
        for name, _ in pairs(WCL_Loadouts.Loadouts) do
            names = names .. name .. ", "
        end
    end

    if player then
        for name, _ in pairs(WCL_Loadouts.PlayerLoadouts) do
            names = names .. name .. ", "
        end
    end
    infoLine("Loadouts: " .. names:sub(1, -3))
end

function LoadoutCommands.wear(player, options, arguments)
    local name = arguments[1]
    local loadout = WCL_Loadouts.getLoadout(name)

    if not name then
        errorLine("A name is required.")
        infoLine(wearCommandText)
        return
    end
    if not loadout then
        errorLine("Loadout not found: " .. name)
        return
    end

    WCL_Loadouts.wearLoadoutClothingReset(player, loadout.clothing)
    infoLine("Wearing loadout: " .. name)
end

function LoadoutCommands.naked(player, options, arguments)
    WCL_Loadouts.removeClothing(player)
    infoLine("NAKED!")
end

function LoadoutCommands.save(player, options, arguments)
    local global = options["-g"]
    local name = arguments[1]

    if global and not ensureIsAdmin() then
        return
    end
    if not name then
        errorLine("A name is required.")
        infoLine(saveCommandText)
        return
    end

    local loadout = {}
    loadout.inventory = {}
    loadout.equips = {}
    loadout.clothing = WCL_Loadouts.getCurrentClothing(player)
    if global then
        WCL_Loadouts.saveLoadout(player, name, loadout)
    else
        WCL_Loadouts.savePlayerLoadout(player, name, loadout)
    end
    infoLine("Saved loadout: " .. name)
end

function LoadoutCommands.del(player, options, arguments)
    local global = options["-g"]
    local name = arguments[1]

    if global and not ensureIsAdmin() then
        return
    end
    if not name then
        errorLine("A name is required.")
        infoLine(deleteCommandText)
        return
    end

    if global then
        WCL_Loadouts.deleteLoadout(player, name)
    else
        WCL_Loadouts.deletePlayerLoadout(player, name)
    end

    infoLine("Deleted loadout: " .. name)
end

local original_ISChat_onCommandEntered = ISChat.onCommandEntered
function ISChat:onCommandEntered()
    local command = ISChat.instance.textEntry:getText()
    local player = getPlayer()

    if command:sub(1, 4) == "/lo " then
        mutedLine(command)

        -- players are not allowed to use /lo
        if not ensureIsStaff() then
            finishChat(command)
            return
        end

        -- parse the command into command, options, and arugments
        local commandParts = command:split(" ")
        local cmd = ""
        local options = {}
        local args = {}
        for i = 2, #commandParts do
            local part = commandParts[i]
            if part:sub(1, 1) == "-" then
                options[part] = true
            elseif cmd == "" then
                cmd = part
            else
                table.insert(args, part)
            end
        end

        -- if no command was specified, show the help
        if not LoadoutCommands[cmd] then
            LoadoutCommands.help(player, options, args)
            finishChat(command)
            return
        end

        -- run the command
        LoadoutCommands[cmd](player, options, args)
        finishChat(command)
        return
    end

    return original_ISChat_onCommandEntered(self)
end