require "TimedActions/ISInventoryTransferAction"

local function getInventoryName(container, player)
    if container:getType() == "floor" then
        return "Ground"
    end
    if instanceof(container:getParent(), "IsoDeadBody") then
        return "Corpse"
    end
    if instanceof(container:getParent(), 'IsoMannequin') then
        return "Mannequin"
    end
    if instanceof(container:getParent(), "BaseVehicle") then
        return container:getParent():getScript():getName() .. "-" .. container:getType()
    end

    if container:isInCharacterInventory(player) then
        if instanceof(container:getParent(), "IsoPlayer") then
            return "Hands"
        else
            return "player-" .. container:getType()
        end
    end

    return container:getType()
end

function ISInventoryTransferAction:WL_SendItem()
    if not self.WL_currentPendingItem then
        return
    end
    sendClientCommand(self.character, "WastelandLogging", "itemTransfer", self.WL_currentPendingItem)
    self.WL_currentPendingItem = nil
end

local ISInventoryTransferAction_transferItem = ISInventoryTransferAction.transferItem
function ISInventoryTransferAction:transferItem(item)
	if self:isAlreadyTransferred(item) then
		return
	end

    ISInventoryTransferAction_transferItem(self, item)

    if self.destContainer:getType() == "TradeUI" or self.srcContainer:getType() == "TradeUI" then
        return
    end

    if self.WL_currentPendingItem then
        if self.WL_currentPendingItem.item == item:getType() then
            self.WL_currentPendingItem.count = self.WL_currentPendingItem.count + 1
            return
        else
            sendClientCommand(self.character, "WastelandLogging", "itemTransfer", self.WL_currentPendingItem)
            self:WL_SendItem()
        end
    end

    local source = getInventoryName(self.srcContainer, self.character)
    local dest = getInventoryName(self.destContainer, self.character)
    local x, y, z = self.character:getX(), self.character:getY(), self.character:getZ()
    local item = item:getType()
    local args = { x = x, y = y, z = z, item = item, source = source, dest = dest, count = 1 }
    self.WL_currentPendingItem = args
end

local ISInventoryTransferAction_perform = ISInventoryTransferAction.perform
function ISInventoryTransferAction:perform()
    ISInventoryTransferAction_perform(self)
    self:WL_SendItem()
end

local ISUninstallVehiclePart_perform = ISUninstallVehiclePart.perform
function ISUninstallVehiclePart:perform()
    local args = { vehicle = self.vehicle:getId(), part = self.part:getId() }
	sendClientCommand(self.character, 'WastelandLogging', 'uninstallPart', args)
    ISUninstallVehiclePart_perform(self)
end