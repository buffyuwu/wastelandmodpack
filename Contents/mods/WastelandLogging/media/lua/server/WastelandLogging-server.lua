local WastelandLoggingCommands = {}

local function getVehicleCondition(vehicle)
    local totalParts = 0
    local totalCondition = 0
    for i=1,vehicle:getPartCount() do
		local part = vehicle:getPartByIndex(i-1)
		local category = part:getCategory() or "Other";
		if category ~= "nodisplay" then
            totalParts = totalParts + 1
            totalCondition = totalCondition + part:getCondition()
        end
    end
    return math.floor(totalCondition / totalParts, 2)
end

function WastelandLoggingCommands.VehicleParts(playerObj, vehicle, part, action)
    local x, y, z = math.floor(vehicle:getX()), math.floor(vehicle:getY()), math.floor(vehicle:getZ())
    local username = playerObj:getUsername()
    local partName = part:getInventoryItem():getName()
    local vehicleName = vehicle:getScript():getName()
    local vehicleCondition = getVehicleCondition(vehicle)
    local partCondition = part:getCondition()
    local logMessage = string.format("%s %s %s (%s%%) from %s (%s%%) at %s,%s,%s", username, action, partName, partCondition, vehicleName, vehicleCondition, x, y, z)
    writeLog("VehicleParts", logMessage)
end

function WastelandLoggingCommands.ItemTransfer(playerObj, x, y, z, item, count, source, dest)
    local x, y, z = math.floor(x), math.floor(y), math.floor(z)
    local username = playerObj:getUsername()
    local logMessage = string.format("%s transferred %s %s from %s to %s at %s,%s,%s", username, count, item, source, dest, x, y, z)
    writeLog("ItemTransfer", logMessage)
end

function WastelandLoggingCommands.OnClientCommand(module, command, player, args)
    if module == 'vehicle' then
        if command == "installPart" then
            local vehicle = getVehicleById(args.vehicle)
            local part = vehicle:getPartById(args.part)
            WastelandLoggingCommands.VehicleParts(player, vehicle, part, "Installed")
        end
    elseif module == 'WastelandLogging' then
        if command == "itemTransfer" then
            WastelandLoggingCommands.ItemTransfer(player, args.x, args.y, args.z, args.item, args.count, args.source, args.dest)
        elseif command == "uninstallPart" then
            local vehicle = getVehicleById(args.vehicle)
            local part = vehicle:getPartById(args.part)
            WastelandLoggingCommands.VehicleParts(player, vehicle, part, "Uninstalled")
        end
    end
end


Events.OnClientCommand.Add(WastelandLoggingCommands.OnClientCommand)