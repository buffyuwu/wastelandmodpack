if not isClient() then return end

Events.ReceiveSafehouseInvite.Remove(ISSafehouseUI.ReceiveSafehouseInvite)

require "GravyUI"


WSH_Panel = ISPanel:derive("WSH_Panel")
WSH_Panel.instance = nil
local FONT_HGT_SMALL = getTextManager():getFontHeight(UIFont.Small)

function WSH_Panel:show()
    if WSH_Panel.instance then
        WSH_Panel.instance:onClose()
    end

    local w = 400
    local h = 350
    local o = ISPanel:new(getCore():getScreenWidth()/2-w/2,getCore():getScreenHeight()/2-h/2, w, h)
    o.backgroundColor = {r=0, g=0, b=0, a=0.8};
    setmetatable(o, self)
    o.__index = self
    o:initialise()
    o:addToUIManager()
    WSH_Panel.instance = o
    return o
end


function WSH_Panel:initialise()
    ISPanel.initialise(self)

    local window = GravyUI.Node(self.width, self.height):pad(5)
    local header, body, footer = window:rows({30, 1, 20}, 5)
    self.headerTitle = header
    self.closeButton = footer:makeButton("Close", self, self.onClose)
    self.safehouseList = ISScrollingListBox:new(body.left, body.top, body.width, body.height);
    self.safehouseList:initialise()
    self.safehouseList.anchorTop = true
    self.safehouseList.anchorLeft = true
    self.safehouseList.itemheight = FONT_HGT_SMALL + 2 * 2;
    self.safehouseList.selected = 0;
    self.safehouseList.joypadParent = self;
    self.safehouseList.font = UIFont.NewSmall;
    self.safehouseList.doDrawItem = self.drawSafehouses;
    self.safehouseList.drawBorder = true;
    self.safehouseList:setOnMouseDownFunction(self, self.onListMouseDown)
    self:addChild(self.closeButton)
    self:addChild(self.safehouseList)
    self:populateList()

end

function WSH_Panel:onListMouseDown()
    if self.safehouseList.mouseOverButton then
         local modal = ISSafehouseUI:new(getCore():getScreenWidth() / 2 - 250, getCore():getScreenHeight() / 2 - 225, 500, 450, self.safehouseList.mouseOverButton.item, getPlayer())
         modal:initialise()
         modal:addToUIManager()
         if WSH_Panel.instance then
            WSH_Panel.instance:onClose()
         end
    end
end

WSH_Panel.canClaimSafehouseText = "Can claim own safehouse"


function WSH_Panel:allSafehousesOfClient()
    local username = getPlayer():getUsername()
    local safehouses = {}
    safehouses.is_owner = false
    safehouses.owns = {}
    safehouses.guestOf = {}
    local list = SafeHouse.getSafehouseList()
    for i=0,list:size()-1 do
		local safe = list:get(i);
        local owner = safe:getOwner()
		if (safe:getPlayers():contains(username) or (owner == username)) then
            if owner == username then
                safehouses.is_owner = true
                table.insert(safehouses.owns, safe)
            else
                table.insert(safehouses.guestOf, safe)
            end
		end
    end
    return safehouses
end


function WSH_Panel:drawSafehouses(y, item, alt)
    local a = 0.9;
    self:drawRectBorder(0, (y), self:getWidth(), self.itemheight - 1, a, self.borderColor.r, self.borderColor.g, self.borderColor.b);

    if self.selected == item.index and item.text ~= WSH_Panel.canClaimSafehouseText then
        self:drawRect(0, (y), self:getWidth(), self.itemheight - 1, 0.3, 0.7, 0.35, 0.15);
        local textWidth = getTextManager():MeasureStringX(UIFont.Small, "VIEW")
        local buttonWid = 8 + textWidth + 8
        local buttonHgt = self.itemheight - 3
        local scrollBarWid = self:isVScrollBarVisible() and 13 or 0
        local buttonX = self.width - 16 - scrollBarWid - buttonWid
        local buttonY = y+1
        local isMouseOver = self.mouseoverselected == item.index
        local isMouseOverButton = isMouseOver and ((self:getMouseX() > buttonX - 16) and (self:getMouseX() < self.width - scrollBarWid) and (self:getMouseY() < buttonY + buttonHgt + 16))
        if isMouseOverButton then
			self:drawRect(buttonX, buttonY, buttonWid, buttonHgt, 1, 0.3, 0.3, 0.3)
			self.mouseOverButton = item
		else
			self:drawRect(buttonX, buttonY, buttonWid, buttonHgt, 1, 0.1, 0.1, 0.1)
		end
		self:drawTextCentre("VIEW", buttonX +  buttonWid / 2, buttonY + (buttonHgt - FONT_HGT_SMALL) / 2 , 1, 1, 1, 1)
    end

    local rb = 1
    if item.itemindex == 1 and item.text == WSH_Panel.canClaimSafehouseText then rb = 0 end

    self:drawText(item.text, 10, y + 2, rb, 1, rb, a, self.font);
    return y + self.itemheight;
end


function WSH_Panel:populateList()
    self.safehouseList:clear();
    local safehouses = WSH_Panel:allSafehousesOfClient()
    if not safehouses.is_owner then
        self.safehouseList:addItem(WSH_Panel.canClaimSafehouseText, nil);
    else
        for _,v in ipairs(safehouses.owns) do
            self.safehouseList:addItem(v:getTitle() .. " (" .. v:getOwner() .. ")", v);
        end
    end
    for _,v in ipairs(safehouses.guestOf) do
        self.safehouseList:addItem(v:getTitle() .. " (" .. v:getOwner() .. ")", v);
    end
end


function WSH_Panel:prerender()
    ISPanel.prerender(self)
    self:drawTextCentre("Safehouses", self.headerTitle.left + (self.headerTitle.width/2), self.headerTitle.top, 1, 1, 1, 1, UIFont.Medium)
    self.safehouseList.mouseOverButton = nil
end


function WSH_Panel:onClose()
    WSH_Panel.instance = nil
    self:removeFromUIManager()
end


local function rebindShow()
    WSH_Panel:show()
end

local original_ISUserPanelUI_create = ISUserPanelUI.create
function ISUserPanelUI:create()
    original_ISUserPanelUI_create(self)
    if self.safehouseBtn then
        self.safehouseBtn:setOnClick(rebindShow)
    end
end

function WSH_Panel.OnSafehousesChanged()
    local list = SafeHouse.getSafehouseList()
    for i=0,list:size()-1 do
		local safe = list:get(i);
        local player_list = safe:getPlayers()
        local owner = safe:getOwner()
        if owner and not player_list:contains(owner) then
            player_list:add(owner)
        end
    end
    if WSH_Panel.instance then
        WSH_Panel.instance:populateList()
    end
end

function ISSafehouseUI:onClickRespawn(clickedOption, enabled)
    local username = self.player:getUsername()
    self.safehouse:setRespawnInSafehouse(enabled, username);
    if enabled then
        local safehouses = WSH_Panel:allSafehousesOfClient()
        for _,v in ipairs(safehouses.owns) do
            if v ~= self.safehouse then
                v:setRespawnInSafehouse(false, username);
            end
        end
        for _,v in ipairs(safehouses.guestOf) do
            if v ~= self.safehouse then
                v:setRespawnInSafehouse(false, username);
            end
        end
    end
end

function ISSafehouseAddPlayerUI:populateList()
    local sh_player_list = self.safehouse:getPlayers()
    self.playerList:clear();
    if not self.scoreboard then return end
    for i=1,self.scoreboard.usernames:size() do
        local username = self.scoreboard.usernames:get(i-1)
        local displayName = self.scoreboard.displayNames:get(i-1)
        local is_part_of_same_safehouse = sh_player_list:contains(username)
        local is_owner = self.safehouse:getOwner() == username
        local already_an_owner = false
        local list = SafeHouse.getSafehouseList()
        if not isAdmin() then
            for i=0,list:size()-1 do
                local safe = list:get(i);
                if safe:getOwner() == username then
                    already_an_owner = true
                end
            end
        end

        local add
        if self.changeOwnership then
            add = not is_owner and (not already_an_owner or isAdmin())
        else
            add = not is_part_of_same_safehouse
        end

        if add then
            local newPlayer = {};
            newPlayer.username = username;
            self.playerList:addItem(displayName, newPlayer);
        end
    end
end

function ISSafehouseAddPlayerUI:onClick(button)
    if button.internal == "CANCEL" then
        self:setVisible(false);
        self:removeFromUIManager();
        ISSafehouseAddPlayerUI.instance = nil
    end
    if button.internal == "ADDPLAYER" then
        if not self.changeOwnership then
            local modal = ISModalDialog:new(0,0, 350, 150, getText("IGUI_FactionUI_InvitationSent",self.selectedPlayer), false, nil, nil);
            modal:initialise()
            modal:addToUIManager()
            sendSafehouseInvite(self.safehouse, self.player, self.selectedPlayer);
        else
            self.safehouse:setOwner(self.selectedPlayer);
            self.safehouse:addPlayer(self.selectedPlayer)
            self.safehouse:syncSafehouse();
            if self.player:getX() >= self.safehouse:getX() - 1 and self. player:getX() < self.safehouse:getX2() + 1 and self.player:getY() >= self.safehouse:getY() - 1 and self.player:getY() < self.safehouse:getY2() + 1 then
                self.safehouse:kickOutOfSafehouse(self.player);
            end
            self.safehouseUI:populateList();
            self:setVisible(false);
            self:removeFromUIManager();
            ISSafehouseAddPlayerUI.instance = nil
        end
    end
end

ISSafehouseUI.ReceiveSafehouseInvite = function(safehouse, host)
    if ISSafehouseUI.inviteDialogs[host] then
        if ISSafehouseUI.inviteDialogs[host]:isReallyVisible() then return end
        ISSafehouseUI.inviteDialogs[host] = nil
    end

    local modal = ISModalDialog:new(getCore():getScreenWidth() / 2 - 175,getCore():getScreenHeight() / 2 - 75, 350, 150, getText("IGUI_SafehouseUI_Invitation", host), true, nil, ISSafehouseUI.onAnswerSafehouseInvite);
    modal:initialise()
    modal:addToUIManager()
    modal.safehouse = safehouse;
    modal.host = host;
    modal.moveWithMouse = true;
    ISSafehouseUI.inviteDialogs[host] = modal
end

local original_ISWorldObjectContextMenu_createMenu = ISWorldObjectContextMenu.createMenu
ISWorldObjectContextMenu.createMenu = function(player, worldobjects, x, y, test)
    local menu = original_ISWorldObjectContextMenu_createMenu(player, worldobjects, x, y, test)
    if getAccessLevel() ~= "overseer" then
        for _,v in ipairs(menu.options) do
            if v.onSelect == ISWorldObjectContextMenu.onTakeSafeHouse and v.toolTip then
                local reason = v.toolTip.description:gsub("\r\n", "\n")
                local already_have = getText("IGUI_Safehouse_AlreadyHaveSafehouse")
                if string.find(reason, getText("IGUI_Safehouse_AlreadyHaveSafehouse")) ~= nil then
                    local safehouses = WSH_Panel:allSafehousesOfClient()
                    if safehouses.is_owner and not isAdmin() then
                        v.toolTip.description = reason:gsub(already_have:gsub("[%(%)%.%%%+%-%*%?%[%^%$%]]", "%%%1"), "You are already an owner of a safehouse")
                    else
                        v.toolTip.description = reason:gsub(already_have:gsub("[%(%)%.%%%+%-%*%?%[%^%$%]]", "%%%1") .. "\n", "")
                        if v.toolTip.description == "" then
                            v.notAvailable = false
                            v.toolTip = nil
                        end
                    end
                end
            end
        end
    end
    return menu
end

local function tempOwnerName()
    local charset = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890"
	local ret = {}
	local r
	for i = 1, 6 do
		r = ZombRandBetween(1, #charset)
		table.insert(ret, charset:sub(r, r))
	end
	return "_DELETE_THIS_SH_" .. table.concat(ret)
end

local function OnServerCommand(module, command, args)
    if module ~= "WastelandSafehouses" then return end

    Events.OnServerCommand.Remove(OnServerCommand)

    local player = getPlayer()

    if command == "SyncSuccess" then
        local x = math.floor(tonumber(args[1])) or 0
        local y = math.floor(tonumber(args[2])) or 0
        local w = math.floor(tonumber(args[3])) or 0
        local h = math.floor(tonumber(args[4])) or 0
        local temp_owner_name = args[5]
        local safehouse = SafeHouse.getSafeHouse(x,y,w,h)
        if safehouse and safehouse:getOwner() == temp_owner_name then
            safehouse:setOwner(player:getUsername())
            safehouse:addPlayer(player:getUsername())
            safehouse:removePlayer(temp_owner_name);
        end
    end

    if command == "SyncFailed" then
        player:addLineChatElement("Something went wrong. Please relog.", 1, 0, 0)
    end
end

ISWorldObjectContextMenu.onTakeSafeHouse = function(worldobjects, square, player)
    local reason = SafeHouse.canBeSafehouse(square, getSpecificPlayer(player)):gsub("\r?\n", "")
    local already_have = getText("IGUI_Safehouse_AlreadyHaveSafehouse")
    if reason == "" or reason == already_have then
        if reason == "" or getAccessLevel() == "overseer" then
            SafeHouse.addSafeHouse(square, getSpecificPlayer(player));
        else
            local safehouses = WSH_Panel:allSafehousesOfClient()
            if safehouses.is_owner and not isAdmin() then
                return
            end
            local x = square:getBuilding():getDef():getX()-2
            local y = square:getBuilding():getDef():getY()-2
            local w = square:getBuilding():getDef():getW()+2*2
            local h = square:getBuilding():getDef():getH()+2*2
            local temp_owner_name = tempOwnerName()
            SafeHouse.addSafeHouse(x, y, w, h, temp_owner_name, false);
            local cmd = {x,y,w,h,temp_owner_name,0}
            Events.OnServerCommand.Add(OnServerCommand)
            sendClientCommand(getSpecificPlayer(player), "WastelandSafehouses", "CreateNew", cmd)
        end
    end
end

local function admintool_setSafehouseData(_title, _owner, _x, _y, _w, _h)
	local playerObj = getSpecificPlayer(0);
	local safeObj = SafeHouse.addSafeHouse(_x, _y, _w, _h, _owner, false);
	safeObj:setTitle(_title);
	safeObj:setOwner(_owner);
    safeObj:addPlayer(_owner)
	safeObj:updateSafehouse(playerObj);
	safeObj:syncSafehouse();
end

function ISAddSafeZoneUI:onClick(button)
	if button.internal == "OK" then
		self.creatingZone = false;
		self:setVisible(false);
		self:removeFromUIManager();
		local setX = math.floor(math.min(self.X1, self.X2));
		local setY = math.floor(math.min(self.Y1, self.Y2));
		local setW = math.floor(math.abs(self.X1 - self.X2) + 1);
		local setH = math.floor(math.abs(self.Y1 - self.Y2) + 1);
		admintool_setSafehouseData(self.titleEntry:getInternalText(), self.ownerEntry:getInternalText(), setX, setY, setW, setH)
		return;
	end
	if button.internal == "STARTINGPOINT" then
		self:redefineStartingPoint();
	end;
	if button.internal == "CANCEL" then
		self.creatingZone = false;
		self:setVisible(false);
		self:removeFromUIManager();
		return;
	end;
end
Events.OnSafehousesChanged.Add(WSH_Panel.OnSafehousesChanged)
Events.ReceiveSafehouseInvite.Add(ISSafehouseUI.ReceiveSafehouseInvite);
Events.AcceptedSafehouseInvite.Add(WSH_Panel.OnSafehousesChanged);