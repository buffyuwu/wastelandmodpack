---
--- BiofuelLootTables.lua
--- 25/11/2022
---
require 'Items/SuburbsDistributions'
require "Items/ProceduralDistributions"

table.insert(SuburbsDistributions["all"]["inventorymale"].items, "warpbiofuel.ContaminatedSample");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 9);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "warpbiofuel.ContaminatedSample");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 9);

table.insert(SuburbsDistributions["all"]["inventorymale"].items, "warpbiofuel.ContaminatedSample2");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 3);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "warpbiofuel.ContaminatedSample2");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 3);

table.insert(SuburbsDistributions["all"]["inventorymale"].items, "warpbiofuel.DirtyPetridish");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 2);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "warpbiofuel.DirtyPetridish");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 2);

table.insert(ProceduralDistributions.list["MedicalClinicTools"].items, "warpbiofuel.BoxOfPetriDishes");
table.insert(ProceduralDistributions.list["MedicalClinicTools"].items, 10);
table.insert(ProceduralDistributions.list["SafehouseMedical"].items, "warpbiofuel.BoxOfPetriDishes");
table.insert(ProceduralDistributions.list["SafehouseMedical"].items, 10);
table.insert(ProceduralDistributions.list["ArmyStorageMedical"].items, "warpbiofuel.BoxOfPetriDishes");
table.insert(ProceduralDistributions.list["ArmyStorageMedical"].items, 10);

table.insert(SuburbsDistributions["all"]["medicine"].items, "warpbiofuel.BoxOfPetriDishes");
table.insert(SuburbsDistributions["all"]["medicine"].items, 6);

table.insert(ProceduralDistributions.list["ToolStoreMisc"].items, "warpbiofuel.BioreactorConsumer");
table.insert(ProceduralDistributions.list["ToolStoreMisc"].items, 1);
table.insert(ProceduralDistributions.list["GarageTools"].items, "warpbiofuel.BioreactorConsumer");
table.insert(ProceduralDistributions.list["GarageTools"].items, 1);
table.insert(ProceduralDistributions.list["StoreShelfMechanics"].items, "warpbiofuel.BioreactorConsumer");
table.insert(ProceduralDistributions.list["StoreShelfMechanics"].items, 1);
table.insert(ProceduralDistributions.list["ToolStoreFarming"].items, "warpbiofuel.BioreactorConsumer");
table.insert(ProceduralDistributions.list["ToolStoreFarming"].items, 1);
table.insert(ProceduralDistributions.list["CrateTools"].items, "warpbiofuel.BioreactorConsumer");
table.insert(ProceduralDistributions.list["CrateTools"].items, 1);
