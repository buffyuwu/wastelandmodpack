---
--- BiofuelRecipes.lua
--- 24/11/2022
---
---@param items Food (Mostly)
---@param result DrainableComboItem
function Recipe.OnCreate.CreateBiofuelGasoline(items, result, player)
	local fuelMade = 0.0

	for i=0,items:size() - 1 do
		local item = items:get(i)
		if(item:getType() == "PetrolCan") then
			fuelMade = item:getUsedDelta()
		end
	end

	for i=0,items:size() - 1 do
		local item = items:get(i)
		if(item:getType() == "Z2Bacteria") then
			local unhappy = item:getUnhappyChange()
			if(unhappy) then
				if(unhappy >= 20) then
					fuelMade = fuelMade + 0.05
				elseif(unhappy >= 10) then
					fuelMade = fuelMade + 0.2
				else
					fuelMade = fuelMade + 0.35
				end
			end
			player:getInventory():AddItem("warpbiofuel.DirtyPetridish");
		end
	end

	if player:getInventory():containsTypeRecurse("warpbiofuel.ResearchNotes") then
		fuelMade = fuelMade * 1.5
	end

	if(result) then
		if(fuelMade > 1.0) then
			fuelMade = 1.0
		end
		result:setUsedDelta(fuelMade)
	end
end


function Recipe.OnCreate.CreateBiofuelPropane(items, result, player)
	local fuelMade = 0.0
	for i=0,items:size() - 1 do
		local item = items:get(i)
		if(item:getType() == "PropaneTank") then
			fuelMade = item:getUsedDelta()
		end
	end

	for i=0,items:size() - 1 do
		local item = items:get(i)
		if(item:getType() == "Z5Bacteria") then
			local unhappy = item:getUnhappyChange()
			if(unhappy) then
				if(unhappy >= 20) then
					fuelMade = fuelMade + 0.02
				elseif(unhappy >= 10) then
					fuelMade = fuelMade + 0.1
				else
					fuelMade = fuelMade + 0.2
				end
			end
			player:getInventory():AddItem("warpbiofuel.DirtyPetridish");
		end
	end

	if player:getInventory():containsTypeRecurse("warpbiofuel.ResearchNotes") then
		fuelMade = fuelMade * 1.5
	end

	if(result) then
		if(fuelMade > 1.0) then
			fuelMade = 1.0
		end
		result:setUsedDelta(fuelMade)
	end
end

function Recipe.OnCreate.CultivateBacteria(items, result, player)
	if(result) then
		result:setAge(0)
	end
end