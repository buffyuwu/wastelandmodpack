WAT_ShowCoords = false

local function ShowCoords()
    WAT_ShowCoords = getPlayer():getModData().WAT_ShowCoords or true
    local font = UIFont.AutoNormSmall
    local text = "Cell: 000,000 (000,000)"
    local textWidth = getTextManager():MeasureStringX(font, text)
    local textHeight = getTextManager():MeasureStringY(font, text)
    local WAT_Coords_instance = ISUIElement:new(getCore():getScreenWidth() - textWidth, 0, textWidth, textHeight)
    local player = getPlayer()
    local isSinglePlayer = not isClient()
    function WAT_Coords_instance:render()
        if WAT_ShowCoords and (isSinglePlayer or player:getAccessLevel() ~= "None") then
            local player = getPlayer()
            local x = math.floor(player:getX())
            local y = math.floor(player:getY())
            local cellX = math.floor(x / 300)
            local cellY = math.floor(y / 300)
            local cOffsetX = (x - cellX * 300)
            local cOffsetY = y - cellY * 300
            local text = "Pos: " .. x .. "," .. y
            local text2 = "Cell: " .. cellX .. "," .. cellY .. " (" .. cOffsetX .. "," .. cOffsetY .. ")"
            self:drawText(text, 0, -2, 1.0, 1.0, 1.0, 1.0, font)
            self:drawText(text2, 0, textHeight, 1.0, 1.0, 1.0, 1.0, font)
        end
    end
    WAT_Coords_instance:initialise()
    WAT_Coords_instance:addToUIManager()
end

Events.OnGameStart.Add(ShowCoords)