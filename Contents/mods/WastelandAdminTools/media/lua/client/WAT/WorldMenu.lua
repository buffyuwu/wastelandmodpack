require "WAT/ItemAudit"
require "WAT/GroundCleaner"
require "WAT/Coords"

local WAT_WorldMenu = {}

function WAT_WorldMenu.doMenu(playerIdx, context)
    if not isClient() or isAdmin() then
        local option = context:addOption("WL Admin", nil, nil)
        local submenu = ISContextMenu:getNew(context)
        context:addSubMenu(option, submenu)
        submenu:addOption("Item Audit" , nil, WAT_WorldMenu.showItemAuditWindow)
        submenu:addOption("Ground Cleaner" , nil, WAT_WorldMenu.showGroundCleanerWindow)
        if WAT_ShowCoords then
            submenu:addOption("Hide Coords" , nil, WAT_WorldMenu.toggleCoords)
        else
            submenu:addOption("Show Coords" , nil, WAT_WorldMenu.toggleCoords)
        end
    end
end

function WAT_WorldMenu.showItemAuditWindow()
    WAT_ItemAudit:display()
end

function WAT_WorldMenu.showGroundCleanerWindow()
    WAT_GroundCleaner:display()
end

function WAT_WorldMenu.toggleCoords()
    WAT_ShowCoords = not WAT_ShowCoords
end

Events.OnFillWorldObjectContextMenu.Add(WAT_WorldMenu.doMenu)