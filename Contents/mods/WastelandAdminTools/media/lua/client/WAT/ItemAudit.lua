require "GravyUI"
require "GroundHightlighter"

WAT_ItemAudit = ISPanelJoypad:derive("WAT_ItemAudit")
WAT_ItemAudit.instance = nil

function WAT_ItemAudit:display()
    if WAT_ItemAudit.instance ~= nil then
        WAT_ItemAudit.instance:close()
    end
    local o = ISPanelJoypad.new(self, 200, 200, 400, 400)
    o:initialise()
    WAT_ItemAudit.instance = o
end

function WAT_ItemAudit:initialise()
    ISPanelJoypad.initialise(self)
    self:addToUIManager()
    self:setAlwaysOnTop(true)
    self.moveWithMouse = true
    self.borderColor = {r=0.4, g=0.4, b=0.4, a=1}
    self.backgroundColor = {r=0, g=0, b=0, a=1}
    self.groundHighlighter = GroundHightlighter:new()
    self.groundHighlighter:enableXray(true)
    self.rangeX = 20
    self.rangeY = 20
    self.rangeMax = 200
    self.rangeMin = 5
    self.showOverlay = false

    local window = GravyUI.Node(self.width, self.height):pad(2)
    local header, rangeXSlot, rangeYSlot, buttons, log = window:rows({20, 12, 12, 20, 1}, 5)

    local scanButton, toggleButton, writeButton, closeButton = buttons:cols(4, 3)
    local rangeXLabel, rangeXInput = rangeXSlot:cols({0.3, 0.7}, 2)
    local rangeYLabel, rangeYInput = rangeYSlot:cols({0.3, 0.7}, 2)

    self.headerLabel = header
    self.rangeXLabel = rangeXLabel
    self.rangeYLabel = rangeYLabel

    self.rangeXInput = rangeXInput:makeSlider(self, self.rangeXInputChange)
    self.rangeYInput = rangeYInput:makeSlider(self, self.rangeYInputChange)

    self.scanButton = scanButton:makeButton("Scan", self, self.scan)
    self.toggleOverlayButton = toggleButton:makeButton("Toggle Overlay", self, self.toggleOverlay)
    self.writeButton = writeButton:makeButton("Write", self, self.write)
    self.closeButton = closeButton:makeButton("Close", self, self.close)

    self.logTextBox = log:makeTextBox("Log")

    self:addChild(self.rangeXInput)
    self:addChild(self.rangeYInput)
    self:addChild(self.scanButton)
    self:addChild(self.toggleOverlayButton)
    self:addChild(self.writeButton)
    self:addChild(self.closeButton)
    self:addChild(self.logTextBox )

    self.rangeXInput:setCurrentValue(self.rangeX)
    self.rangeXInput:setValues(self.rangeMin, self.rangeMax, 1, 5, false)
    self.rangeYInput:setCurrentValue(self.rangeY)
    self.rangeYInput:setValues(self.rangeMin, self.rangeMax, 1, 5, false)

    self.logTextBox:setMultipleLine(true)
    self.logTextBox:setEditable(true)
    self.logTextBox:setSelectable(true)
	self.logTextBox:addScrollBars()
end

function WAT_ItemAudit:rangeXInputChange()
    self.rangeX = self.rangeXInput:getCurrentValue()
    if self.groundHighlighter.type ~= "none" then
        self.groundHighlighter:remove()
        self:toggleOverlay()
    end
end

function WAT_ItemAudit:rangeYInputChange()
    self.rangeY = self.rangeYInput:getCurrentValue()
    if self.groundHighlighter.type ~= "none" then
        self.groundHighlighter:remove()
        self:toggleOverlay()
    end
end

function WAT_ItemAudit:prerender()
    self:drawRect(0, 0, self.width, self.height, self.backgroundColor.a, self.backgroundColor.r, self.backgroundColor.g, self.backgroundColor.b)
    self:drawRectBorder(0, 0, self.width, self.height, self.borderColor.a, self.borderColor.r, self.borderColor.g, self.borderColor.b)

    self:drawText("Item Audit", self.headerLabel.left, self.headerLabel.top, 1, 1, 1, 1, UIFont.Medium)
    self:drawText("Range West/East", self.rangeXLabel.left, self.rangeXLabel.top, 1, 1, 1, 1, UIFont.Small)
    self:drawText("Range North/South", self.rangeYLabel.left, self.rangeYLabel.top, 1, 1, 1, 1, UIFont.Small)
end

function WAT_ItemAudit:render()
	ISPanelJoypad.render(self)
end

function WAT_ItemAudit:scan()
    local sx, sy, ex, ey = self:getRange()
    local itemsByCategory = {}
    local itemsByName = {}
    local totalItems = 0
    local totalItemsOnGround = 0
    local totalItemsInContainers = 0
    local totalItemInCars = 0
    local numContainers = 0
    local numCars = 0
    local numInBags = 0
    local checkedSquares = 0
    local iSquare, objects, movingObjects, worldObjects, object, container, item

    local function checkItem(itemToCheck, fromBag)
        local category = itemToCheck:getDisplayCategory()
        local name = itemToCheck:getName()
        itemsByCategory[category] = (itemsByCategory[category] or 0) + 1
        itemsByName[name] = (itemsByName[name] or 0) + 1
        totalItems = totalItems + 1
        if fromBag then
            numInBags = numInBags + 1
        end

        if instanceof(itemToCheck, "InventoryContainer") then
            local container = itemToCheck:getItemContainer()
            if container then
                local items = container:getItems()
                if items then
                    for i = 0, items:size() - 1 do
                        local innerItem = items:get(i)
                        if innerItem then
                            checkItem(innerItem, true)
                        end
                    end
                end
            end
        end
    end

    local cell = getCell()
    for x = sx, ex do
    for y = sy, ey do
    for z = 0,  5  do
        iSquare = cell:getGridSquare(x, y, z)
        if iSquare then
            checkedSquares = checkedSquares + 1
            worldObjects = iSquare:getWorldObjects()
            if worldObjects then
                for j = 0, worldObjects:size() - 1 do
                    object = worldObjects:get(j):getItem()
                    if object then
                        checkItem(object)
                        totalItemsOnGround = totalItemsOnGround + 1
                    end
                end
            end

            objects = iSquare:getObjects()
            if objects then
                for j = 0, objects:size() - 1 do
                    object = objects:get(j)
                    if object then
                        container = object:getContainer()
                        if container then
                            numContainers = numContainers + 1
                            for k = 0, container:getItems():size() - 1 do
                                item = container:getItems():get(k)
                                if item then
                                    checkItem(item)
                                    totalItemsInContainers = totalItemsInContainers + 1
                                end
                            end
                        end
                    end
                end
            end

            movingObjects = iSquare:getMovingObjects()
            if movingObjects then
                for j = 0, movingObjects:size() - 1 do
                    object = movingObjects:get(j)
                    if instanceof(object, "BaseVehicle") then
                        numCars = numCars + 1
                        for k = 0, object:getPartCount() - 1 do
                            local part = object:getPartByIndex(k)
                            if part then
                                container = part:getItemContainer()
                                if container then
                                    for l = 0, container:getItems():size() - 1 do
                                        item = container:getItems():get(l)
                                        if item then
                                            checkItem(item)
                                            totalItemInCars = totalItemInCars + 1
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end end end

    self.logTextBox:clear()

    local byCategoryLines = {}
    for category, count in pairs(itemsByCategory) do
        table.insert(byCategoryLines, string.format("%7d: %s", count, category))
    end
    table.sort(byCategoryLines)

    local byItemLines = {}
    for name, count in pairs(itemsByName) do
        table.insert(byItemLines, string.format("%7d: %s", count, name))
    end
    table.sort(byItemLines)

    local lines = {}
    local player = getPlayer()
    table.insert(lines, string.format("Scanned %d squares from %d,%d with range %dx%d", checkedSquares, player:getX(), player:getY(), self.rangeX, self.rangeY))
    table.insert(lines, string.format("%d items found in %d Categories", totalItems, #byCategoryLines))
    table.insert(lines, string.format("%d on the ground, %d in %d containers, %d in %d vehicles, %d in bags", totalItemsOnGround, totalItemsInContainers, numContainers, totalItemInCars, numCars, numInBags))
    table.insert(lines, "")
    table.insert(lines, "===By Category===")
    table.insert(lines, "")
    for i=#byCategoryLines,1,-1 do
        table.insert(lines, byCategoryLines[i])
    end
    table.insert(lines, "")
    table.insert(lines, "===By Item===")
    table.insert(lines, "")
    for i=#byItemLines,1,-1 do
        table.insert(lines, byItemLines[i])
    end
    self.logTextBox:setText(table.concat(lines, "\n"))
end

function WAT_ItemAudit:toggleOverlay()
    if self.groundHighlighter.type == "none" then
        local sx, sy, ex, ey = self:getRange()
        self.groundHighlighter:highlightSquare(sx, sy, ex, ey)
    else
        self.groundHighlighter:remove()
    end
end

function WAT_ItemAudit:close()
    self:setVisible(false)
    self:removeFromUIManager()
    WAT_ItemAudit.instance = nil
end

function WAT_ItemAudit:removeFromUIManager()
    self.groundHighlighter:remove()
    ISPanelJoypad.removeFromUIManager(self)
end

function WAT_ItemAudit:getRange()
    local square = getPlayer():getCurrentSquare()
    if not square then
        return 0, 0, 0, 0
    end
    local start_x = square:getX()
    local start_y = square:getY()
    return start_x - self.rangeX, start_y - self.rangeY,
           start_x + self.rangeX, start_y + self.rangeY
end

function WAT_ItemAudit:write()
    local timestamp = getTimestamp()
    local filename = "ItemAudit_" .. timestamp .. ".txt"
    local writer = getFileWriter(filename, true, false)
    writer:write(self.logTextBox.text)
    writer:close()
    getPlayer():setHaloNote("Saved to " .. filename)
end


local function map_override(self)
    if not WAT_ItemAudit.instance or self.isometric then
        return
    end
    local sx, sy, ex, ey = WAT_ItemAudit.instance:getRange()

    local tlX = self.mapAPI:worldToUIX(sx, sy)
    local tlY = self.mapAPI:worldToUIY(sx, sy)
    local brX = self.mapAPI:worldToUIX(ex, ey)
    local brY = self.mapAPI:worldToUIY(ex, ey)
    self:drawRect(tlX, tlY, brX - tlX, brY - tlY, 0.5, 0, 1, 0)
end

local original_ISWorldMap_render = ISWorldMap.render
function ISWorldMap:render()
    original_ISWorldMap_render(self)
    map_override(self)
end