require 'Items/ProceduralDistributions'

local function registerAsLoot(item, chance, allocation)
  table.insert(ProceduralDistributions.list[allocation].items, item);
  table.insert(ProceduralDistributions.list[allocation].items, chance);
end

registerAsLoot("Base.RubberBushSeedBag", 4, "CrateFarming");
registerAsLoot("Base.RubberBushSeedBag", 10, "GardenStoreMisc");
registerAsLoot("Base.RubberBushSeedBag", 4, "GigamartFarming");
registerAsLoot("Base.RubberBushSeedBag", 3, "ToolStoreFarming");

registerAsLoot("Base.SuspensionSpring", 2, "CrateMechanics");
registerAsLoot("Base.SuspensionSpring", 4, "CrateMetalwork");
registerAsLoot("Base.SuspensionSpring", 1, "GarageMechanics");
registerAsLoot("Base.SuspensionSpring", 4, "GarageMetalwork");
registerAsLoot("Base.SuspensionSpring", 10, "MechanicShelfSuspension");
registerAsLoot("Base.SuspensionSpring", 1, "MechanicSpecial");
registerAsLoot("Base.SuspensionSpring", 4, "StoreShelfMechanics");
registerAsLoot("Base.SuspensionSpring", 0.1, "CrateRandomJunk");

registerAsLoot("Base.CuredRubber", 0.1, "CrateRandomJunk");
registerAsLoot("Base.CuredRubber", 5, "DrugShackTools");
registerAsLoot("Base.CuredRubber", 4, "JanitorTools");
registerAsLoot("Base.CuredRubber", 6, "MechanicShelfMisc");
registerAsLoot("Base.CuredRubber", 6, "MechanicShelfWheels");
registerAsLoot("Base.CuredRubber", 1, "MechanicSpecial");

