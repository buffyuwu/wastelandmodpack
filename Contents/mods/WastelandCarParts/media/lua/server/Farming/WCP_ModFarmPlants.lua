require "Farming/SFarmingSystem"
require "Farming/farming_vegetableconf"
require "Farming/SGFarmingSystem"

if isClient() then return end

local originalSFarmingSystem_growPlant = SFarmingSystem.growPlant;
function SFarmingSystem:growPlant(luaObject, nextGrowing, updateNbOfGrow)
    if luaObject.state == "seeded" and luaObject.typeOfSeed == "WCPRubberBush" then
		    local new = luaObject.nbOfGrow <= 0
            luaObject = farming_vegetableconf.growRubberBush(luaObject, nextGrowing, updateNbOfGrow)
		    -- maybe this plant gonna be disease
            if not new and luaObject.nbOfGrow > 0 then
                self:diseaseThis(luaObject, true)
            end
            luaObject.nbOfGrow = luaObject.nbOfGrow + 1
    else
        originalSFarmingSystem_growPlant(self, luaObject, nextGrowing, updateNbOfGrow)
    end
end