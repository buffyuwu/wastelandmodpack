---
--- FirearmAttachmentAppender.lua
--- 02/07/2023
---

local function addAttachmentSlots(weapon, attachment)
	local item = ScriptManager.instance:getItem(attachment)
	if item then
		local itemInstance = item:InstanceItem(nil)
		if itemInstance and instanceof(itemInstance, "WeaponPart") and itemInstance:getMountOn() then
			local mountOnOptions = {}
			for i=0, itemInstance:getMountOn():size()-1 do
				table.insert(mountOnOptions, itemInstance:getMountOn():get(i))
			end
			table.insert(mountOnOptions, weapon)
			local mountOnString = table.concat(mountOnOptions, "; ")
			item:DoParam("MountOn = " .. mountOnString)
		end
	end
end

local attachmentDefinitions = {
	["Base.HandcraftedRifle"] = {"Base.Sling", "Base.Sling_Leather", "Base.Sling_Olive", "Base.Sling_Camo",
	                  "Base.ImprovisedSilencer", "Base.Silencer_PopBottle"}
}

for weapon, slots in pairs(attachmentDefinitions) do
	for _, slot in ipairs(slots) do
		addAttachmentSlots(weapon, slot)
	end
end