---
--- FirearmRecipes.lua
--- 02/07/2023
---

---@param result InventoryItem
---@param player IsoPlayer
function Recipe.OnCreate.disassembleRifle(items, result, player)
	if not result then return end -- Shouldn't happen
	if not player then return end -- Shouldn't happen

	if ZombRand(0, 3) > 0 then -- 2/3 chance we get it back as ZombRand returns 0, 1, or 2
		player:getInventory():AddItem("Base.LongGunBarrel");
	end

	if ZombRand(0, 3) > 0 then  -- 2/3 chance we get it back as ZombRand returns 0, 1, or 2
		player:getInventory():AddItem("Base.HandCraftedRifleStock");
	end

	if ZombRand(0, 4) > 2 then  -- 1/4 chance we get an extra component
		player:getInventory():AddItem("Base.MachinedFirearmComponents");
	end

	local springs = ZombRand(0, 4)  -- Give back 0 to 3 springs
	for i = 1, springs do
		player:getInventory():AddItem("Base.ClipSpring");
	end

end