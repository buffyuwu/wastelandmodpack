module Base
{
	fixing FixHandcraftedRifle
    {
		Require : HandcraftedRifle,
		Fixer : GunToolKit=1; Aiming=5,
		Fixer : ImprovisedGunToolKit=2; Aiming=2,
		Override:true,
    }

	recipe CraftClipSprings
	{
		Wire=1,
		keep WoodenStick,
		SkillRequired:MetalWelding=4,
		Result:ClipSpring=3,
        Category:Welding,
		Time:100,
		OnGiveXP:Recipe.OnGiveXP.MetalWelding10,
		NeedToBeLearn:true,
	}

	recipe CraftLongGunBarrel
	{
		MetalPipe,
		SmallSheetMetal,
		BlowTorch=4,
		keep [Recipe.GetItemTypes.WeldingMask],
		Result:LongGunBarrel,
		Category:Welding,
		OnGiveXP:Recipe.OnGiveXP.MetalWelding10,
		Time:300,
		AnimNode:BlowTorchFloor,
        Sound:BlowTorch,
        Prop1:BlowTorch,
		NeedToBeLearn:true,
	}

	recipe CraftHandmadeRifleStock
    {
        Log,
        keep [Recipe.GetItemTypes.Saw],
        keep [Recipe.GetItemTypes.SharpKnife]/SharpedStone/MeatCleaver,
        OnGiveXP:Recipe.OnGiveXP.WoodWork5,
        Time:400.0,
        SkillRequired:Woodwork=8,
        Category:Carpentry,
        Result:HandCraftedRifleStock,
        Sound:Sawing,
        AnimNode:Disassemble,
        NeedToBeLearn:true,
    }

	recipe CraftHandmadeRifleClip
    {
        MachinedClipSteel=1,
		ClipSpring=3,
		BlowTorch=2,
        keep [Recipe.GetItemTypes.WeldingMask],
        Result:HandcraftedRifleClip = 1,
        Time:220.0,
        Category:Welding,
        SkillRequired:MetalWelding=6;Mechanics=4,
        OnGiveXP:Recipe.OnGiveXP.MetalWelding10,
        AnimNode:BlowTorchFloor,
        Sound:BlowTorch,
        Prop1:BlowTorch,
        NeedToBeLearn:true,
    }

	recipe CraftHandmadeRifle
    {
        LongGunBarrel=1,
        HandCraftedRifleStock=1,
        MachinedFirearmComponents=3,
        ClipSpring=4,
        keep [Recipe.GetItemTypes.Screwdriver],
		keep BallPeenHammer,
		keep Wrench,
        Result:HandcraftedRifle = 1,
        Time:500.0,
        Category:Welding,
        SkillRequired:MetalWelding=10;Mechanics=6,
        OnGiveXP:Recipe.OnGiveXP.MetalWelding25,
        AnimNode:Disassemble,
        Sound:Dismantle,
        Prop1:Screwdriver,
        NeedToBeLearn:true,
    }

    recipe DisassembleHandmadeRifle
    {
		HandcraftedRifle,
		keep [Recipe.GetItemTypes.Screwdriver],
        keep BallPeenHammer,
        keep Wrench,
        Result:MachinedFirearmComponents,
        OnCreate:Recipe.OnCreate.disassembleRifle,
        Time:500.0,
        Category:Welding,
        SkillRequired:MetalWelding=8;Mechanics=4,
        AnimNode:Disassemble,
        Sound:Dismantle,
        Prop1:Screwdriver,
        NeedToBeLearn:true,
    }
}