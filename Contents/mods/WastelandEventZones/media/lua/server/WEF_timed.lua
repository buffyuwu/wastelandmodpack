WEZ_EventZones = WEZ_EventZones or {}

local SPEED_SPRINTER = 1
local SPEED_FAST_SHAMBLER = 2
local SPEED_SLOW_SHAMBLER = 3
local speedField
local defaultSpeed

local function findField(o, fname)
    for i = 0, getNumClassFields(o) - 1 do
        local f = getClassField(o, i)
        if tostring(f) == fname then
        return f
        end
    end
end

local function updateSpeed(zombie, targetSpeed)
    getSandboxOptions():set("ZombieLore.Speed", targetSpeed)
    zombie:makeInactive(true)
    zombie:makeInactive(false)
    getSandboxOptions():set("ZombieLore.Speed", defaultSpeed)
end

local function makeSlowShambler(isoZombie)
    updateSpeed(isoZombie, SPEED_SLOW_SHAMBLER)
end

local function makeFastShambler(isoZombie)
    updateSpeed(isoZombie, SPEED_FAST_SHAMBLER)
end

local function makeSprinter(isoZombie)
    updateSpeed(isoZombie, SPEED_SPRINTER)
end

local function getZombieSpeed(isoZombie)
    return getClassFieldVal(isoZombie, speedField)
end

local function removeZombie(isoZombie)
    isoZombie:removeFromWorld()
    isoZombie:removeFromSquare()
end

local function wasConsidered(isoZombie, sprint, fast, slow)
    local square = isoZombie:getSquare()
    if not square then return false end
    local modData = isoZombie:getModData()
    local x = square:getX()
    local y = square:getY()
    local modX = modData.WEZ_LastX or 0
    local modY = modData.WEZ_LastY or 0
    local modSprint = modData.WEZ_Sprint or 0
    local modFast = modData.WEZ_Fast or 0
    local modSlow = modData.WEZ_Slow or 0
    local dX = math.abs(x - modX)
    local dY = math.abs(y - modY)
    return isoZombie:getModData().WEZ_WasConsidered and dX < 40 and dY < 40 and sprint == modSprint and fast == modFast and slow == modSlow
end

local function setConsidered(isoZombie)
    local square = isoZombie:getSquare()
    if not square then return end
    local modData = isoZombie:getModData()
    modData.WEZ_LastX = square:getX()
    modData.WEZ_LastY = square:getY()
    modData.WEZ_WasConsidered = true
end


local function processZombieInZone(isoZombie, zone)
    if zone.percentageSlowShamblers ~= 0 or zone.percentageSprinters ~= 0 then
        if isClient() and isoZombie:isRemoteZombie() then
            return
        end

        if not wasConsidered(isoZombie) then
            if zone.percentageSprinters > 0 and ZombRand(1, 100) <= zone.percentageSprinters then
                isoZombie:getModData().WEZ_type = SPEED_SPRINTER
            elseif zone.percentageFastShamblers > 0 and ZombRand(1, 100) <= zone.percentageFastShamblers then
                isoZombie:getModData().WEZ_type = SPEED_FAST_SHAMBLER
            elseif zone.percentageSlowShamblers > 0 and ZombRand(1, 100) <= zone.percentageSlowShamblers then
                isoZombie:getModData().WEZ_type = SPEED_SLOW_SHAMBLER
            else
                isoZombie:getModData().WEZ_type = SPEED_FAST_SHAMBLER
            end
        end
        setConsidered(isoZombie, zone.percentageSprinters, zone.percentageFastShamblers, zone.percentageSlowShamblers)
        local type = isoZombie:getModData().WEZ_type
        if type == SPEED_SPRINTER then
            if getZombieSpeed(isoZombie) ~= SPEED_SPRINTER then
                makeSprinter(isoZombie)
            end
        elseif type == SPEED_SLOW_SHAMBLER then
            if getZombieSpeed(isoZombie) ~= SPEED_SLOW_SHAMBLER then
                makeSlowShambler(isoZombie)
            end
        elseif type == SPEED_FAST_SHAMBLER then
            if getZombieSpeed(isoZombie) ~= SPEED_FAST_SHAMBLER then
                makeFastShambler(isoZombie)
            end
        end
    end
end

local function processZombies()
    local zombies = getCell():getZombieList()
    local zombieCount = zombies:size()
    local toRemoveList = {}
    for i = 0, zombieCount - 1 do
        local isoZombie = zombies:get(i)
        local zombieX = isoZombie:getX()
        local zombieY = isoZombie:getY()
        for _, zone in pairs(WEZ_EventZones) do
            if zombieX >= zone.minX and zombieX <= (zone.maxX + 1) and zombieY >= zone.minY and zombieY <= (zone.maxY + 1) then
                if zone.preventZombies then
                    if not isClient() then
                        table.insert(toRemoveList, isoZombie)
                    end
                else
                    processZombieInZone(isoZombie, zone)
                end
            end
        end
    end
    for _, isoZombie in pairs(toRemoveList) do
        removeZombie(isoZombie)
    end
end

local ticksToWait = 60
local function doZombieUpdates()
    if ticksToWait > 0 then
        ticksToWait = ticksToWait - 1
        return
    end

    if speedField == nil then
        speedField = findField(IsoZombie.new(nil), "public int zombie.characters.IsoZombie.speedType")
        defaultSpeed = tonumber(getSandboxOptions():getOptionByName("ZombieLore.Speed"):asConfigOption():getValueAsLuaString())
    end

    ticksToWait = 600
    print("WEZ: Starting zombie update")
    processZombies()
    print("WEZ: Finished zombie update")
end
Events.OnTick.Add(doZombieUpdates)