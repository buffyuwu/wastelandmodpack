if not isServer() then return end

local Json = require "json"

--- @type WEZ_EventZone[]
WEZ_EventZones = WEZ_EventZones or {}
local wereZonesLoaded = false

local function loadFromDisk()
    print("Loading WEZ from disk")
    wereZonesLoaded = true
    local fileReaderObj = getFileReader("WastelandEventZones.json", true)
    local json = ""
    local line = fileReaderObj:readLine()
    while line ~= nil do
        json = json .. line
        line = fileReaderObj:readLine()
    end
    fileReaderObj:close()
    if json and json ~= "" then
        local decoded = Json.Decode(json)
        if decoded then
            WEZ_EventZones = decoded
        end
    end

    for _, zone in pairs(WEZ_EventZones) do
        if not zone.percentageSprinters then
            zone.percentageSprinters = 0
        end
        if not zone.percentageSlowShamblers then
            zone.percentageSlowShamblers = 0
        end
    end
end

local function writeToDisk()
    local fileWriterObj = getFileWriter("WastelandEventZones.json", true, false)
    local json = Json.Encode(WEZ_EventZones)
    fileWriterObj:write(json)
    fileWriterObj:close()
end

local function loadIfNeeded()
    if not wereZonesLoaded then
        loadFromDisk()
    end
end

local function sendZonesToClient(player)
    sendServerCommand(player, "WastelandEventZones", "SyncZones", WEZ_EventZones)
end

local function sendZoneToAll(zoneId)
    sendServerCommand("WastelandEventZones", "SyncZone", WEZ_EventZones[zoneId])
end

local function sendZonesToAll()
    sendServerCommand("WastelandEventZones", "SyncZones", WEZ_EventZones)
end

local Commands = {}

function Commands.SetZone(player, args)
    loadIfNeeded()
    local zoneId = args.id
    WEZ_EventZones[zoneId] = args
    sendZoneToAll(zoneId)
    writeToDisk()
end

function Commands.DeleteZone(player, args)
    loadIfNeeded()
    local zoneId = args.id
    if not zoneId then return end
    WEZ_EventZones[zoneId] = nil
    sendZonesToAll()
    writeToDisk()
end

function Commands.GetZones(player, args)
    loadIfNeeded()
    sendZonesToClient(player)
end

local function processClientCommand(module, command, player, args)
    if module ~= "WastelandEventZones" then return end
    if not Commands[command] then return end
    Commands[command](player, args)
end

Events.OnClientCommand.Add(processClientCommand)
