if not isClient() then return end

require "WEZ_EventZone"

local Commands = {}

local lastTry = 0
local didGetIntialZones = false

local function checkForInitialZones()
    if didGetIntialZones then
        Events.OnTick.Remove(checkForInitialZones);
        return
    end
    if getTimestampMs() - lastTry < 2000 then return end
    lastTry = getTimestampMs()
    sendClientCommand(getPlayer(), "WastelandEventZones", "GetZones", {})
end

local function processServerCommand(module, command, args)
    if module ~= "WastelandEventZones" then return end
    if not Commands[command] then return end
    Commands[command](args)
end

function Commands.SyncZone(args)
    if WEZ_EventZones[args.id] then
        for k,v in pairs(args) do
            WEZ_EventZones[args.id][k] = v
        end
    else
        WEZ_EventZones[args.id] = WEZ_EventZone:loadFrom(args)
    end
end

function Commands.SyncZones(args)
    didGetIntialZones = true

    if args == nil then
        WEZ_EventZones = {}
        return
    end

    local seenZoneIds = {}
    for _, zone in pairs(args) do
        seenZoneIds[zone.id] = true
        if WEZ_EventZones[zone.id] then
            for k,v in pairs(zone) do
                WEZ_EventZones[zone.id][k] = v
            end
        else
            WEZ_EventZones[zone.id] = WEZ_EventZone:loadFrom(zone)
        end
    end
    for id, _ in pairs(WEZ_EventZones) do
        if not seenZoneIds[id] then
            WEZ_EventZones[id] = nil
        end
    end
end

Events.OnServerCommand.Add(processServerCommand)
Events.OnInitWorld.Add(function()
    Events.OnTick.Add(checkForInitialZones)
end)