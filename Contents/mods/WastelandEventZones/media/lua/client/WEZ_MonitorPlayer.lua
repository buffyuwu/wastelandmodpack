if not isClient() then return end

WEZ_MonitorPlayer = {}
WEZ_MonitorPlayer.checkTimeout = 0
WEZ_MonitorPlayer.checkInterval = 20
WEZ_MonitorPlayer.zonesIn = {}
WEZ_MonitorPlayer.zonesWarned = {}
WEZ_MonitorPlayer.jailZoneId = false
WEZ_MonitorPlayer.stillInJail = false
WEZ_MonitorPlayer.cancelRun = false

function WEZ_MonitorPlayer.Check()
    if WEZ_MonitorPlayer.checkTimeout > 0 then
        WEZ_MonitorPlayer.checkTimeout = WEZ_MonitorPlayer.checkTimeout - 1
        return
    end
    WEZ_MonitorPlayer.checkTimeout = WEZ_MonitorPlayer.checkInterval

    local currentlyInZones = {}
    local currentlyWarnedZones = {}
    local player = getPlayer()
    if player then
        if player:isGodMod() then return end
        WEZ_MonitorPlayer.stillInJail = false
        WEZ_MonitorPlayer.cancelRun = false
        local x, y = player:getX(), player:getY()
        local zones = WEZ_EventZone.getZonesAt(x, y)
        for _, zone in pairs(zones) do
            currentlyInZones[zone.id] = true
            if not WEZ_MonitorPlayer.cancelRun then
                WEZ_MonitorPlayer.CheckZone(player, zone)
            end
        end
        WEZ_MonitorPlayer.CheckTPBackToJail(player)
        local warnedZones = WEZ_EventZone.getWarningZonesAt(x, y)
        for _, zone in pairs(warnedZones) do
            if not WEZ_MonitorPlayer.zonesWarned[zone.id] then
                WEZ_MonitorPlayer.zonesWarned[zone.id] = true
                WEZ_MonitorPlayer.showWarning(player, zone)
            end
            currentlyWarnedZones[zone.id] = true
        end
    end

    for zoneId, _ in pairs(WEZ_MonitorPlayer.zonesIn) do
        if not currentlyInZones[zoneId] then
            WEZ_MonitorPlayer.zonesIn[zoneId] = false
        end
    end
    for zoneId, _ in pairs(WEZ_MonitorPlayer.zonesWarned) do
        if not currentlyWarnedZones[zoneId] then
            WEZ_MonitorPlayer.zonesWarned[zoneId] = false
        end
    end
end

function WEZ_MonitorPlayer.CheckZone(player, zone)
    if WEZ_MonitorPlayer.CheckBoot(player, zone) then
        return
    end

    if not WEZ_MonitorPlayer.zonesIn[zone.id] then
        WEZ_MonitorPlayer.zonesIn[zone.id] = true
        zone:onPlayerVisit(player)
    end

    WEZ_MonitorPlayer.CheckJail(player, zone)
    WEZ_MonitorPlayer.CheckTeleport(player, zone)
    WEZ_MonitorPlayer.CheckDamage(player, zone)
    WEZ_MonitorPlayer.CheckRP(player, zone)
end

function WEZ_MonitorPlayer.CheckTPBackToJail(player)
    if WEZ_MonitorPlayer.jailZoneId and not WEZ_MonitorPlayer.stillInJail then
        local targetZone = WEZ_EventZone.getZone(WEZ_MonitorPlayer.jailZoneId)
        if targetZone and targetZone.isJail then
            local x, y = targetZone:getClosestPointInsideZone(player:getX(), player:getY())
            WEZ_MonitorPlayer.TpPlayer(player, x, y, 0)
        else
            WEZ_MonitorPlayer.jailZoneId = false
        end
    end
end

function WEZ_MonitorPlayer.CheckJail(player, zone)
    if zone.isJail then
        WEZ_MonitorPlayer.jailZoneId = zone.id
        WEZ_MonitorPlayer.stillInJail = true
    end
end

function WEZ_MonitorPlayer.CheckBoot(player, zone)
    local isPlayerAllowed, reason = zone:isPlayerAllowed(player)
    if not isPlayerAllowed then
        local x, y = player:getX(), player:getY()
        local newX, newY = zone:getClosestPointOutsideZone(x, y)
        WEZ_MonitorPlayer.jailZoneId = false
        WEZ_MonitorPlayer.cancelRun = true
        WEZ_MonitorPlayer.TpPlayer(player, newX, newY, 0)
        player:setHaloNote(reason, 255, 0, 0, 60.0)
        return true
    end
    return false
end

function WEZ_MonitorPlayer.CheckTeleport(player, zone)
    if zone.teleportX ~= 0 and zone.teleportY ~= 0 then
        if math.floor(zone.teleportX) == math.floor(player:getX()) and math.floor(zone.teleportY) == math.floor(player:getY()) then
            return
        end
        WEZ_MonitorPlayer.jailZoneId = false
        WEZ_MonitorPlayer.cancelRun = true
        WEZ_MonitorPlayer.TpPlayer(player, zone.teleportX, zone.teleportY, 0)
        player:setHaloNote("Whoosh", 0, 255, 0, 60.0)
    end
end

function WEZ_MonitorPlayer.CheckDamage(player, zone)
    if zone.damageRate > 0 then
        local items = {}
        for item in string.gmatch(zone.damagePreventItems, "([^;]+)") do
            table.insert(items, item)
        end
        local wornItems = WEZ_MonitorPlayer.getWornItems(player)
        for _, item in ipairs(items) do
            for _, wornItem in ipairs(wornItems) do
                if item == wornItem then
                    return
                end
            end
        end
        player:getBodyDamage():ReduceGeneralHealth(zone.damageRate/100)
    end
end

function WEZ_MonitorPlayer.getWornItems(player)
    local wornItems = {}
    local playerItems = player:getInventory():getItems()
    for i=0, playerItems:size()-1 do
        local item = playerItems:get(i)
        if player:isEquippedClothing(item) then
            table.insert(wornItems, item:getFullType())
        end
    end
    return wornItems
end

--- @param player IsoPlayer
--- @param zone WEZ_EventZone
function WEZ_MonitorPlayer.CheckRP(player, zone)
    if zone.isRpZone then
        player:getStats():setHunger(0.0)
        player:getStats():setThirst(0.0)
        player:getStats():setFatigue(0.0)
        player:getStats():setThirst(0.0)
        player:getBodyDamage():setBoredomLevel(0)
        player:getBodyDamage():setUnhappynessLevel(0)
        player:getBodyDamage():getThermoregulator():reset()
    end
end

--- @param player IsoPlayer
--- @param zone WEZ_EventZone
function WEZ_MonitorPlayer.showWarning(player, zone)
    if zone.warningMessage == "" then
        return
    end
    player:addLineChatElement(zone.warningMessage, 255, 0, 0)
end

function WEZ_MonitorPlayer.TpPlayer(player, x, y, z)
    local vehicle = player:getVehicle()
    if vehicle then
        if vehicle:getDriver() == player and vehicle:getSpeed2D() > 0 then
            vehicle:setForceBrake()
            return
        end
        vehicle:exit(player)
    end

    player:setX(x)
    player:setY(y)
    player:setZ(z)
    player:setLx(x)
    player:setLy(y)
    player:setLz(z)
end

Events.OnTick.Add(WEZ_MonitorPlayer.Check)