if not isClient() then return end

-- TODO: Add required clothing for a zone

--- @class WEZ_EventZone
WEZ_EventZone = WEZ_EventZone or {}
--- @type WEZ_EventZone[]
WEZ_EventZones = {}

function WEZ_EventZone.getZonesAt(x, y)
    local zones = {}
    for _, zone in pairs(WEZ_EventZones) do
        if zone:isInZone(x, y) then
            table.insert(zones, zone)
        end
    end
    return zones
end

function WEZ_EventZone.getWarningZonesAt(x, y)
    local zones = {}
    for _, zone in pairs(WEZ_EventZones) do
        if zone:isInWarningZone(x, y) then
            table.insert(zones, zone)
        end
    end
    return zones
end

function WEZ_EventZone.getZone(zoneId)
    return WEZ_EventZones[zoneId]
end

function WEZ_EventZone.dump()
    print(WEZ_tabledump(WEZ_EventZones))
end

function WEZ_EventZone:new(name, x1, y1, x2, y2)
    --- @type WEZ_EventZone
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o:init()
    o.name = name
    o.minX = math.min(x1, x2)
    o.minY = math.min(y1, y2)
    o.maxX = math.max(x1, x2)
    o.maxY = math.max(y1, y2)
    o:save()
    return o
end

function WEZ_EventZone:loadFrom(o)
    setmetatable(o, self)

    -- upgrade old zones
    if o.damagePreventItems == nil then o.damagePreventItems = "" end
    if o.percentageSprinters == nil then o.percentageSprinters = 0 end
    if o.percentageFastShamblers == nil then o.percentageFastShamblers = 0 end
    if o.percentageSlowShamblers == nil then o.percentageSlowShamblers = 0 end
    if o.isJail == nil then o.isJail = false end
    if o.warningBuffer == nil then o.warningBuffer = 0 end
    if o.warningMessage == nil then o.warningMessage = "" end

    self.__index = self
    return o
end

function WEZ_EventZone:init()
    --- @type string
    self.id = getRandomUUID()
    self.name = ""
    self.minX = 0
    self.minY = 0
    self.maxX = 0
    self.maxY = 0
    self.isAdminOnly = false
    self.isJail = false
    self.teleportX = 0
    self.teleportY = 0
    self.damageRate = 0
    self.damagePreventItems = ""
    self.preventZombies = false
    self.minCharecterAgeDays = 0
    self.maxCharecterVisits = 0
    self.minTimeBetweenVisitsHours = 0
    self.isRpZone = false
    self.playerVisits = {}
    self.percentageSprinters = 0
    self.percentageFastShamblers = 0
    self.percentageSlowShamblers = 0
    self.warningBuffer = 0
    self.warningMessage = ""
end

function WEZ_EventZone:save()
    sendClientCommand(getPlayer(), "WastelandEventZones", "SetZone", {
        id = self.id,
        name = self.name,
        minX = self.minX,
        minY = self.minY,
        maxX = self.maxX,
        maxY = self.maxY,
        isAdminOnly = self.isAdminOnly,
        isJail = self.isJail,
        teleportX = self.teleportX,
        teleportY = self.teleportY,
        damageRate = self.damageRate,
        damagePreventItems = self.damagePreventItems,
        preventZombies = self.preventZombies,
        minCharecterAgeDays = self.minCharecterAgeDays,
        maxCharecterVisits = self.maxCharecterVisits,
        minTimeBetweenVisitsHours = self.minTimeBetweenVisitsHours,
        isRpZone = self.isRpZone,
        playerVisits = self.playerVisits,
        percentageSprinters = self.percentageSprinters,
        percentageFastShamblers = self.percentageFastShamblers,
        percentageSlowShamblers = self.percentageSlowShamblers,
        warningBuffer = self.warningBuffer,
        warningMessage = self.warningMessage,
    });
end

function WEZ_EventZone:delete()
    sendClientCommand(getPlayer(), "WastelandEventZones", "DeleteZone", {id = self.id})
end

function WEZ_EventZone:isInZone(x, y)
    if x >= self.minX and x <= (self.maxX+1) and y >= self.minY and y <= (self.maxY+1) then
        return true
    end
    return false
end

function WEZ_EventZone:isInWarningZone(x, y)
    if self.warningBuffer == 0 then
        return false
    end
    if x >= (self.minX-self.warningBuffer) and x <= (self.maxX+1+self.warningBuffer) and y >= (self.minY-self.warningBuffer) and y <= (self.maxY+1+self.warningBuffer) then
        return true
    end
    return false
end

function WEZ_EventZone:isPlayerInZone(player)
    return self:isInZone(player:getX(), player:getY())
end

function WEZ_EventZone:isPlayerInWarningZone(player)
    return self:isInWarningZone(player:getX(), player:getY())
end

-- given a point inside the zone, return the closest point outside the zone
function WEZ_EventZone:getClosestPointOutsideZone(x, y)
    -- if you are already outside the zone, you are already at the closest point to you outside the zone
    if not self:isInZone(x, y) then
        return x, y
    end
    local cX = x
    local cY = y
    if x-self.minX < self.maxX-x then
        cX = self.minX - 1
    else
        cX = self.maxX + 1
    end
    if y-self.minY < self.maxY-y then
        cY = self.minY - 1
    else
        cY = self.maxY + 1
    end
    if math.abs(x-cX) < math.abs(y-cY) then
        return cX, y
    else
        return x, cY
    end
end

-- given a point outside the zone, return the closest point inside the zone
function WEZ_EventZone:getClosestPointInsideZone(x, y)
    -- if you are already inside the zone, you are already at the closest point to you inside the zone
    if self:isInZone(x, y) then
        return x, y
    end
    local cX = x
    local cY = y
    if cX < self.minX then
        cX = self.minX
    elseif cX > self.maxX then
        cX = self.maxX
    end
    if cY < self.minY then
        cY = self.minY
    elseif cY > self.maxY then
        cY = self.maxY
    end
    return cX, cY
end

function WEZ_EventZone:setArea(x1, y1, x2, y2)
    self.minX = math.min(x1, x2)
    self.minY = math.min(y1, y2)
    self.maxX = math.max(x1, x2)
    self.maxY = math.max(y1, y2)
end

function WEZ_EventZone:setIsTeleport(isTeleport, x, y)
    self.isTeleport = isTeleport
    self.teleportX = x
    self.teleportY = y
end

function WEZ_EventZone:setPreventZombies(preventZombies)
    self.preventZombies = preventZombies
end

function WEZ_EventZone:setMinCharecterAgeDays(minCharecterAgeDays)
    self.minCharecterAgeDays = minCharecterAgeDays
end

function WEZ_EventZone:setMaxCharecterVisits(maxCharecterVisits)
    self.maxCharecterVisits = maxCharecterVisits
end

function WEZ_EventZone:setMinTimeBetweenVisitsHours(minTimeBetweenVisitsHours)
    self.minTimeBetweenVisitsHours = minTimeBetweenVisitsHours
end

function WEZ_EventZone:initPlayer(player)
    local username = player:getUsername()
    if not self.playerVisits then self.playerVisits = {} end
    if not self.playerVisits[username] then self.playerVisits[username] = {} end
    if not self.playerVisits[username].count then self.playerVisits[username].count = 0 end
    if not self.playerVisits[username].lastVisit then self.playerVisits[username].lastVisit = 0 end
end

function WEZ_EventZone:isPlayerAllowed(player)
    self:initPlayer(player)
    local username = player:getUsername()

    if self.isAdminOnly and not player:isAccessLevel("admin") then
        return false, "You must be an admin to enter this zone."
    end
    if self.minCharecterAgeDays > 0 and player:getHoursSurvived() < (self.minCharecterAgeDays * 24) then
        return false, "You must be at least " .. self.minCharecterAgeDays .. " days old to enter this zone."
    end
    if self.maxCharecterVisits > 0 and self.playerVisits[username].count >= self.maxCharecterVisits then
        return false, "You have already visited this zone " .. self.maxCharecterVisits .. " times."
    end
    if self.minTimeBetweenVisitsHours > 0 and self.playerVisits[username].lastVisit + self.minTimeBetweenVisitsHours > getGameTime():getWorldAgeHours() then
        return false, "You must wait " .. self.minTimeBetweenVisitsHours .. " hours before visiting this zone again."
    end

    return true
end

function WEZ_EventZone:onPlayerVisit(player)
    self:initPlayer(player)

    local username = player:getUsername()
    self.playerVisits[username].count = self.playerVisits[username].count + 1
    self.playerVisits[username].lastVisit = getGameTime():getWorldAgeHours()
    self:save()
end
