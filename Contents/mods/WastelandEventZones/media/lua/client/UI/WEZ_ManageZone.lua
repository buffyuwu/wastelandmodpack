if not isClient() then return end

require "GravyUI"
require "GroundHightlighter"
require "ISUI/ISPanel"

WEZ_ManageZone = ISPanel:derive("WEZ_ManageZone")

WEZ_ManageZone.instance = nil

function WEZ_ManageZone:show(zone)
    if WEZ_ManageZone.instance then
        WEZ_ManageZone.instance:onClose()
    end
    if WEZ_CreateZone.instance then
        WEZ_CreateZone.instance:onClose()
    end
    if WEZ_ListZones.instance then
        WEZ_ListZones.instance:onClose()
    end
    local scale = getTextManager():MeasureStringY(UIFont.Small, "XXX") / 12
    local w = 270 * scale
    local h = 460 * scale
    local o = ISPanel:new(getCore():getScreenWidth()/2-w/2,getCore():getScreenHeight()/2-h/2, w, h)
    setmetatable(o, self)
    o.__index = self
    o.zone = zone
    o.scale = scale
    o:initialise()
    o:addToUIManager()
    WEZ_ManageZone.instance = o
    return o
end

function WEZ_ManageZone:initialise()
    ISPanel.initialise(self)
    self.startX = 0
    self.startY = 0
    self.endX = 0
    self.endY = 0
    self.moveWithMouse = true
    self.groundHighlighter = GroundHightlighter:new()
    self.groundHighlighter:enableXray(true)

    local win = GravyUI.Node(self.width, self.height):pad(5)
    self.win = win
    local header, body, footer = win:rows({30 * self.scale, 1, 40 * self.scale}, 5)
    local header, headerRight = header:cols({1, 100 * self.scale}, 5)
    local fields, zombies = body:rows({1, 70 * self.scale}, 5)
    local nameInput, startPoint, endPoint,
          adminOnly, isJail, rpOnly, damageRate, damagePreventItems,
          noZombies, charAge, maxVisits,
          hoursBetween, teleportPoint,
          warningBuffer, warningMessage = fields:grid(15, {140 * self.scale, 1}, 6, 0)
    local zTitle, zDesc, zFields = zombies:rows({15 * self.scale, 12 * self.scale, 1}, 5)
    local zLabelRow, zDataRow = zFields:grid({12 * self.scale, 1}, 3, 3, 3)
    local f1, f2 = footer:grid(2, 3, 3, 3)

    self.headerBox = header
    self.showHighlightCheckbox = headerRight:makeTickBox()

    self.nameLabel = nameInput[1]
    self.startPointLabel = startPoint[1]
    self.endPointLabel = endPoint[1]
    self.adminOnlyLabel = adminOnly[1]
    self.isJailLabel = isJail[1]
    self.rpZoneLabel = rpOnly[1]
    self.damageRateLabel = damageRate[1]
    self.damagePreventItemsLabel = damagePreventItems[1]
    self.noZombiesLabel = noZombies[1]
    self.charAgeLabel = charAge[1]
    self.maxVisitsLabel = maxVisits[1]
    self.hoursBetweenLabel = hoursBetween[1]
    self.teleportPointLabel = teleportPoint[1]
    self.warningBufferLabel = warningBuffer[1]
    self.warningMessageLabel = warningMessage[1]
    self.zTitle = zTitle
    self.zDesc = zDesc
    self.sprintersLabel = zLabelRow[1]
    self.fastShambsLabel = zLabelRow[2]
    self.slowShambsLabel = zLabelRow[3]

    self.nameInput = nameInput[2]:makeTextBox(self.zone.name)
    self.startPointDisplay = startPoint[2]
    self.endPointDisplay = endPoint[2]
    self.adminOnlyCheckbox = adminOnly[2]:makeTickBox()
    self.isJailCheckbox = isJail[2]:makeTickBox()
    self.rpZoneCheckbox = rpOnly[2]:makeTickBox()

    local inputSize = 30
    self.damagePreventItemsInput = damagePreventItems[2]:makeTextBox(self.zone.damagePreventItems)
    self.damageRateInput = damageRate[2]:resize(inputSize, damageRate[2].height):offset(-(damageRate[2].width-inputSize)/2, 0):makeTextBox(tostring(self.zone.damageRate))
    self.noZombiesCheckbox = noZombies[2]:resize(inputSize, noZombies[2].height):offset(-(noZombies[2].width-inputSize)/2, 0):makeTickBox(self.zone.preventZombies)
    self.charAgeInput = charAge[2]:resize(inputSize, charAge[2].height):offset(-(charAge[2].width-inputSize)/2, 0):makeTextBox(tostring(self.zone.minCharecterAgeDays))
    self.maxVisitsInput = maxVisits[2]:resize(inputSize, maxVisits[2].height):offset(-(maxVisits[2].width-inputSize)/2, 0):makeTextBox(tostring(self.zone.maxCharecterVisits))
    self.hoursBetweenInput = hoursBetween[2]:resize(inputSize, hoursBetween[2].height):offset(-(hoursBetween[2].width-inputSize)/2, 0):makeTextBox(tostring(self.zone.minTimeBetweenVisitsHours))
    self.teleportPointDisplay = teleportPoint[2]
    self.warningBufferInput = warningBuffer[2]:resize(inputSize, warningBuffer[2].height):offset(-(warningBuffer[2].width-inputSize)/2, 0):makeTextBox(tostring(self.zone.warningBuffer))
    self.warningMessageInput = warningMessage[2]:makeTextBox(self.zone.warningMessage)

    self.sprintersRatio = zDataRow[1]:resize(inputSize, zDataRow[1].height):makeTextBox(tostring(self.zone.percentageSprinters))
    self.fastShambsRatio = zDataRow[2]:resize(inputSize, zDataRow[2].height):makeTextBox(tostring(self.zone.percentageFastShamblers))
    self.slowShambsRatio = zDataRow[3]:resize(inputSize, zDataRow[3].height):makeTextBox(tostring(self.zone.percentageSlowShamblers))

    self.saveZoneButton = f1[1]:makeButton("Save", self, self.onSaveZone)
    self.removeButton = f2[1]:makeButton("Remove", self, self.onRemoveZone)
    self.closeButton = f1[2]:makeButton("Close", self, self.onClose)
    self.teleportButton = f2[2]:makeButton("", self, self.onTpButton)
    self.setStartButton = f1[3]:makeButton("Set Start", self, self.onStartButton)
    self.setEndButton = f2[3]:makeButton("Set End", self, self.onEndButton)

    if self.zone.teleportX == 0 and self.zone.teleportY == 0 then
        self.teleportButton:setTitle("Set TP Here")
    else
        self.teleportButton:setTitle("Clear TP")
    end

    self:addChild(self.showHighlightCheckbox)
    self:addChild(self.nameInput)
    self:addChild(self.adminOnlyCheckbox)
    self:addChild(self.isJailCheckbox)
    self:addChild(self.rpZoneCheckbox)
    self:addChild(self.noZombiesCheckbox)
    self:addChild(self.damagePreventItemsInput)
    self:addChild(self.damageRateInput)
    self:addChild(self.charAgeInput)
    self:addChild(self.maxVisitsInput)
    self:addChild(self.hoursBetweenInput)
    self:addChild(self.warningBufferInput)
    self:addChild(self.warningMessageInput)
    self:addChild(self.saveZoneButton)
    self:addChild(self.closeButton)
    self:addChild(self.removeButton)
    self:addChild(self.teleportButton)
    self:addChild(self.setStartButton)
    self:addChild(self.setEndButton)

    self:addChild(self.sprintersRatio)
    self:addChild(self.fastShambsRatio)
    self:addChild(self.slowShambsRatio)

    self.showHighlightCheckbox:addOption("Highlight?")
    self.adminOnlyCheckbox:addOption("")
    self.isJailCheckbox:addOption("")
    self.rpZoneCheckbox:addOption("")
    self.noZombiesCheckbox:addOption("")

    self.showHighlightCheckbox:setSelected(1, false)
    if self.zone.isAdminOnly then self.adminOnlyCheckbox:setSelected(1, true) end
    if self.zone.isJail then self.isJailCheckbox:setSelected(1, true) end
    if self.zone.preventZombies then self.noZombiesCheckbox:setSelected(1, true) end
    if self.zone.isRpZone then self.rpZoneCheckbox:setSelected(1, true) end

    self.damageRateInput:setOnlyNumbers(true)
    self.charAgeInput:setOnlyNumbers(true)
    self.maxVisitsInput:setOnlyNumbers(true)
    self.hoursBetweenInput:setOnlyNumbers(true)
    self.sprintersRatio:setOnlyNumbers(true)
    self.fastShambsRatio:setOnlyNumbers(true)
    self.slowShambsRatio:setOnlyNumbers(true)
    self.warningBufferInput:setOnlyNumbers(true)
end

function WEZ_ManageZone:drawOption(text, labelBox)
    self:drawText(text, labelBox.left, labelBox.top + 2, 1, 1, 1, 1, UIFont.Small)
    local b = math.floor(self.y + labelBox.bottom + 4)
    local l = math.floor(self.x + 5)
    local r = math.floor(self.x + (self.win.width * 2 ) - 10)
    self:drawLine2(l, b, r, b, 1, 0.5, 0.5, 0.5)
end

function WEZ_ManageZone:prerender()
    ISPanel.prerender(self)

    if self.showHighlightCheckbox:isSelected(1) and self.groundHighlighter.type == "none" then
        self:showHightlight()
    elseif not self.showHighlightCheckbox:isSelected(1) and self.groundHighlighter.type ~= "none" then
        self.groundHighlighter:remove()
    end

    self:drawTextCentre("Event Zones", self.headerBox.left + (self.headerBox.width/2), self.headerBox.top, 1, 1, 1, 1, UIFont.Medium)

    self:drawOption("Name:", self.nameLabel)
    self:drawOption("Start Point:", self.startPointLabel)
    self:drawOption("End Point:", self.endPointLabel)
    self:drawOption("Admin Only:", self.adminOnlyLabel)
    self:drawOption("Jail Zone:", self.isJailLabel)
    self:drawOption("RP Zone:", self.rpZoneLabel)
    self:drawOption("Damage Rate:", self.damageRateLabel)
    self:drawOption("Damage Prevention:", self.damagePreventItemsLabel)
    self:drawOption("No Zombies:", self.noZombiesLabel)
    self:drawOption("Min Char Age To Enter:", self.charAgeLabel)
    self:drawOption("Total Max Visits:", self.maxVisitsLabel)
    self:drawOption("Hours Between Visits:", self.hoursBetweenLabel)
    self:drawOption("Teleport Point:", self.teleportPointLabel)
    self:drawOption("Warning Buffer:", self.warningBufferLabel)
    self:drawOption("Warning Message:", self.warningMessageLabel)

    self:drawTextCentre("Zombie Ratios", self.zTitle.left + (self.zTitle.width/2), self.zTitle.top, 1, 1, 1, 1, UIFont.Medium)
    self:drawText("A number 0 to 100, 0 being disabled.", self.zDesc.left, self.zDesc.top, 1, 1, 1, 1, UIFont.Small)

    self:drawTextCentre("Sprinters", self.sprintersLabel.left + (self.sprintersLabel.width/2), self.sprintersLabel.top, 1, 1, 1, 1, UIFont.Small)
    self:drawTextCentre("Fast Shamblers", self.fastShambsLabel.left + (self.fastShambsLabel.width/2), self.fastShambsLabel.top, 1, 1, 1, 1, UIFont.Small)
    self:drawTextCentre("Slow Shamblers", self.slowShambsLabel.left + (self.slowShambsLabel.width/2), self.slowShambsLabel.top, 1, 1, 1, 1, UIFont.Small)

    self:drawText(self:getStartPointString(), self.startPointDisplay.left, self.startPointDisplay.top, 1, 1, 1, 1, UIFont.Small)
    self:drawText(self:getEndPointString(), self.endPointDisplay.left, self.endPointDisplay.top, 1, 1, 1, 1, UIFont.Small)
    self:drawText(self:getTeleportPointString(), self.teleportPointDisplay.left, self.teleportPointDisplay.top, 1, 1, 1, 1, UIFont.Small)
end

function WEZ_ManageZone:getStartPointString()
    return string.format("(%d, %d)", self.zone.minX, self.zone.maxX)
end

function WEZ_ManageZone:getEndPointString()
    return string.format("(%d, %d)", self.zone.minY, self.zone.maxY)
end

function WEZ_ManageZone:getTeleportPointString()
    if self.zone.teleportX == 0 and self.zone.teleportY == 0 then
        return "Not Set"
    end
    return string.format("(%d, %d)", self.zone.teleportX, self.zone.teleportY)
end

function WEZ_ManageZone:onClose()
    WEZ_ManageZone.instance = nil
    self:removeFromUIManager()
end

function WEZ_ManageZone:removeFromUIManager()
    self.showHighlightCheckbox:setSelected(1, false)
    self.groundHighlighter:remove()
    ISPanelJoypad.removeFromUIManager(self)
end

function WEZ_ManageZone:onRemoveZone()
    self.zone:delete()
    self:onClose()
end

function WEZ_ManageZone:onSaveZone()
    self.zone.isAdminOnly = self.adminOnlyCheckbox:isSelected(1)
    self.zone.isJail = self.isJailCheckbox:isSelected(1)
    self.zone.isRpZone = self.rpZoneCheckbox:isSelected(1)
    self.zone.damageRate = tonumber(self.damageRateInput:getText())
    self.zone.damagePreventItems = self.damagePreventItemsInput:getText()
    self.zone.preventZombies = self.noZombiesCheckbox:isSelected(1)
    self.zone.minCharecterAgeDays = tonumber(self.charAgeInput:getText())
    self.zone.maxCharecterVisits = tonumber(self.maxVisitsInput:getText())
    self.zone.minTimeBetweenVisitsHours = tonumber(self.hoursBetweenInput:getText())
    self.zone.percentageSprinters = math.min(100, math.max(0, tonumber(self.sprintersRatio:getText())))
    self.zone.percentageFastShamblers = math.min(100, math.max(0, tonumber(self.fastShambsRatio:getText())))
    self.zone.percentageSlowShamblers = math.min(100, math.max(0, tonumber(self.slowShambsRatio:getText())))
    self.zone.warningBuffer = tonumber(self.warningBufferInput:getText() or "0")
    self.zone.warningMessage = self.warningMessageInput:getText() or ""
    self.zone:save()

    self.sprintersRatio:setText(tostring(self.zone.percentageSprinters))
    self.fastShambsRatio:setText(tostring(self.zone.percentageFastShamblers))
    self.slowShambsRatio:setText(tostring(self.zone.percentageSlowShamblers))
end

function WEZ_ManageZone:onStartButton()
    local player = getPlayer()
    local x, y = player:getX(), player:getY()
    if x > self.zone.maxX or y > self.zone.maxY then
        player:setHaloNote("The start point must be North West of the end point")
        return
    end
    self.zone.minX = math.floor(x)
    self.zone.minY = math.floor(y)
    if self.showHighlightCheckbox:isSelected(1) then
        self:showHightlight()
    end
end

function WEZ_ManageZone:onEndButton()
    local player = getPlayer()
    local x, y = player:getX(), player:getY()
    if x < self.zone.minX or y < self.zone.minY then
        player:setHaloNote("The end point must be South East of the start point")
        return
    end
    self.zone.maxX = math.floor(x)
    self.zone.maxY = math.floor(y)
    if self.showHighlightCheckbox:isSelected(1) then
        self:showHightlight()
    end
end

function WEZ_ManageZone:onTpButton()
    if self.zone.teleportX == 0 or self.zone.teleportY == 0 then
        local player = getPlayer()
        self.zone.teleportX = math.floor(player:getX())
        self.zone.teleportY = math.floor(player:getY())
        self.teleportButton:setTitle("Clear TP")
    else
        self.zone.teleportX = 0
        self.zone.teleportY = 0
        self.teleportButton:setTitle("Set TP Here")
    end
end

function WEZ_ManageZone:showHightlight()
    if self.zone.minX == 0 or self.zone.minY == 0 then return end
    local sx, sy = self.zone.minX, self.zone.minY
    local ex, ey = self.zone.maxX, self.zone.maxY
    self.groundHighlighter:highlightSquare(sx, sy, ex, ey)
end