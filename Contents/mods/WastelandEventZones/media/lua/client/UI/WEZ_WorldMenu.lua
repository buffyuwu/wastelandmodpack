if not isClient() then return end

require "WEZ_EventZone"
require "UI/WEZ_CreateZone"
require "UI/WEZ_ManageZone"
require "UI/WEZ_ListZones"

local WEZ_WorldMenu = {}

WEZ_WorldMenu.doMenu = function(playerIdx, context)
    if not isClient() or isAdmin() then
        local player = getPlayer(playerIdx)
        local zones = WEZ_EventZone.getZonesAt(player:getX(), player:getY())

        local option = context:addOption("Event Zones", nil, nil)
        local submenu = ISContextMenu:getNew(context)
        context:addSubMenu(option, submenu)
        submenu:addOption("List All" , nil, WEZ_WorldMenu.listZones)
        for i=1,#zones do
            local zone = zones[i]
            submenu:addOption("Manage: " .. zone.name, zone, WEZ_WorldMenu.manageZone)
        end
        submenu:addOption("Create New", nil, WEZ_WorldMenu.createZone)
    end
end

function WEZ_WorldMenu.listZones()
    WEZ_ListZones:show()
end

function WEZ_WorldMenu.createZone()
    WEZ_CreateZone:show()
end

--- @param zone WEZ_EventZone
function WEZ_WorldMenu.manageZone(zone)
    WEZ_ManageZone:show(zone)
end

Events.OnFillWorldObjectContextMenu.Add(WEZ_WorldMenu.doMenu)
