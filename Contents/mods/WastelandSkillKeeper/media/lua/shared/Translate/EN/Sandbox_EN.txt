Sandbox_EN = {
    Sandbox_MaxFreeDeaths = "Number of Free Deaths",
    Sandbox_ExpireDays = "Days until Deaths expire",
    Sandbox_PunishPercent = "Percentage of XP to lose",
}
