
local function getFile(player, type)
	return "recoverable/"..player:getUsername().."."..type..".txt";
end

local function getArchvieFile(player, type)
	return "recoverable/archive/"..player:getUsername().."."..type..".txt";
end

local function ReadArrayFile(file)
	local arr = {}
	local file = getFileReader(file, false)
	if file then
		local line = file:readLine()
		while line do
			table.insert(arr, line)
			line = file:readLine()
		end
		file:close()
	end
	return arr
end

local function ReadArrayFileNumber(file)
	local arr = {}
	local file = getFileReader(file, false)
	if file then
		local line = file:readLine()
		while line do
			table.insert(arr, tonumber(line))
			line = file:readLine()
		end
		file:close()
	end
	return arr
end

local function WriteArrayFileNumber(file, arr)
	local file = getFileWriter(file, true, false)
	for _, v in ipairs(arr) do
		file:writeln(tostring(v))
	end
	file:close()
end

local function WriteArrayFile(file, arr)
	local file = getFileWriter(file, true, false)
	for _, v in ipairs(arr) do
		file:writeln(v)
	end
	file:close()
end

local function ReadAssociativeArrayFile(file)
	local arr = {}
	local file = getFileReader(file, false)
	if file then
		local a = file:readLine()
		local b = file:readLine()
		while a and b do
			arr[a] = tonumber(b)
			a = file:readLine()
			b = file:readLine()
		end
		file:close()
	end
	return arr
end

local function WriteAssociativeArrayFile(file, arr)
	local file = getFileWriter(file, true, false)
	for k, v in pairs(arr) do
		file:writeln(k)
		file:writeln(tostring(v))
	end
	file:close()
end

local function getDeaths(player)
	local rawDeaths = ReadArrayFileNumber(getFile(player, "deaths"))
	local now = getTimestamp()
	local sevenDaysAgo = now - (SandboxVars.WastelandOptions.SkillKeeperExpireDays * 24 * 60 * 60)
	local deaths = {}
	for _, v in ipairs(rawDeaths) do
		if v and v > sevenDaysAgo then
			table.insert(deaths, v)
		end
	end
	if #deaths ~= #rawDeaths then
		WriteArrayFileNumber(getFile(player, "deaths"), deaths)
	end
	return deaths
end

local function getData(player)
	local data = {}
	data.deaths = getDeaths(player)
	data.skills = ReadAssociativeArrayFile(getFile(player, "skills"))
	data.knownMediaLines = ReadArrayFile(getFile(player, "media"))
	data.gainedRecipes = ReadArrayFile(getFile(player, "recipes"))
	if getActivatedMods():contains("roleplaychat") then
		data.languages = ReadArrayFile(getFile(player, "languages"))
	end
	return data
end

local function writeDataXp(player, xpData)
	WriteAssociativeArrayFile(getFile(player, "skills"), xpData)
end

local function writeDataMedia(player, mediaData)
	WriteArrayFile(getFile(player, "media"), mediaData)
end

local function writeDataRecipes(player, recipeData)
	WriteArrayFile(getFile(player, "recipes"), recipeData)
end

local function writeLanguages(player, languages)
	WriteArrayFile(getFile(player, "languages"), languages)
end

local function addDeath(player)
	local deaths = getDeaths(player)
	table.insert(deaths, getTimestamp())
	WriteArrayFileNumber(getFile(player, "deaths"), deaths)
end

local function removeDeath(player, death)
	local deaths = getDeaths(player)
	local newDeaths = {}
	for _, v in ipairs(deaths) do
		if v ~= death then
			table.insert(newDeaths, v)
		end
	end
	WriteArrayFileNumber(getFile(player, "deaths"), newDeaths)
end

local function archiveData(player)
	local data = getData(player)
	if data.skills then
		WriteAssociativeArrayFile(getArchvieFile(player, "skills"), data.skills)
	end
	if data.knownMediaLines then
		WriteArrayFile(getArchvieFile(player, "media"), data.knownMediaLines)
	end
	if data.gainedRecipes then
		WriteArrayFile(getArchvieFile(player, "recipes"), data.gainedRecipes)
	end
	if data.languages then
		WriteArrayFile(getArchvieFile(player, "languages"), data.languages)
	end
end

local function restoreArchive(player)
	local data = {}
	data.skills = ReadAssociativeArrayFile(getArchvieFile(player, "skills"))
	data.knownMediaLines = ReadArrayFile(getArchvieFile(player, "media"))
	data.gainedRecipes = ReadArrayFile(getArchvieFile(player, "recipes"))
	if getActivatedMods():contains("roleplaychat") then
		data.languages = ReadArrayFile(getArchvieFile(player, "languages"))
	end
	if data.skills then
		WriteAssociativeArrayFile(getFile(player, "skills"), data.skills)
	end
	if data.knownMediaLines then
		WriteArrayFile(getFile(player, "media"), data.knownMediaLines)
	end
	if data.gainedRecipes then
		WriteArrayFile(getFile(player, "recipes"), data.gainedRecipes)
	end
	if data.languages then
		WriteArrayFile(getFile(player, "languages"), data.languages)
	end
end

Events.OnClientCommand.Add(function (module, command, player, args)
    if module == "WSK_Main" then
        if command == "writeDataXP" then
			writeDataXp(player, args)
        elseif command == "writeDataMedia" then
			writeDataMedia(player, args)
		elseif command == "writeDataRecipes" then
			writeDataRecipes(player, args)
		elseif command == "writeLanguages" then
			if not args then return end
			writeLanguages(player, args)
		elseif command == "archiveData" then
			archiveData(player)
		elseif command == "getData" then
			local data = getData(player)
			sendServerCommand(player, "WSK_Main", "getData", data)
		elseif command == "getDeaths" then
			local deaths = getDeaths(player)
			sendServerCommand(player, "WSK_Main", "updateDeaths", deaths)
		elseif command == "addDeath" then
			addDeath(player)
		elseif command == "removeDeath" then
			local targetPlayerUsername = args[1]
			local onlinePlayers = getOnlinePlayers()
			for i=0, onlinePlayers:size()-1 do
				local targetPlayer = onlinePlayers:get(i)
				if targetPlayer:getUsername() == targetPlayerUsername then
					removeDeath(targetPlayer, args[2])
					sendServerCommand(targetPlayer, "WSK_Main", "updateDeaths", getDeaths(player))
					break
				end
			end
		elseif command == "restoreArchive" then
			local onlinePlayers = getOnlinePlayers()
			for i=0, onlinePlayers:size()-1 do
				local targetPlayer = onlinePlayers:get(i)
				if targetPlayer:getUsername() == args[1] then
					restoreArchive(targetPlayer)
					local data = getData(targetPlayer)
					sendServerCommand(targetPlayer, "WSK_Main", "getData", data)
					break
				end
			end
		end
    end
end)