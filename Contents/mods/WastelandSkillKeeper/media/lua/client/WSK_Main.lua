local checkKey = "WSK_WasChecked"
local strengthKey = "WSK_StartingStrength"
local fitnessKey = "WSK_StartingFitness"
local wasXpAdded = true
local lastKnownMediaCount = 0
local lastKnownRecipesCount = 0
local tickDelay = 0
local cachedMediaLineGuids = {}
local cachedRpLanguages = {
	rpLanguage1 = "Empty Slot",
	rpLanguage2 = "Empty Slot",
}
local deathsKey = "WSK_Deaths"

local function CacheMediaLineGuids()
	cachedMediaLineGuids = {}
	local recordedMedia = getZomboidRadio():getRecordedMedia()
	local categories = recordedMedia:getCategories()
	for i=1,categories:size() do
		local category = categories:get(i-1)
		local mediaType = RecordedMedia.getMediaTypeForCategory(category)
		local mediaList = recordedMedia:getAllMediaForType(mediaType)
		for j=1,mediaList:size() do
			local mediaData = mediaList:get(j-1)
			for k=1, mediaData:getLineCount() do
				local mediaLine = mediaData:getLine(k-1)
				if mediaLine then
					for l = 0, getNumClassFields(mediaLine) - 1 do
						local field = getClassField(mediaLine, l)
						if string.find(tostring(field), "%.text") then
							table.insert(cachedMediaLineGuids, getClassFieldVal(mediaLine, field))
						end
					end
				end
			end
		end
	end
end

local function GetXpBoostMap()
	local xpBoostMap = {
		Strength = 5,
		Fitness = 5,
	}
	local player = getPlayer()

	local profession = ProfessionFactory.getProfession(player:getDescriptor():getProfession())
	if profession then
		local professionXpMap = transformIntoKahluaTable(profession:getXPBoostMap())
		if professionXpMap then
			for perk,level in pairs(professionXpMap) do
				local perkStr = perk:getId()
				xpBoostMap[perkStr] = (xpBoostMap[perkStr] or 0) + tonumber(tostring(level))
			end
		end
	end

	local playerTraits = player:getTraits()
	for i=0, playerTraits:size()-1 do
		local playerTrait = playerTraits:get(i)
		local trait = TraitFactory.getTrait(playerTrait)
		if trait then
			local traitXpMap = transformIntoKahluaTable(trait:getXPBoostMap())
			if traitXpMap then
				for perk,level in pairs(traitXpMap) do
					local perkStr = perk:getId()
					xpBoostMap[perkStr] = (xpBoostMap[perkStr] or 0) + tonumber(tostring(level))
				end
			end
		end
	end

	-- Because PZ automatically gives you traits for strength and fitness
	-- we need to log what they were at the start of the character.
	local modData = player:getModData()
	if not modData[strengthKey] then
		modData[strengthKey] = xpBoostMap["Strength"]
	else
		xpBoostMap["Strength"] = modData[strengthKey]
	end

	if not modData[fitnessKey] then
		modData[fitnessKey] = xpBoostMap["Fitness"]
	else
		xpBoostMap["Fitness"] = modData[fitnessKey]
	end

	if getActivatedMods():contains("WastelandProfessions") then
		for perk,level in pairs(xpBoostMap) do
			if WastelandProfession_DoubleableSkills[perk] then
				xpBoostMap[perk] = math.min(level * 2, 6)
			end
		end
	end

	return xpBoostMap
end

local function IsNewMedia(player)
    local total = 0
	for _, lineGuid in ipairs(cachedMediaLineGuids) do
		if player.isKnownMediaLine and player:isKnownMediaLine(lineGuid) then
			total = total + 1
		end
	end

    if total > lastKnownMediaCount then
        lastKnownMediaCount = total
        return true
    end

	return false
end

local function GetListenedToMedia(player)
	local mediaLines = {}
	for _, lineGuid in ipairs(cachedMediaLineGuids) do
		if player.isKnownMediaLine and player:isKnownMediaLine(lineGuid) then
			table.insert(mediaLines, lineGuid)
		end
	end
	return mediaLines
end

local function IsNewRecipes(player)
    local knownRecipesCount = player:getKnownRecipes():size()
    if lastKnownRecipesCount ~= knownRecipesCount then
        lastKnownRecipesCount = knownRecipesCount
        return true
    end
    return false
end

local function GetRecipes(player)
	local recipes = {}
	local knownRecipes = player:getKnownRecipes()

	for i=0, knownRecipes:size()-1 do
		local recipeID = knownRecipes:get(i)
		recipes[recipeID] = true
	end

	local playerDesc = player:getDescriptor()
	local playerTraits = player:getTraits()
	for i=0, playerTraits:size()-1 do
		local playerTrait = playerTraits:get(i)
		local trait = TraitFactory.getTrait(playerTrait)
		if trait then
			local traitRecipes = trait:getFreeRecipes()
			for ii=0, traitRecipes:size()-1 do
				local traitRecipe = traitRecipes:get(ii)
				recipes[traitRecipe] = nil
			end
		end
	end

	local playerProfession = playerDesc:getProfession()
	local profession = ProfessionFactory.getProfession(playerProfession)
	if profession then
		local profFreeRecipes = profession:getFreeRecipes()
		for i=0, profFreeRecipes:size()-1 do
			local profRecipe = profFreeRecipes:get(i)
			recipes[profRecipe] = nil
		end
	end

	local returnedGainedRecipes = {}
	for recipeID,_ in pairs(recipes) do
		table.insert(returnedGainedRecipes, recipeID)
	end

	return returnedGainedRecipes
end

local function GetPlayerSkills(player)
    local skills = {}
	local xpBoostMap = GetXpBoostMap()

    for i=1, Perks.getMaxIndex()-1 do
		local perks = Perks.fromIndex(i)
        local perk = PerkFactory.getPerk(perks)
        if perk then
            local currentXP = player:getXp():getXP(perk)
            local perkStr = perk:getId()
            local bonusLevels = (xpBoostMap[perkStr] or 0)
            local savingXP = currentXP-perk:getTotalXpForLevel(bonusLevels)
			if savingXP > 0 then
            	skills[perkStr] = savingXP
			end
        end
    end

    return skills
end

local function setDeathsOnPlayer(player, deaths)
	local modData = player:getModData()

	if modData[deathsKey .. "count"] then
		local count = modData[deathsKey .. "count"]
		for i=1, count do
			modData[deathsKey .. tostring(i)] = nil
		end
	end

	if deaths then
		for i, death in ipairs(deaths) do
			modData[deathsKey .. tostring(i)] = death
		end
		modData[deathsKey .. "count"] = #deaths
	else
		modData[deathsKey .. "count"] = 0
	end
	player:transmitModData()
end

local function SetPlayerData(data)
    local player = getPlayer()
	local deaths = data.deaths
	setDeathsOnPlayer(player, deaths)
	local numDeaths = #deaths
	local hasWastelandXP = getActivatedMods():contains("WastelandXP")
    local skills = data.skills
    local knownMediaLines = data.knownMediaLines
    local gainedRecipes = data.gainedRecipes
	local xpBoostMap = GetXpBoostMap()
    local xp = player:getXp()
	local percentKeep = 100 - SandboxVars.WastelandOptions.SkillKeeperPunishPercent

    for i=1, Perks.getMaxIndex()-1 do
		local perks = Perks.fromIndex(i)
        local perk = PerkFactory.getPerk(perks)
        if perk then
            local perkStr = perk:getId()
            if skills[perkStr] then
				if numDeaths > SandboxVars.WastelandOptions.SkillKeeperMaxFreeDeaths then
					skills[perkStr] = skills[perkStr] * (percentKeep / 100)
				end
                local bonusLevels = (xpBoostMap[perkStr] or 0)
				local totalXp = skills[perkStr] + perk:getTotalXpForLevel(bonusLevels)
				if hasWastelandXP then
					local cap = getLevelCap(player, perk)
					if cap then
						totalXp = math.min(totalXp, perk:getTotalXpForLevel(cap))
					end
				end
				local neededXp = totalXp - xp:getXP(perk)
                if neededXp > 0 then
                    xp:AddXP(perk, neededXp, false, false, true)
                end
            end
        end
    end

    for _, recipeID in ipairs(gainedRecipes) do
        player:learnRecipe(recipeID)
    end

    for _, line in ipairs(knownMediaLines) do
        player:addKnownMediaLine(line)
    end

	if getActivatedMods():contains("roleplaychat") and data.languages and #data.languages >= 2 then
		cachedRpLanguages.rpLanguage1 = data.languages[1]
		cachedRpLanguages.rpLanguage2 = data.languages[2]
		local modData = player:getModData()
		modData.rpLanguage1 = cachedRpLanguages.rpLanguage1
		modData.rpLanguage2 = cachedRpLanguages.rpLanguage2
		ISChat.instance.rpLanguage1 = cachedRpLanguages.rpLanguage1
		ISChat.instance.rpLanguage2 = cachedRpLanguages.rpLanguage2
	end

	player:addLineChatElement("You have died "..numDeaths.." times in the last "..SandboxVars.WastelandOptions.SkillKeeperExpireDays.." days", 1, 0, 0);
	if numDeaths > SandboxVars.WastelandOptions.SkillKeeperMaxFreeDeaths then
		player:addLineChatElement("Recovered " .. percentKeep .. "% of XP", 1, 0, 0)
	else
		player:addLineChatElement("Recovered all XP", 1, 0, 0)
	end

    player:getModData()[checkKey] = true
	wasXpAdded = true
end

local function CheckSavePlayer()
	local player = getPlayer()
    if not player:getModData()[checkKey] then return end
	if player:isDead() then	return end

    if wasXpAdded then
        wasXpAdded = false
		print("Send Earned XP to server")
        sendClientCommand(player, "WSK_Main", "writeDataXP", GetPlayerSkills(player))
    end
    if IsNewRecipes(player) then
		print("Send Learned Recipes to server")
        sendClientCommand(player, "WSK_Main", "writeDataRecipes", GetRecipes(player))
    end
    if IsNewMedia(player) then
		print("Send Consumed Media to server")
        sendClientCommand(player, "WSK_Main", "writeDataMedia", GetListenedToMedia(player))
    end
	if getActivatedMods():contains("roleplaychat") then
		local modData = player:getModData()
		if modData.rpLanguage1 ~= cachedRpLanguages.rpLanguage1 or modData.rpLanguage2 ~= cachedRpLanguages.rpLanguage2 then
			cachedRpLanguages.rpLanguage1 = modData.rpLanguage1
			cachedRpLanguages.rpLanguage2 = modData.rpLanguage2
			print("Send RP Languages to server")
			sendClientCommand(player, "WSK_Main", "writeLanguages", {cachedRpLanguages.rpLanguage1, cachedRpLanguages.rpLanguage2})
		end
	end
end

local function CheckForGetData()
    if tickDelay > 0 then
        tickDelay = tickDelay - 1
        return
    end
    tickDelay = 120
	local player = getPlayer()
    if not player:getModData()[checkKey] then
		sendClientCommand(player, "WSK_Main", "getData", {})
	else
		sendClientCommand(player, "WSK_Main", "getDeaths", {})
    end
end

local function CheckServerCommand(module, command, args)
    if module == "WSK_Main" then
		Events.OnTick.Remove(CheckForGetData)
        if command == "getData" then
            SetPlayerData(args)
        end
		if command == "updateDeaths" then
			setDeathsOnPlayer(getPlayer(), args)
		end
    end
end
Events.OnServerCommand.Add(CheckServerCommand)

Events.AddXP.Add(function ()
    wasXpAdded = true
end)

Events.EveryTenMinutes.Add(CheckSavePlayer)

Events.OnCreatePlayer.Add(function (_, player)
	CacheMediaLineGuids()
	tickDelay = 60
	Events.OnTick.Add(CheckForGetData)
end)

Events.OnPlayerDeath.Add(function (player)
	sendClientCommand(player, "WSK_Main", "addDeath", {})
	sendClientCommand(player, "WSK_Main", "archiveData", {})
	wasXpAdded = true
	lastKnownMediaCount = 0
	lastKnownRecipesCount = 0
	tickDelay = 0
	cachedMediaLineGuids = {}
	cachedRpLanguages = {
		rpLanguage1 = "Empty Slot",
		rpLanguage2 = "Empty Slot",
	}
end)

local function AdminContextEntry(context, player)
	local username = player:getUsername()
	local modData = player:getModData()
	if modData[deathsKey .. "count"] and modData[deathsKey .. "count"] > 0 then
		local submenu = ISContextMenu:getNew(context)
		local option = context:addOption("Manage Deaths: " .. username, nil, nil)
		context:addSubMenu(option, submenu)
		for i=1,modData[deathsKey .. "count"] do
			local death = modData[deathsKey .. tostring(i)]
			local hoursAgo = math.floor((getTimestamp() - death) / 360) / 10
			submenu:addOption("Remove: " .. hoursAgo .. " hours ago", nil, function()
				sendClientCommand(player, "WSK_Main", "removeDeath", {username, death})
			end)
		end
	end
	context:addOption("Force Skill Restore: " .. username, nil, function()
		sendClientCommand(player, "WSK_Main", "restoreArchive", {username})
	end)
end

Events.OnFillWorldObjectContextMenu.Add(function(playerIdx, context, worldobjects)
	local player = getPlayer(playerIdx)
	local modData = player:getModData()
	if modData[deathsKey .. "count"] and modData[deathsKey .. "count"] > 0 then
		local submenu = ISContextMenu:getNew(context)
		local option = context:addOption("Deaths", nil, nil)
		context:addSubMenu(option, submenu)
		for i=1,modData[deathsKey .. "count"] do
			local death = modData[deathsKey .. tostring(i)]
			local hoursAgo = math.floor((getTimestamp() - death) / 360) / 10
			submenu:addOption(hoursAgo .. " hours ago", nil, nil)
		end
	end
	if isAdmin() then
		local seenPlayers = {}
		for _, v in ipairs(worldobjects) do
			local movingObjects = v:getSquare():getMovingObjects()
			for i = 0, movingObjects:size() - 1 do
				local o = movingObjects:get(i)
				if instanceof(o, "IsoPlayer") then
					local username = o:getUsername()
					if not seenPlayers[username] then
						seenPlayers[username] = true
						AdminContextEntry(context, o)
					end
				end
			end
		end
	end
end)