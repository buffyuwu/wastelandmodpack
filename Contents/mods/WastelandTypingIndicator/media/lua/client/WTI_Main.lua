-- Only MP
if not isClient() then return end

WTI_Main = WTI_Main or {}
WTI_Main.players = WTI_Main.players or {}
WTI_Main.tickDelay = WTI_Main.tickDelay or 0
WTI_Main.muteTyping = WTI_Main.muteTyping or false

-- TODO: should check if admin and visible, or chat option is disabled
function WTI_Main.shouldSync()
    return not WTI_Main.muteTyping
end

local onTypingDebounce = 0
local clearDebouce = 0
local emptyObject = {}
local lastWasClear = false
local nextRange = 0
function WTI_Main.onTyping(range)
    if not WTI_Main.shouldSync() then
        return
    end
    nextRange = range
    lastWasClear = false
    clearDebouce = 0
    if onTypingDebounce == 0 then
        onTypingDebounce = 45
    end
end

function WTI_Main.onCleared(immediately)
    onTypingDebounce = 0
    if lastWasClear then
        return
    end
    lastWasClear = true
    clearDebouce = immediately and 1 or 45
end

function WTI_Main.doLog(text)
    local p = getPlayer()
    local x = math.floor(p:getX())
    local y = math.floor(p:getY())
    local z = math.floor(p:getZ())
    sendClientCommand(p, 'WTI', 'doLog', {x, y, z, text})
end

function WTI_Main.update()
    if onTypingDebounce > 0 then
        if onTypingDebounce == 1 then
            sendClientCommand(getPlayer(), 'WTI', 'onTyping', {nextRange})
        end
        onTypingDebounce = onTypingDebounce - 1
    end

    if clearDebouce > 0 then
        if clearDebouce == 1 then
            sendClientCommand(getPlayer(), 'WTI', 'onCleared', emptyObject)
        end
        clearDebouce = clearDebouce - 1
    end

    if WTI_Main.tickDelay > 0 then
        WTI_Main.tickDelay = WTI_Main.tickDelay - 1
        return
    end
    WTI_Main.tickDelay = 30

    local toRemove = {}
    for username, status in pairs(WTI_Main.players) do
        if status.lastTs + 8000 < getTimestampMs() then
            table.insert(toRemove, username)
        end
    end
    for _, username in pairs(toRemove) do
        WTI_Main.players[username] = nil
    end
end

function WTI_Main.onServerCommand(module, command, args)
    if module ~= "WTI" then return end
    if command == "onTyping" then
        WTI_Main.players[args.username] = {
            displayName = args.displayName,
            lastTs = getTimestampMs(),
        }
    elseif command == "onCleared" then
        WTI_Main.players[args.username] = nil
    end
end

Events.OnServerCommand.Add(WTI_Main.onServerCommand)
Events.OnTick.Add(WTI_Main.update)