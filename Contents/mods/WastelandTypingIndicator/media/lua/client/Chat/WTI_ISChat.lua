-- Only MP
if not isClient() then return end

require "GroundHightlighter"

WTI_Main = WTI_Main or {}

local fntSize = getTextManager():getFontFromEnum(UIFont.Small):getLineHeight()

local function getRangeFromText(text)
    if getActivatedMods():contains("roleplaychat") then
        if text:sub(1, 4) == "/say" or text:sub(1, 3) == "/me" or text:sub(1, 3) == "/do" or text:sub(1, 2) == "/m" or text:sub(1, 2) == "/d" then
            return SandboxVars.RoleplayChat.sayRange or 15
        elseif text:sub(1, 7) == "/melong" or text:sub(1, 7) == "/dolong" or text:sub(1, 4) == "/mel" or text:sub(1, 3) == "/dl" then
            return SandboxVars.RoleplayChat.meLongRange or 40
        elseif text:sub(1, 4) == "/low" or text:sub(1, 6) == "/melow" or text:sub(1, 6) == "/dolow" or text:sub(1, 3) == "/ml" or text:sub(1, 4) == "/dol" then
            return SandboxVars.RoleplayChat.lowRange or 5
        elseif text:sub(1, 8) == "/whisper" or text:sub(1, 10) == "/whisperme" or text:sub(1, 4) == "/wme" or text:sub(1, 2) == "/w" then
            return SandboxVars.RoleplayChat.whisperRange or 2
        elseif text:sub(1, 5) == "/yell" or text:sub(1, 6) == "/shout" then
            return SandboxVars.RoleplayChat.shoutRange or 50
        else
            return SandboxVars.RoleplayChat.sayRange or 15
        end
    else
        return 15
    end
end

local function isSpeakingCommand(text)
    return text == "/say" or text == "/me" or text == "/do" or text == "/m" or text == "/d" or
        text == "/melong" or text == "/dolong" or text == "/mel" or text == "/dl" or text == "/low" or
        text == "/melow" or text == "/dolow" or text == "/ml" or text == "/dol" or text == "/whisper" or
        text == "/whisperme" or text == "/wme" or text == "/w" or text == "/yell" or text == "/shout"
end

local original_ISChat_onTextChange = ISChat.onTextChange
function ISChat:onTextChange()
    original_ISChat_onTextChange(self)

    local instance = ISChat.instance

    -- Don't show typing indicator if not in the chat tab
    if instance.currentTabID ~= 1 then
        WTI_Main.onCleared()
        return
    end

    local text = instance.textEntry:getInternalText()
    local textLen = text:len()
    if textLen == 0 then
        WTI_Main.onCleared()
        return
    end

    -- Don't show typing indicator while typing a /command but
    -- show it if the command is followed by a space and text
    if text:sub(1, 1) == "/" then
        for i = 1, textLen do
            if text:sub(i, i) == " " then
                if isSpeakingCommand(text:sub(1, i-1)) and textLen > i then
                    WTI_Main.onTyping(getRangeFromText(text))
                    return
                else
                    break
                end
            end
        end
        WTI_Main.onCleared()
        return
    end

    WTI_Main.onTyping(getRangeFromText(text))
end

local original_ISChat_calcTabSize = ISChat.calcTabSize
function ISChat:calcTabSize()
    local tabSize = original_ISChat_calcTabSize(self)
    -- Make room for the typing indicator
    tabSize.height = tabSize.height - fntSize - 4
    return tabSize
end

local original_ISChat_render = ISChat.render
function ISChat:render()
    original_ISChat_render(self)
    if ISChat.instance.showRangeTicks > 0 then
        if self.showRangeTicks % 20 == 0 then
            if self.showRangeSwitch then
                self.groundHighlighter:setColor(0.8, 0.8, 0.8, 1.0)
            else
                self.groundHighlighter:setColor(0.2, 0.2, 0.2, 1.0)
            end
            self.showRangeSwitch = not self.showRangeSwitch
        end

        if ISChat.instance.showRangeTicks == 1 then
            self.groundHighlighter:remove()
        end
        ISChat.instance.showRangeTicks = ISChat.instance.showRangeTicks - 1
    end

    local typers = {}
    for _, status in pairs(WTI_Main.players) do
        table.insert(typers, status.displayName)
    end

    if #typers == 0 then
        return
    end

    table.sort(typers)

    local text = getText("UI_WTI_Typing") .. table.concat(typers, ", ")
    local x = self.textEntry:getX() + 2
    local y = self.textEntry:getY() - fntSize - 2
    local width = getTextManager():MeasureStringX(UIFont.Small, text)
    if width > self.textEntry:getWidth() then
        text = getText("UI_WTI_ManyTyping")
    end
    self:drawText(text, x, y, 1, 1, 1, 1, UIFont.Small)
end

local original_ISChat_createChildren = ISChat.createChildren
function ISChat:createChildren()
    original_ISChat_createChildren(self)

    self.muteTypingButton = ISButton:new(self.gearButton:getX() - 30, 1, 20, 16, "", self, ISChat.onMuteTypingButtonClick)
    self.muteTypingButton.anchorRight = true
    self.muteTypingButton.anchorLeft = false
    self.muteTypingButton:initialise()
    self.muteTypingButton.borderColor.a = 0.0
    self.muteTypingButton.backgroundColor.a = 0.0
    self.muteTypingButton.backgroundColorMouseOver.a = 0.0
    if WTI_Main.muteTyping then
        self.muteTypingButton:setImage(getTexture("media/ui/WTI_typing_off.png"))
    else
        self.muteTypingButton:setImage(getTexture("media/ui/WTI_typing_on.png"))
    end
    self.muteTypingButton:setUIName("toggle typing indicator")
    self:addChild(self.muteTypingButton)
    self.muteTypingButton:setVisible(true)

    self.showRangeButton = ISButton:new(self.muteTypingButton:getX() - 30, 1, 20, 16, "", self, ISChat.onShowRangeButtonClick)
    self.showRangeButton.anchorRight = true
    self.showRangeButton.anchorLeft = false
    self.showRangeButton:initialise()
    self.showRangeButton.borderColor.a = 0.0
    self.showRangeButton.backgroundColor.a = 0.0
    self.showRangeButton.backgroundColorMouseOver.a = 0.0
    self.showRangeButton:setImage(getTexture("media/ui/WTI_range.png"))
    self.showRangeButton:setUIName("toggle range indicator")
    self:addChild(self.showRangeButton)
    self.showRangeButton:setVisible(true)
    self.showRangeTicks = 0

    self.groundHighlighter = GroundHightlighter:new()
    self.groundHighlighter:setColor(0.8, 0.8, 0.8, 0.5)
end

local original_ISChat_unfocus = ISChat.unfocus
function ISChat:unfocus()
    original_ISChat_unfocus(self)
    WTI_Main.onCleared()
end

local original_ISChat_onCommandEntered = ISChat.onCommandEntered
function ISChat:onCommandEntered()
    local text = ISChat.instance.textEntry:getInternalText()
    original_ISChat_onCommandEntered(self)
    WTI_Main.onCleared(true)
    WTI_Main.doLog(text)
end

function ISChat:onMuteTypingButtonClick()
    WTI_Main.muteTyping = not WTI_Main.muteTyping
    if WTI_Main.muteTyping then
        self.muteTypingButton:setImage(getTexture("media/ui/WTI_typing_off.png"))
    else
        self.muteTypingButton:setImage(getTexture("media/ui/WTI_typing_on.png"))
    end
end

function ISChat:onShowRangeButtonClick()
    if self.showRangeTicks > 0 then
        self.showRangeTicks = 1
    end

    if not getActivatedMods():contains("roleplaychat") then
        local text = ISChat.instance.textEntry:getInternalText()
        local range = getRangeFromText(text)
        ISChat.instance:showMessageRange(range)
        return
    end

    local context = ISContextMenu.get(0, self:getAbsoluteX() + self:getWidth() / 2, self:getAbsoluteY() + self.showRangeButton:getY())
    context:addOption("Whisper", ISChat.instance, ISChat.instance.showMessageRange, SandboxVars.RoleplayChat.whisperRange or 2)
    context:addOption("Low", ISChat.instance, ISChat.instance.showMessageRange, SandboxVars.RoleplayChat.lowRange or 5)
    context:addOption("Normal", ISChat.instance, ISChat.instance.showMessageRange, SandboxVars.RoleplayChat.sayRange or 15)
    context:addOption("Long", ISChat.instance, ISChat.instance.showMessageRange, SandboxVars.RoleplayChat.highRange or 40)
    context:addOption("Shout", ISChat.instance, ISChat.instance.showMessageRange, SandboxVars.RoleplayChat.shoutRange or 50)
end

function ISChat:showMessageRange(range)
    local p = getPlayer()
    local x = p:getX()
    local y = p:getY()
    local z = p:getZ()
    self.lastRange = range
    self.showRangeTicks = 100
    self.showRangeSwitch = false
    self.groundHighlighter:highlightCircle(x, y, range, z)
end