-- Only MP
if not isServer() then return end

local function canSee(player, otherPlayer, range)
    if not player or not otherPlayer then return false end
    if player:getDistanceSq(otherPlayer) > range then return false end
    if player:isInvisible() and not otherPlayer:isInvisible() then return false end
    return true
end

local function onWtiCommand(module, command, sendingPlayer, args)
    if module ~= "WTI" then return end

    if command == "doLog" then
        local username = sendingPlayer:getUsername()
        local forname = sendingPlayer:getDescriptor():getForename()
        local x, y, z, text = args[1], args[2], args[3], args[4]
        local logMessage = string.format("%s (%s) @ %s,%s,%s: %s", username, forname, x, y, z, text)
        writeLog("CleanChat", logMessage)
        return
    end

    local onlinePlayers = getOnlinePlayers()
    if onlinePlayers:size() == 0 then return end
    local range = args and args[0] or 15
    local toSend = {
        username = sendingPlayer:getUsername(),
        displayName = sendingPlayer:getDescriptor():getForename(),
    }
    for i=0, onlinePlayers:size()-1 do
        local player = onlinePlayers:get(i)
        if canSee(player, sendingPlayer, range) then
            sendServerCommand(player, "WTI", command, toSend)
        end
    end
end

Events.OnClientCommand.Add(onWtiCommand)