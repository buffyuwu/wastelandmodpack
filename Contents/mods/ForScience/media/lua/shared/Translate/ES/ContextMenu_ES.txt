ContextMenu_ES = {

    ContextMenu_FSBandage = "Practicar primeros auxilios"
    ContextMenu_FSDissect_Corpse = "Diseccionar cad�ver",
    ContextMenu_FSDissect_Tooltip = "Escuela de medicina en el campo",
    ContextMenu_FSJournal_Tooltip = "Bonificaci�n por el diario m�dico",
    ContextMenu_FSTinker = "Practicar electr�nica",
	ContextMenu_FSSimple = "Practicar con dispositivo simple",
	ContextMenu_FSAdvanced = "Practicar con dispositivo avanzado",
    ContextMenu_FSManual_Tooltip = "Bonificaci�n por la gu�a de novatos",
    ContextMenu_FSTooSkilled_Tooltip = "Demasiado h�bil para aprender con este dispositivo",
	ContextMenu_FSMaintain = "Mantenimiento de arma"
	ContextMenu_FSMaintain_Tooltip = "Realizar mantenimiento regular"
	ContextMenu_FSCondition = "Condici�n";
	ContextMenu_FSBloodLevel = "Nivel de sangre";

} 