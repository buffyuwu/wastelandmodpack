---
--- WWP_AddPlayer.lua
--- 24/06/2023
---

if not isClient() then return end

require "GravyUI"
require "ISUI/ISPanel"

local FONT_HGT_SMALL = getTextManager():getFontHeight(UIFont.Small)

WWP_AddPlayer = ISPanel:derive("WWP_AddPlayer")
WWP_AddPlayer.instance = nil

function WWP_AddPlayer:show(zoneManager)
	local w = 400
	local h = 350
	local o = ISPanel:new(getCore():getScreenWidth()/2-w/2,getCore():getScreenHeight()/2-h/2, w, h)
	setmetatable(o, self)
	o.__index = self
	o.zoneManager = zoneManager
	o:initialise()
	o:addToUIManager()
	WWP_AddPlayer.instance = o
	return o
end

function WWP_AddPlayer:initialise()
	self.moveWithMouse = true

	local window = GravyUI.Node(self.width, self.height):pad(15)
	local header, body, footer = window:rows({30, 1, 20}, 15)
	local leftBtn, nothing, rightBtn = footer:cols({0.2, 0.6, 30}, 15)
	self.headerLabel = header

	self.playerList = ISScrollingListBox:new(body.left, body.top, body.width, body.height);
	self.playerList:initialise();
	self.playerList:instantiate();
	self.playerList.itemheight = FONT_HGT_SMALL + 2 * 2;
	self.playerList.selected = 0;
	self.playerList.joypadParent = self;
	self.playerList.font = UIFont.NewSmall;
	self.playerList.doDrawItem = self.drawPlayers;
	self.playerList.drawBorder = true;

	self.goButton = rightBtn:makeButton("Add Player", self, self.onAddPlayer)
	self.cancelButton = leftBtn:makeButton("Cancel", self, self.onCancel)

	self:addChild(self.playerList);
	self:addChild(self.goButton)
	self:addChild(self.cancelButton)

	self:populatePlayerList()
end

function WWP_AddPlayer:prerender()
	ISPanel.prerender(self)
	self:drawTextCentre("Connected Players", self.headerLabel.left + (self.headerLabel.width/2),
			self.headerLabel.top, 1, 1, 1, 1, UIFont.Medium)
end

function WWP_AddPlayer:drawPlayers(y, item, alt)
	local a = 0.9;
	self:drawRectBorder(0, (y), self:getWidth(), self.itemheight - 1, a, self.borderColor.r, self.borderColor.g,
			self.borderColor.b);
	if self.selected == item.index then
		self:drawRect(0, (y), self:getWidth(), self.itemheight - 1, 0.3, 0.7, 0.35, 0.15);
	end
	self:drawText(item.text, 10, y + 2, 1, 1, 1, a, self.font);
	return y + self.itemheight;
end


function WWP_AddPlayer:populatePlayerList()
	self.playerList:clear();
	local players = getOnlinePlayers()
	players = players or ArrayList.new()
	for playerIndex = 0, players:size() -1 do
		local p = players:get(playerIndex)
		self.playerList:addItem(p:getUsername(), p:getUsername());
	end
end

function WWP_AddPlayer:onAddPlayer()
	local username = self.playerList.items[self.playerList.selected].item
	if(username) then
		self.zoneManager:addEmployee(username)
		self:onCancel()
	end
end

function WWP_AddPlayer:onCancel()
	WWP_AddPlayer.instance = nil
	self:removeFromUIManager()
end