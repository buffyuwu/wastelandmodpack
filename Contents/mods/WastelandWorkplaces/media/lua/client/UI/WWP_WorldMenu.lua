---
--- WWP_WorldMenu.lua
--- Right click context menu for Admins, used for managing workplaces
--- 18/06/2023
---
---
if not isClient() then return end

require "UI/WWP_CreateZone"

local WWP_WorldMenu = {}

WWP_WorldMenu.doMenu = function(playerIdx, context)
	local player = getPlayer(playerIdx)
	local x, y = ISCoordConversion.ToWorld(getMouseXScaled(), getMouseYScaled(), player:getZ())
	local zones = WWP_WorkplaceZone.getZonesAt(x, y, player:getZ())
	if isAdmin() then
		local option = context:addOption("Workplaces", nil, nil)
		local submenu = ISContextMenu:getNew(context)
		context:addSubMenu(option, submenu)
		submenu:addOption("List All" , nil, WWP_WorldMenu.listZones)
		for i=1,#zones do 	-- Very unlikely to be more than one here
			local zone = zones[i]
			submenu:addOption("Manage: " .. zone.name, zone, WWP_WorldMenu.manageZone)
		end

		local startingCoordinates = {
			startX = x,
			startY = y,
			endX = player:getX(),
			endY = player:getY(),
		}
		submenu:addOption("Create New", startingCoordinates, WWP_WorldMenu.createZone)
	else
		for i=1,#zones do 	-- Very unlikely to be more than one here
			local zone = zones[i]
			if(zone:isEmployee(player)) then
				context:addOption("Manage " .. zone.name, zone, WWP_WorldMenu.manageZone)
				local openClosed = zone.open and "Close " or "Open "
				context:addOption(openClosed.. zone.name, zone, WWP_WorldMenu.flipOpenClosed)
			end
		end
	end
end

function WWP_WorldMenu.listZones()
	WWP_ListWorkplaces:show()
end

function WWP_WorldMenu.createZone(startingCoordinates)
	WWP_CreateZone:show(startingCoordinates)
end

--- @param zone WWP_WorkplaceZone
function WWP_WorldMenu.manageZone(zone)
	WWP_ManageZone:show(zone, isAdmin())
end

--- @param zone WWP_WorkplaceZone
function WWP_WorldMenu.flipOpenClosed(zone)
	zone.open = not zone.open
	zone:save()
end

Events.OnFillWorldObjectContextMenu.Add(WWP_WorldMenu.doMenu)