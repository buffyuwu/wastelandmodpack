---
--- WWP_CreateZone.lua
--- 18/06/2023
---

if not isClient() then return end

require "GravyUI"
require "GroundHightlighter"
require "ISUI/ISPanel"
require "WWP_WorkplaceType"
require "UI/WWP_ManageZone"

WWP_CreateZone = ISPanel:derive("WWP_CreateZone")
WWP_CreateZone.instance = nil

function WWP_CreateZone:show(startingCoordinates)
	if WWP_ManageZone.instance then
		WWP_ManageZone.instance:onClose()
	end
	if WWP_CreateZone.instance then
		WWP_CreateZone.instance:onClose()
	end
	if WWP_ListWorkplaces.instance then
		WWP_ListWorkplaces.instance:onClose()
	end

	local w = 400
	local h = 210
	local o = ISPanel:new(getCore():getScreenWidth()/2-w/2,getCore():getScreenHeight()/2-h/2, w, h)
	setmetatable(o, self)
	o.__index = self
	o.startX = startingCoordinates.startX
	o.startY = startingCoordinates.startY
	o.endX = startingCoordinates.endX
	o.endY = startingCoordinates.endY
	o:initialise()
	o:addToUIManager()
	WWP_CreateZone.instance = o
	return o
end

function WWP_CreateZone:initialise()
	ISPanel.initialise(self)
	self.moveWithMouse = true
	self.groundHighlighter = GroundHightlighter:new()
	self.groundHighlighter:setColor(0.99, 0.85, 0.05, 1.0)
	self.groundHighlighter:enableXray(true)
	self.lastHighlight = {x1 = 0, y1 = 0, x2 = 0, y2 = 0}

	local win = GravyUI.Node(self.width, self.height):pad(5)
	local header, body, footer = win:rows({30, 1, 50}, 5)
	local header, headerRight = header:cols({1, 100}, 5)
	local nameInput, typeInput, startPoint, endPoint = body:grid(4, {0.5, 0.5}, 5, 5)
	local fr1, fr2 = footer:grid(2, 2, 5, 5)

	self.headerBox = header

	self.showHighlightCheckbox = headerRight:makeTickBox()
	self.nameLabel = nameInput[1]
	self.nameInput = nameInput[2]:makeTextBox("")
	self.typeLabel = typeInput[1]
	self.typeComboBox = typeInput[2]:makeComboBox()
	self.startPointLabel = startPoint[1]
	self.startPointDisplay = startPoint[2]
	self.endPointLabel = endPoint[1]
	self.endPointDisplay = endPoint[2]

	for _, type in pairs(WWP_WorkplaceTypes) do
		self.typeComboBox:addOptionWithData(type.name, type)
	end

	self.setStartPointButton = fr1[1]:makeButton("Set Start Point", self, self.onSetStartPoint)
	self.setEndPointButton = fr2[1]:makeButton("Set End Point", self, self.onSetEndPoint)
	self.createZoneButton = fr1[2]:makeButton("Create Workplace", self, self.onCreateZone)
	self.closeButton = fr2[2]:makeButton("Close", self, self.onClose)


	self:addChild(self.showHighlightCheckbox)
	self:addChild(self.nameInput)
	self:addChild(self.typeComboBox)
	self:addChild(self.setStartPointButton)
	self:addChild(self.setEndPointButton)
	self:addChild(self.createZoneButton)
	self:addChild(self.closeButton)

	self.showHighlightCheckbox:addOption("Highlight?")
	self.showHighlightCheckbox:setSelected(1, true)
end

function WWP_CreateZone:prerender()
	ISPanel.prerender(self)

	if self.showHighlightCheckbox:isSelected(1) and self.groundHighlighter.type == "none" then
		self:showHightlight()
	elseif not self.showHighlightCheckbox:isSelected(1) and self.groundHighlighter.type ~= "none" then
		self.groundHighlighter:remove()
	end

	self:drawTextCentre("Workplace Creator", self.headerBox.left + (self.headerBox.width/2), self.headerBox.top, 1, 1, 1, 1, UIFont.Medium)
	self:drawTextRight("Workplace Name:", self.nameLabel.right, self.nameLabel.top, 1, 1, 1, 1, UIFont.Small)
	self:drawTextRight("Workplace Type:", self.typeLabel.right, self.typeLabel.top, 1, 1, 1, 1, UIFont.Small)
	self:drawTextRight("Start Point:", self.startPointLabel.right, self.startPointLabel.top, 1, 1, 1, 1, UIFont.Small)
	self:drawTextRight("End Point:", self.endPointLabel.right, self.endPointLabel.top, 1, 1, 1, 1, UIFont.Small)

	self:drawText(self:getStartPointString(), self.startPointDisplay.left, self.startPointDisplay.top, 1, 1, 1, 1, UIFont.Small)
	self:drawText(self:getEndPointString(), self.endPointDisplay.left, self.endPointDisplay.top, 1, 1, 1, 1, UIFont.Small)
end

function WWP_CreateZone:onSetStartPoint()
	local player = getPlayer()
	self.startX = player:getX()
	self.startY = player:getY()
	self:showHightlight()
end

function WWP_CreateZone:onSetEndPoint()
	local player = getPlayer()
	self.endX = player:getX()
	self.endY = player:getY()
	self:showHightlight()
end

function WWP_CreateZone:onCreateZone()
	if self.startX == 0 or self.startY == 0 then return end
	if self.endX == 0 or self.endY == 0 then return end
	if self.nameInput:getText() == "" then return end
	local newZone = WWP_WorkplaceZone:new(self.nameInput:getText(),
			self.typeComboBox:getOptionData(self.typeComboBox.selected),
			self.startX, self.startY, self.endX, self.endY, getPlayer():getZ())
	newZone:save()
	self.startX = 0
	self.startY = 0
	self.endX = 0
	self.endY = 0
	WWP_ManageZone:show(newZone, true)
	self:onClose()
end

function WWP_CreateZone:onClose()
	WWP_CreateZone.instance = nil
	self:removeFromUIManager()
end

function WWP_CreateZone:removeFromUIManager()
	self.groundHighlighter:remove()
	ISPanel.removeFromUIManager(self)
end

function WWP_CreateZone:getStartPointString()
	return string.format("(%d, %d)", self.startX, self.startY)
end

function WWP_CreateZone:getEndPointString()
	return string.format("(%d, %d)", self.endX, self.endY)
end

function WWP_CreateZone:showHightlight()
	if self.startX == 0 or self.startY == 0 then return end
	if not self.showHighlightCheckbox:isSelected(1) then return end
	local sx, sy = self.startX, self.startY
	local ex, ey
	if self.endX > 0 and self.endY > 0 then
		ex, ey = self.endX, self.endY
	else
		local player = getPlayer()
		ex, ey = player:getX(), player:getY()
	end
	sx,ex = math.floor(math.min(sx, ex)), math.floor(math.max(sx, ex))
	sy,ey = math.floor(math.min(sy, ey)), math.floor(math.max(sy, ey))

	if self.lastHighlight.x1 == sx and self.lastHighlight.y1 == sy and self.lastHighlight.x2 == ex and self.lastHighlight.y2 == ey then
		return
	end

	self.lastHighlight.x1 = sx
	self.lastHighlight.y1 = sy
	self.lastHighlight.x2 = ex
	self.lastHighlight.y2 = ey
	self.groundHighlighter:highlightSquare(sx, sy, ex, ey, getPlayer():getZ())
end