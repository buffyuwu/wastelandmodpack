---
--- WWP_ManageZone.lua
--- 18/06/2023
---

if not isClient() then return end

require "GravyUI"
require "GroundHightlighter"
require "ISUI/ISPanel"
require "WWP_WorkplaceType"

WWP_ManageZone = ISPanel:derive("WWP_ManageZone")
WWP_ManageZone.instance = nil

local FONT_HGT_SMALL = getTextManager():getFontHeight(UIFont.Small)

function WWP_ManageZone:show(zone, isAdmin)
	if WWP_ManageZone.instance then
		WWP_ManageZone.instance:onClose()
	end
	if WWP_CreateZone.instance then
		WWP_CreateZone.instance:onClose()
	end
	if WWP_ListWorkplaces.instance then
		WWP_ListWorkplaces.instance:onClose()
	end

	local w = 450
	local h = 550
	local o = ISPanel:new(getCore():getScreenWidth()/2-w/2,getCore():getScreenHeight()/2-h/2, w, h)
	setmetatable(o, self)
	o.__index = self
	o.isAdminOrPartner = isAdmin or zone:isPartner(getPlayer():getUsername())
	o.zone = zone
	o.isAdmin = isAdmin
	o:initialise()
	o:addToUIManager()
	WWP_ManageZone.instance = o
	return o
end

function WWP_ManageZone:initialise()
	ISPanel.initialise(self)
	self.startX = 0
	self.startY = 0
	self.endX = 0
	self.endY = 0
	self.moveWithMouse = true
	self.groundHighlighter = GroundHightlighter:new()
	self.groundHighlighter:enableXray(true)

	local win = GravyUI.Node(self.width, self.height):pad(5)
	self.win = win
	local header, body, footer = win:rows({30, 1, 100}, 5)
	local header, headerRight = header:cols({1, 100}, 5)
	local fields, employees = body:rows({120, 1.0}, 5)
	local nameInput, typeInput, startPoint, endPoint, zLocation, openControl = fields:grid(6, {160, 1}, 6, 0)
	local employeesTitle, employeesList = employees:rows({20, 1.0}, 5)
	local startZLabelNode, startZTextBox, endZLabelNode, endZTextBox = zLocation[2]:cols(4, 5)
	local f1, f2, f3, f4 = footer:grid(4, 3, 3, 3)

	self.headerBox = header
	self.showHighlightCheckbox = headerRight:makeTickBox()
	self.groundHighlighter:setColor(0.99, 0.85, 0.05, 1.0)

	self.nameLabel = nameInput[1]
	self.typeLabel = typeInput[1]
	self.startPointLabel = startPoint[1]
	self.endPointLabel = endPoint[1]
	self.zLocationLabel = zLocation[1]
	self.openClosedLabel = openControl[1]
	self.startZLabel = startZLabelNode
	self.endZLabel = endZLabelNode

	if self.isAdmin then
		self.startZText = startZTextBox:makeTextBox(tostring(self.zone.minZ))
		self.endZText = endZTextBox:makeTextBox(tostring(self.zone.maxZ))
		self:addChild(self.startZText)
		self:addChild(self.endZText)
	else
		self.startZText = startZTextBox
		self.endZText = endZTextBox
	end

	self.employeesLabel = employeesTitle
	self.playerList = ISScrollingListBox:new(employeesList.left, employeesList.top, employeesList.width, employeesList.height);
	self.playerList:initialise();
	self.playerList:instantiate();
	self.playerList.itemheight = FONT_HGT_SMALL + 2 * 2;
	self.playerList.selected = 0;
	self.playerList.joypadParent = self;
	self.playerList.font = UIFont.NewSmall;
	self.playerList.doDrawItem = self.drawPlayers;
	self.playerList.drawBorder = true;

	if self.isAdminOrPartner then
		self.nameTextBox = nameInput[2]:makeTextBox(self.zone.name)
		self:addChild(self.nameTextBox)
	else
		self.nameForPlayer = nameInput[2]
	end

	self.startPointDisplay = startPoint[2]
	self.endPointDisplay = endPoint[2]
	self.openCheckbox = openControl[2]:makeTickBox()

	if self.isAdmin then
		self.typeComboBox = typeInput[2]:makeComboBox()
		for _, type in pairs(WWP_WorkplaceTypes) do
			self.typeComboBox:addOptionWithData(type.name, type)
		end
		self.typeComboBox:selectData(self.zone.type)
		self:addChild(self.typeComboBox)
	else
		self.zoneTypeForPlayer = typeInput[2]
	end

	if self.isAdminOrPartner then
		self.fireEmployeeButton = f1[1]:makeButton("Fire Employee", self, self.onFireEmployee)
		self.hireEmployeeButton = f1[2]:makeButton("Hire Employee", self, self.onAddEmployee)
		self.promoteEmployeeButton = f1[3]:makeButton("Promote Employee", self, self.onPromoteEmployee)
		self:addChild(self.fireEmployeeButton);
		self:addChild(self.hireEmployeeButton);
		self:addChild(self.promoteEmployeeButton);
	end

	if self.isAdmin then
		self.demoteButton = f2[3]:makeButton("Demote Employee", self, self.onDemoteEmployee)
		self.setStartButton = f3[1]:makeButton("Set Start", self, self.onStartButton)
		self.setEndButton = f3[2]:makeButton("Set End", self, self.onEndButton)
		self.deleteButton = f3[3]:makeButton("Delete Zone", self, self.onDeleteZone)
		self:addChild(self.demoteButton)
		self:addChild(self.setStartButton)
		self:addChild(self.setEndButton)
		self:addChild(self.deleteButton)
	end

	self.quitJobButton = f4[1]:makeButton("Quit Job", self, self.onQuitJob)
	self.closeButton = f4[3]:makeButton("Save and Exit", self, self.OnSaveAndExit)
	self:addChild(self.quitJobButton)
	self:addChild(self.closeButton)

	self:addChild(self.showHighlightCheckbox)
	self:addChild(self.openCheckbox)
	self:addChild(self.playerList);
	self.showHighlightCheckbox:addOption("Highlight?")
	self.showHighlightCheckbox:setSelected(1, self.isAdmin)
	self.openCheckbox:addOption("Open")
	self.openCheckbox:setSelected(1, self.zone.open)

	self:populateEmployeesList()
end

function WWP_ManageZone:drawPlayers(y, item, alt)
	local a = 0.9;
	self:drawRectBorder(0, (y), self:getWidth(), self.itemheight - 1, a, self.borderColor.r, self.borderColor.g, self.borderColor.b);

	if self.selected == item.index then
		self:drawRect(0, (y), self:getWidth(), self.itemheight - 1, 0.3, 0.7, 0.35, 0.15);
	end

	self:drawText(item.text, 10, y + 2, 1, 1, 1, a, self.font);
	return y + self.itemheight;
end

function WWP_ManageZone:populateEmployeesList()
	self.playerList:clear();

	for employee, isPartner in pairs(self.zone.employees) do
		local text = employee
		local tooltip = employee .. " merely works here and cannot hire or fire employees"
		if(isPartner) then
			text = text .. " (Partner)"
			tooltip = employee .. " can manage the business by hiring and firing employees"
		end
		local item0 = self.playerList:addItem(text, employee);
		item0.tooltip = tooltip
	end
end

function WWP_ManageZone:drawOption(text, labelBox)
	self:drawText(text, labelBox.left, labelBox.top + 2, 1, 1, 1, 1, UIFont.Small)
	local b = math.floor(self.y + labelBox.bottom + 4)
	local l = math.floor(self.x + 5)
	local r = math.floor(self.x + (self.win.width * 2 ) - 10)
	self:drawLine2(l, b, r, b, 1, 0.5, 0.5, 0.5)
end

function WWP_ManageZone:prerender()
	ISPanel.prerender(self)

	if self.showHighlightCheckbox:isSelected(1) and self.groundHighlighter.type == "none" then
		self:showHightlight()
	elseif not self.showHighlightCheckbox:isSelected(1) and self.groundHighlighter.type ~= "none" then
		self.groundHighlighter:remove()
	end
	self:drawTextCentre("Workplace Manager", self.headerBox.left + (self.headerBox.width/2), self.headerBox.top, 1, 1, 1, 1, UIFont.Medium)

	self:drawOption("Name:", self.nameLabel)

	if not self.isAdminOrPartner then
		self:drawOption(self.zone.name, self.nameForPlayer)
	end

	self:drawOption("Type:", self.typeLabel)
	if not self.isAdmin then
		self:drawOption(self.zone.type.name, self.zoneTypeForPlayer)
	end
	self:drawOption("Start Point:", self.startPointLabel)
	self:drawOption("End Point:", self.endPointLabel)
	self:drawOption("Z Levels:", self.zLocationLabel)
	self:drawOption("Start:", self.startZLabel)
	self:drawOption("End:", self.endZLabel)

	if not self.isAdmin then
		self:drawOption(tostring(self.zone.minZ), self.startZText)
		self:drawOption(tostring(self.zone.maxZ), self.endZText)
	end

	self:drawOption("Public Status:", self.openClosedLabel)
	self:drawOption("Employees", self.employeesLabel)

	self:drawText(self:getStartPointString(), self.startPointDisplay.left, self.startPointDisplay.top, 1, 1, 1, 1, UIFont.Small)
	self:drawText(self:getEndPointString(), self.endPointDisplay.left, self.endPointDisplay.top, 1, 1, 1, 1, UIFont.Small)
end

function WWP_ManageZone:getStartPointString()
	return string.format("(%d, %d)", self.zone.minX, self.zone.maxX)
end

function WWP_ManageZone:getEndPointString()
	return string.format("(%d, %d)", self.zone.minY, self.zone.maxY)
end

function WWP_ManageZone:OnSaveAndExit()
	self:onSaveZone()
	WWP_ManageZone.instance = nil
	self:removeFromUIManager()
end

function WWP_ManageZone:onSaveZone()
	self.zone.open = self.openCheckbox:isSelected(1)

	if(self.isAdminOrPartner) then
		self.zone.name = self.nameTextBox:getText()
	end

	if(self.isAdmin) then
		self.zone.type = self.typeComboBox:getOptionData(self.typeComboBox.selected)
		self.zone.minZ = tonumber(self.startZText:getText())
		self.zone.maxZ = tonumber(self.endZText:getText())
	end

	self.zone:save()
end

function WWP_ManageZone:removeFromUIManager()
	self.showHighlightCheckbox:setSelected(1, false)
	self.groundHighlighter:remove()
	ISPanelJoypad.removeFromUIManager(self)
end

function WWP_ManageZone:onAddEmployee()
	WWP_AddPlayer:show(self)
end

function WWP_ManageZone:addEmployee(username)
	self.zone:addEmployee(username)
	self:populateEmployeesList()
end

function WWP_ManageZone:onPromoteEmployee()
	local username = self.playerList.items[self.playerList.selected].item
	if(username) then
		local txt = "Are you sure you want to promote " .. username .. "?\n\nYou cannot remove a partner once they have been promoted!"
		local modal = ISModalDialog:new(0,0, 350, 150, txt, true, username,
				WWP_ManageZone.confirmPromoteEmployee, nil, self);
		modal:initialise()
		modal:addToUIManager()
		modal.ui = self;
		modal.moveWithMouse = true;
	end
end

function WWP_ManageZone:onDemoteEmployee()
	local username = self.playerList.items[self.playerList.selected].item
	if(username) then
		self.zone:demoteEmployee(username)
		self:populateEmployeesList()
	end
end

function WWP_ManageZone.confirmPromoteEmployee(username, button, self)
	if(button.internal == "YES") then
		self.zone:promoteEmployee(username)
		self:populateEmployeesList()
	end
end

function WWP_ManageZone:onQuitJob()
	local player = getPlayer()
	if player and self.zone:isEmployee(player) then
		local modal = ISModalDialog:new(0,0, 350, 150,  "Are you sure you want to quit?",
			true, player:getUsername(), WWP_ManageZone.confirmEmployeeRemoved, nil, self);
		modal:initialise()
		modal:addToUIManager()
		modal.ui = self;
		modal.moveWithMouse = true;
	end
end

function WWP_ManageZone:onFireEmployee()
	local username = self.playerList.items[self.playerList.selected].item
	if(username) then
		if not self.isAdmin and self.zone:isPartner(username) then return end
		local txt = "Are you sure you want to fire " .. username .. "?"
		local modal = ISModalDialog:new(0,0, 350, 150, txt, true, username,
				WWP_ManageZone.confirmEmployeeRemoved, nil, self);
		modal:initialise()
		modal:addToUIManager()
		modal.ui = self;
		modal.moveWithMouse = true;
	end
end

function WWP_ManageZone.confirmEmployeeRemoved(username, button, self)
	if(button.internal == "YES") then
		self.zone:fireEmployee(username)
		self:populateEmployeesList()
		local player = getPlayer()
		if player and player:getUsername() == username then
			self:OnSaveAndExit()
		end
	end
end

function WWP_ManageZone:onDeleteZone()
	self.zone:delete()
	WWP_ManageZone.instance = nil
	self:removeFromUIManager()
end

function WWP_ManageZone:onStartButton()
	local player = getPlayer()
	local x, y = player:getX(), player:getY()
	if x > self.zone.maxX or y > self.zone.maxY then
		player:setHaloNote("The start point must be North West of the end point")
		return
	end
	self.zone.minX = math.floor(x)
	self.zone.minY = math.floor(y)
	if self.showHighlightCheckbox:isSelected(1) then
		self:showHightlight()
	end
end

function WWP_ManageZone:onEndButton()
	local player = getPlayer()
	local x, y = player:getX(), player:getY()
	if x < self.zone.minX or y < self.zone.minY then
		player:setHaloNote("The end point must be South East of the start point")
		return
	end
	self.zone.maxX = math.floor(x)
	self.zone.maxY = math.floor(y)
	if self.showHighlightCheckbox:isSelected(1) then
		self:showHightlight()
	end
end

function WWP_ManageZone:showHightlight()
	if self.zone.minX == 0 or self.zone.minY == 0 then return end
	local sx, sy = self.zone.minX, self.zone.minY
	local ex, ey = self.zone.maxX, self.zone.maxY
	self.groundHighlighter:highlightSquare(sx, sy, ex, ey, getPlayer():getZ())
end