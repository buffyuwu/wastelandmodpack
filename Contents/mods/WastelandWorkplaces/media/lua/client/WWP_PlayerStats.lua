---
--- WWP_PlayerStats.lua
--- Manages the player's stats and how much they have worked.
--- 07/07/2023
---

if not isClient() then return end

WWP_PlayerStats = {}
WWP_PlayerStats.millisToFullWorkPointsRestored = 72000000

---@param player IsoPlayer to check work data for
---@return table with percentRemaining and lastCheckedAt keys
function WWP_PlayerStats.getPlayerWorkData(player)
	local workData = player:getModData().WWP_WorkData
	if not workData then -- data is missing, so make some and set it in
		workData = { percentRemaining = 50, lastCheckedAt = getTimestampMs()}
		player:getModData().WWP_WorkData = workData
	else -- else we had some work data to use so lets update it before returning
		WWP_PlayerStats.updatePlayerWorkPoints(workData)
	end
	return workData
end

function WWP_PlayerStats.updatePlayerWorkPoints(workData)
	local currentTimeMillis = getTimestampMs()
	if workData.percentRemaining ~= 100 then
		local millisSinceLastCheck = currentTimeMillis - workData.lastCheckedAt
		local percentToRestore = (millisSinceLastCheck /  WWP_PlayerStats.millisToFullWorkPointsRestored) * 100
		local newPercent = workData.percentRemaining + percentToRestore
		workData.percentRemaining = math.min(newPercent, 100)
	end
	workData.lastCheckedAt = currentTimeMillis
end