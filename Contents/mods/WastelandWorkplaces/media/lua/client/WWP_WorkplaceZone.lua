---
--- WWP_WorkplaceZone.lua
--- 18/06/2023
---

if not isClient() then return end

require "WWP_WorkplaceType"
require "WWP_PlayerStats"

--- @class WWP_WorkplaceZone
WWP_WorkplaceZone = WWP_WorkplaceZone or {}
--- @type WWP_WorkplaceZone[]
WWP_WorkplaceZones = {}

function WWP_WorkplaceZone.getAllZones()
	return WWP_WorkplaceZones
end

function WWP_WorkplaceZone.getZonesAt(x, y, z)
	local zones = {}
	for _, zone in pairs(WWP_WorkplaceZones) do
		if zone:isInZone(x, y, z) then
			table.insert(zones, zone)
		end
	end
	return zones
end

function WWP_WorkplaceZone.getZone(zoneId)
	return WWP_WorkplaceZones[zoneId]
end

---@param name string Human visible name
function WWP_WorkplaceZone:new(name, type, x1, y1, x2, y2, zLevel)
	--- @type WWP_WorkplaceZone
	local o = {}
	setmetatable(o, self)
	self.__index = self
	o:init()
	o.name = name
	o.type = type
	o.minX = math.min(x1, x2)
	o.minY = math.min(y1, y2)
	o.maxX = math.max(x1, x2)
	o.maxY = math.max(y1, y2)
	o.minZ = zLevel
	o.maxZ = zLevel
	o:save()
	return o
end

function WWP_WorkplaceZone:loadFrom(o)
	setmetatable(o, self)
	self.__index = self
	if o.minZ == nil then o.minZ = 0 end
	if o.maxZ == nil then o.maxZ = 0 end
	o.type = WWP_WorkplaceTypes[o.typeKey]
	return o
end

function WWP_WorkplaceZone:init()
	--- @type string
	self.id = getRandomUUID()
	self.name = ""
	self.type = ""
	self.open = true
	self.employees = {}
	self.minX = 0
	self.minY = 0
	self.maxX = 0
	self.maxY = 0
	self.minZ = 0
	self.maxZ = 0
end

function WWP_WorkplaceZone:save()
	sendClientCommand(getPlayer(), "WastelandWorkplaces", "SetZone", {
		id = self.id,
		name = self.name,
		typeKey = self.type.key,
		open = self.open,
		employees = self.employees,
		minX = self.minX,
		minY = self.minY,
		maxX = self.maxX,
		maxY = self.maxY,
		minZ = self.minZ,
		maxZ = self.maxZ,
	})
end

function WWP_WorkplaceZone:delete()
	sendClientCommand(getPlayer(), "WastelandWorkplaces", "DeleteZone", {id = self.id})
end

function WWP_WorkplaceZone:isInZone(x, y, z)
	if x >= self.minX and x <= (self.maxX+1)
			and y >= self.minY and y <= (self.maxY+1)
			and z >= self.minZ and z <= self.maxZ then
		return true
	end
	return false
end

function WWP_WorkplaceZone:isEmployee(player)
	return self.employees[player:getUsername()] ~= nil
end

function WWP_WorkplaceZone:isPartner(username)
	return self.employees[username]
end

function WWP_WorkplaceZone:fireEmployee(username)
	self.employees[username] = nil
	self:save()
end

function WWP_WorkplaceZone:addEmployee(username)
	if not self.employees[username] then
		self.employees[username] = false
		self:save()
	end
end

function WWP_WorkplaceZone:promoteEmployee(username)
	self.employees[username] = true
	self:save()
end

function WWP_WorkplaceZone:demoteEmployee(username)
	self.employees[username] = false
	self:save()
end

function WWP_WorkplaceZone:isPlayerInZone(player)
	return self:isInZone(player:getX(), player:getY(), player:getZ())
end

function WWP_WorkplaceZone:onEnter(player)
	local entryMessage = "Entering " .. self.name
	if(self:isEmployee(player)) then
		if not self.open then
			entryMessage = entryMessage .. "\nClosed: Open to gain work benefits"
			player:setHaloNote(entryMessage, 200, 200, 200, 200.0)
			return
		end
		entryMessage = entryMessage .. "\n" .. self:getWorkPercentLeftString(player)
		player:setHaloNote(entryMessage, 253, 216, 12, 500.0)
	else
		if not self.open then
			entryMessage = entryMessage .. "\n***  CLOSED  ***"
			player:setHaloNote(entryMessage, 250, 20, 60, 200.0)
			return
		end

		if self.type:requireEmployeesForBenefits() and (#self.type:getBenefits()) > 0 then
			local employees, _ = self:countPlayersInZone()
			if employees == 0 then
				entryMessage = entryMessage .. "\nNo Employees present"
				player:setHaloNote(entryMessage, 200, 200, 200, 350.0)
				return
			end
		end

		for _, benefit in pairs(self.type:getBenefits()) do
			entryMessage = entryMessage .. "\n" .. benefit
		end
		player:setHaloNote(entryMessage, 124, 252, 0, 500.0)
	end
end

function WWP_WorkplaceZone:countPlayersInZone()
	local employeeCount = 0
	local visitorCount = 0
	local players = getOnlinePlayers()
	players = players or ArrayList.new()
	for playerIndex = 0, players:size() -1 do
		local p = players:get(playerIndex)
		local isInZone = self:isInZone(p:getX(), p:getY(), p:getZ())

		if isInZone then
			if self:isEmployee(p) then
				employeeCount = employeeCount + 1
			else
				visitorCount = visitorCount + 1
			end
		end
	end

	return employeeCount, visitorCount
end

function WWP_WorkplaceZone:getWorkPercentLeftString(player)
	local workData = WWP_PlayerStats.getPlayerWorkData(player)
	return "Work Remaining: " .. string.format("%d", workData.percentRemaining) .. "%"
end

function WWP_WorkplaceZone:onExit(player)
	local msg = "Leaving " .. self.name
	if(self:isEmployee(player)) then
		msg = msg .. "\n" .. self:getWorkPercentLeftString(player)
	end
	player:setHaloNote(msg, 200, 200, 200, 250.0)
end

function WWP_WorkplaceZone:perMinute(player)
	if not self.open then return end

	if self.type:requireEmployeesForBenefits() and (#self.type:getBenefits()) > 0 then
		local employees, _ = self:countPlayersInZone()
		if employees == 0 then return end
	end

	self.type:applyBenefits(player)
end

function WWP_WorkplaceZone:perTwoMinutes(player)
	if self.type:doSpeedyTicks() then
		self:doWorkTick(player)
	end
end

function WWP_WorkplaceZone:perFiveMinutes(player)
	if not self.type:doSpeedyTicks() then
		self:doWorkTick(player)
	end
end

function WWP_WorkplaceZone:doWorkTick(player)
	if not self.open then return end
	if not self:isEmployee(player) then return end
	local workData = WWP_PlayerStats.getPlayerWorkData(player)
	if(workData.percentRemaining - self.type:getWorkPercent() >= 0) then

		if self.type:requireSomeonePresentForRewards() then
			local employees, visitors = self:countPlayersInZone()
			if self.type:requireCustomersForRewards() then
				if visitors == 0 then
					rewardString = "No customers present\n"  .. self:getWorkPercentLeftString(player)
					player:setHaloNote(rewardString, 200, 200, 200, 350.0)
					return
				end
			else -- Then anyone will do
				if visitors == 0 and employees < 2 then
					rewardString = "Nobody around\n"  .. self:getWorkPercentLeftString(player)
					player:setHaloNote(rewardString, 200, 200, 200, 350.0)
					return
				end
			end
		end

		workData.percentRemaining = workData.percentRemaining - self.type:getWorkPercent()
		local rewardString = self.type:generateReward(player)
		rewardString = "Gained " .. rewardString .. "\n"  .. self:getWorkPercentLeftString(player)
		player:setHaloNote(rewardString, 253, 216, 12, 450.0)
	end
end