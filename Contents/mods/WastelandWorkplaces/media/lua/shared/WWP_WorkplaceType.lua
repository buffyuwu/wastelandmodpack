---
--- WWP_WorkplaceType.lua
--- 18/06/2023
---
require "XPController"

WWP_WorkplaceTypes = WWP_WorkplaceTypes or {}

-- WorkplaceType base class
---@public field name string
WWP_WorkplaceType = {}

---@param key string This MUST NOT EVER CHANGE once you put it in
---@param name string Human visible name. Change it as much as you want.
---@param percentXP int What % of rewards will be XP instead of loot items
---@param lootTable table of loot and weighting to how likely it appears
---@param xpTable table for XP, works similar to above but includes how much XP they get if it comes up as a reward
function WWP_WorkplaceType:new(key, name, percentXP, lootTable, xpTable)
	local obj = {}
	setmetatable(obj, self)
	self.__index = self
	obj.name = name
	obj.key = key
	obj.percentXP = percentXP
	obj.lootTable = {}
	obj:appendLootTable(lootTable)
	obj.xpTable = xpTable
	obj:initialise()
	WWP_WorkplaceTypes[key] = obj
	return obj
end

function WWP_WorkplaceType:initialise()
	self.totalXPWeight = 0
	for _, row in ipairs(self.xpTable) do
		self.totalXPWeight = self.totalXPWeight + row.weighting
	end
end

function WWP_WorkplaceType:appendLootTable(lootTable)
	for item, weight in pairs(lootTable) do
		self.lootTable[item] = weight
	end

	-- Recalculate total weight
	self.totalWeight = 0
	for item, weight in pairs(self.lootTable) do
		self.totalWeight = self.totalWeight + weight
	end
end

--- Get an array of string describing the benefits of the establishment
-- e.g. {"+ Energy", "+ Happiness" }
function WWP_WorkplaceType:getBenefits() return {} end

--- Applies benefits like + happiness or healing to the player inside
------@param player IsoGameCharacter
function WWP_WorkplaceType:applyBenefits(player) end

--- What percent of work points are taken from the player each hour at this workplace
function WWP_WorkplaceType:getWorkPercent() return 10 end

---@return boolean true if visitors don't get benefits if no employee is present (e.g. no doctor means no bonus healing from the clinic)
function WWP_WorkplaceType:requireEmployeesForBenefits() return true end

---@return boolean true if employees don't get paid/xp etc unless SOMEONE is there. Can be another employee!
function WWP_WorkplaceType:requireSomeonePresentForRewards() return true end

---@return boolean true if employees don't get paid/xp etc unless a non-employee is around for them to server
---Note that this won't do anything unless :requireSomeonePresentForRewards is also set to true.
function WWP_WorkplaceType:requireCustomersForRewards() return false end

---@return boolean true if we do speedy ticks for employee work points (2 minutes) instead of slow ticks (5 minutes)
function WWP_WorkplaceType:doSpeedyTicks() return false end

--- Generates a reward and returns a string describing it e.g. "Item: Donuts" or "Skill: Nimble 100XP"
---@param player IsoGameCharacter
function WWP_WorkplaceType:generateReward(player)
	local random = ZombRand(0,  100)
	if(random < self.percentXP) then

		local perk, amount = self:rollXP()

		local cap = getLevelCap(player, perk)
		local isSkillCapped = (cap ~= nil) and (player:getPerkLevel(perk) >= cap)
		if isSkillCapped then
			return self:giveRandomLootReward(player)
		end

		local perkBoost = player:getXp():getPerkBoost(perk)
		amount = amount * (perkBoost + 1) -- Not perfect but okay to increase XP gains
		player:getXp():AddXP(perk, amount, false, false, false);
		local xpAmountString = tostring(amount) .. "XP"

		-- Check if we just leveled up
		isSkillCapped = (cap ~= nil) and (player:getPerkLevel(perk) >= cap)
		if isSkillCapped then  -- If we have hit the cap, set XP to the cap level exactly
			player:getXp():setXPToLevel(perk, cap)
			xpAmountString = "MAX LEVEL"
		end

		return perk:getName() .. ": " .. xpAmountString
	else
		return self:giveRandomLootReward(player)
	end
end

function WWP_WorkplaceType:rollXP()
	local randomNumber = ZombRand(1, self.totalXPWeight+1)
	local cumulativeWeighting = 0
	for _, row in ipairs(self.xpTable) do
		cumulativeWeighting = cumulativeWeighting + row.weighting
		if randomNumber <= cumulativeWeighting then
			return row.perk, row.amount
		end
	end
end

---@param player IsoGameCharacter
function WWP_WorkplaceType:giveRandomLootReward(player)
	local randomNumber = ZombRand(1,  self.totalWeight+1)
	local cumulativeWeight = 0
	for item, weight in pairs(self.lootTable) do
		cumulativeWeight = cumulativeWeight + weight
		if randomNumber <= cumulativeWeight then
			local inventory = player:getInventory()
			local newItem = inventory:AddItem(item)
			if(newItem) then
				return "Item: " .. newItem:getName()
			else
				return "ERROR: CANNOT FIND ITEM " .. item
			end
		end
	end
	return "fulfilment"	-- shouldn't ever reach this point
end


--- Utility method
---@param player IsoGameCharacter
function WWP_WorkplaceType:decreaseUnhappy(player, amount)
	local sadnessAdjust = player:getBodyDamage():getUnhappynessLevel()
	if(sadnessAdjust > 0) then
		sadnessAdjust = math.max(0, sadnessAdjust - amount)
		player:getBodyDamage():setUnhappynessLevel(sadnessAdjust)
	end
end

--- Utility method
---@param player IsoGameCharacter
function WWP_WorkplaceType:decreaseBoredom(player, amount)
	local boredomAdjust =  player:getBodyDamage():getBoredomLevel()
	if(boredomAdjust > 0) then
		boredomAdjust = math.max(0, boredomAdjust - amount)
		player:getBodyDamage():setBoredomLevel(boredomAdjust)
	end
end

--- Utility method
---@param player IsoGameCharacter
function WWP_WorkplaceType:decreaseStress(player, amount)
	amount = amount / 100 -- Stress is a decimal from 0.0 to 1.0, so consistent!
	local stats = player:getStats()
	local stressAdjust = stats:getStress()
	if(stressAdjust > 0) then
		stressAdjust = math.max(0, stressAdjust - amount)
		stats:setStress(stressAdjust)
	end
end

--------------------------- Workplace Type Definitions  -------------------------------

WorkplaceBar = WWP_WorkplaceType:new("bar", "Bar", 25, {
	["Base.BeerBottle"] = 9,
	["Base.BeerCan"] = 2,
	["Base.WhiskeyFull"] = 2,
	["Base.Wine"] = 1,
	["Base.Wine2"] = 1,
	["Base.Peanuts"] = 3,
	["Base.Crisps"] = 1,
	["Base.Crisps2"] = 1,
	["Base.Pop"] = 1,
	["Base.Pop2"] = 1,
	["Base.TortillaChips"] = 2,
}, { 		-- see PerkFactory.Perks : zombie.characters.skills.PerkFactory.Perks
	{ perk = Perks.Cooking, amount = 50, weighting = 3 },
	{ perk = Perks.Nimble, amount = 35, weighting = 3 },
})

if getActivatedMods():contains("sapphcooking") then
	WorkplaceBar:appendLootTable({
		["SapphCooking.SakeFull"] = 2,
		["SapphCooking.RumFull"] = 3,
		["SapphCooking.GinFull"] = 2,
		["SapphCooking.ColaBottle"] = 3,
	})
end

function WorkplaceBar:getBenefits()
	return {"+ Happiness", "- Boredom"}
end

---@param player IsoGameCharacter
function WorkplaceBar:applyBenefits(player)
	WWP_WorkplaceType:decreaseBoredom(player, 10)
	WWP_WorkplaceType:decreaseUnhappy(player, 10)
end

function WorkplaceBar:requireCustomersForRewards()
	return true
end

WorkplaceClinic = WWP_WorkplaceType:new("clinic", "Clinic", 70, {
	["Base.Bandage"] = 7,
	["Base.SutureNeedle"] = 2,
	["Base.Tissue"] = 3,
	["Base.CottonBalls"] = 3,
	["Base.Pills"] = 1,
	["Base.PillsBeta"] = 1,
	["Base.PillsAntiDep"] = 1,
	["Base.Antibiotics"] = 4,
	["Base.AlcoholWipes"] = 1,
}, {
	{ perk = Perks.Doctor, amount = 50, weighting = 8 },
	{ perk = Perks.Lightfoot, amount = 50, weighting = 2 },
	{ perk = Perks.SmallBlade, amount = 50, weighting = 1 },
})

function WorkplaceClinic:getBenefits()
	return {"+ Healing"}
end

---@param player IsoGameCharacter
function WorkplaceClinic:applyBenefits(player)
	local sicknessAdjust = player:getBodyDamage():getFoodSicknessLevel() - 3
	sicknessAdjust = math.max(sicknessAdjust, 0)
	player:getBodyDamage():setFoodSicknessLevel(sicknessAdjust)

	for i = 0, player:getBodyDamage():getBodyParts():size() - 1 do
		local bodyPart = player:getBodyDamage():getBodyParts():get(i);

		local bleedingTime = bodyPart:getBleedingTime()
		if(bleedingTime > 0) then
			bleedingTime = math.max(0, bleedingTime - 0.1)
			bodyPart:setBleedingTime(bleedingTime)
		end

		local scratchTime = bodyPart:getScratchTime()
		if(scratchTime > 0) then
			scratchTime = math.max(0, scratchTime - 0.3)
			bodyPart:setScratchTime(scratchTime)
		end

		local cutTime = bodyPart:getCutTime()
		if(cutTime > 0) then
			cutTime = math.max(0, cutTime - 0.12)
			bodyPart:setCutTime(cutTime)
		end

		local deepWoundTime = bodyPart:getDeepWoundTime()
		if(deepWoundTime > 0) then
			deepWoundTime = math.max(0, deepWoundTime - 0.06)
			bodyPart:setDeepWoundTime(deepWoundTime)
		end

		local fractureTime = bodyPart:getFractureTime()
		if(fractureTime > 0) then
			fractureTime = math.max(0, fractureTime - 0.01)
			bodyPart:setFractureTime(fractureTime)
		end

		local biteTime = bodyPart:getBiteTime()
		if(biteTime > 0) then
			biteTime = math.max(0, biteTime - 0.2)
			bodyPart:setBiteTime(biteTime)
		end
	end
end

function WorkplaceClinic:requireCustomersForRewards()
	return true
end

WorkplaceLibrary = WWP_WorkplaceType:new("library","Library", 80, {
	["Base.SheetPaper2"] = 4,
	["Base.Notebook"] = 1,
	["Base.Book"] = 6,
	["Base.Newspaper"] = 2,
	["Base.Magazine"] = 2,
	["Base.Peppermint"] = 1,
}, {
	{ perk = Perks.Lightfoot, amount = 35, weighting = 2 },
})

function WorkplaceLibrary:getBenefits()
	return {"- Boredom"}
end

---@param player IsoGameCharacter
function WorkplaceLibrary:applyBenefits(player)
	WWP_WorkplaceType:decreaseBoredom(player, 10)
end

function WorkplaceLibrary:requireEmployeesForBenefits()
	return false
end

function WorkplaceLibrary:requireSomeonePresentForRewards()
	return false
end


WorkplaceChineseRestaurant = WWP_WorkplaceType:new("restaurant_chinese", "Chinese Restaurant", 25, {
	["Base.Soysauce"] = 1,
	["Base.RiceVinegar"] = 1,
	["Base.OilVegetable"] = 1,
	["Base.Honey"] = 1,
	["Base.Butter"] = 1,
	["Base.Avocado"] = 3,
	["Base.Seaweed"] = 3,
	["Base.MeatDumpling"] = 2,
	["Base.ShrimpDumpling"] = 2,
	["Base.BakingSoda"] = 1,
}, {
	{ perk = Perks.Cooking, amount = 70, weighting = 10 },
	{ perk = Perks.Nimble, amount = 30, weighting = 2 },
	{ perk = Perks.SmallBlade, amount = 20, weighting = 2 },
})

if getActivatedMods():contains("sapphcooking") then
	WorkplaceChineseRestaurant:appendLootTable({
		["SapphCooking.SoySauce_Sachet"] = 12,
		["SapphCooking.HotsaucePacket"] = 7,
		["SapphCooking.MonosodiumGlutamate_MSG"] = 2,
		["SapphCooking.SaltPacket"] = 6,
		["SapphCooking.CurryPowder"] = 3,
		["SapphCooking.BagofWontonWrappers"] = 12,
	})
end

function WorkplaceChineseRestaurant:getBenefits()
	return {"+ Happiness", "- Stress"}
end

---@param player IsoGameCharacter
function WorkplaceChineseRestaurant:applyBenefits(player)
	WWP_WorkplaceType:decreaseUnhappy(player, 10)
	WWP_WorkplaceType:decreaseStress(player, 10)
end

function WorkplaceChineseRestaurant:requireCustomersForRewards()
	return true
end

WorkplaceFrenchRestaurant = WWP_WorkplaceType:new("restaurant_french", "French Restaurant", 25, {
	["Base.Wine"] = 4,
	["Base.Wine2"] = 12,
	["Base.FrogMeat"] = 2,
	["Base.Steak"] = 8,
	["Base.Yeast"] = 2,
	["Base.Cheese"] = 2,
	["Base.Pepper"] = 1,
	["Base.OilOlive"] = 1,
	["Base.Vinegar"] = 1,
	["Base.MeatPatty"] = 5,
	["Base.EggCarton"] = 3,
	["Base.Chocolate"] = 2,
	["Base.Milk"] = 3,
	["Base.Salmon"] = 6,
	["Base.CakeCheesecake"] = 1,
	["Base.CakeBlackForest"] = 1,
	["Base.CakeChocolate"] = 1,
	["Base.CakeRedVelvet"] = 1,
	["Base.CakeStrawberryShortcake"] = 1,
	["Base.PiePumpkin"] = 1,
	["Base.Cupcake"] = 1,
	["Base.CakeChocolate"] = 1,
	["Base.CakeChocolate"] = 1,
	["Base.SugarBrown"] = 3,
	["Base.Butter"] = 3,
	["Base.BakingSoda"] = 1,
}, {
	{ perk = Perks.Cooking, amount = 70, weighting = 10 },
	{ perk = Perks.Nimble, amount = 30, weighting = 2 },
	{ perk = Perks.SmallBlade, amount = 20, weighting = 2 },
})

if getActivatedMods():contains("sapphcooking") then
	WorkplaceFrenchRestaurant:appendLootTable({
		["SapphCooking.WhiteChocolate"] = 2,
		["SapphCooking.SapphCookingSugar"] = 3,
	})
end

function WorkplaceFrenchRestaurant:getBenefits()
	return {"+ Happiness", "- Stress"}
end

---@param player IsoGameCharacter
function WorkplaceFrenchRestaurant:applyBenefits(player)
	WWP_WorkplaceType:decreaseUnhappy(player, 10)
	WWP_WorkplaceType:decreaseStress(player, 10)
end

function WorkplaceFrenchRestaurant:requireCustomersForRewards()
	return true
end

WorkplaceTailor = WWP_WorkplaceType:new("tailor","Tailor", 50, {
	["Base.Yarn"] = 7,
	["Base.Thread"] = 8,
	["Base.Twine"] = 1,
	["Base.LeatherStrips"] = 3,
	["Base.DenimStrips"] = 3,
	["Base.RippedSheets"] = 1,

}, {
	{ perk = Perks.Tailoring, amount = 70, weighting = 10 },
	{ perk = Perks.Maintenance, amount = 20, weighting = 1 },
})

function WorkplaceTailor:requireSomeonePresentForRewards() return false end


WorkplaceGeneralStore= WWP_WorkplaceType:new("general_store", "General Store", 5, {
	["Base.EmptySandbag"] = 2,
	["Base.AlarmClock"] = 1,
	["Base.Earbuds"] = 2,
	["Base.WalkieTalkie3"] = 1,
	["Base.Extinguisher"] = 1,
	["Base.CandleLit"] = 2,
	["Base.HandTorch"] = 1,
	["Base.Matches"] = 2,
	["Base.Pot"] = 1,
	["Base.Pan"] = 1,
	["Base.Woodglue"] = 2,
	["Base.DuctTape"] = 2,
	["Base.FishingTackle"] = 1,
	["Base.FishingTackle2"] = 1,
	["Base.FishingLine"] = 2,
	["Base.EmptyPetrolCan"] = 1,
	["Base.GardeningSprayEmpty"] = 1,
	["Base.Battery"] = 2,
	["Base.LightBulb"] = 1,
	["Base.Rope"] = 2,
	["Base.NailsBox"] = 1,
	["Base.Screws"] = 1,
	["Base.WeldingRods"] = 1,
	["Base.Wire"] = 1,
	["Base.BucketEmpty"] = 1,
	["Base.Flour"] = 1,
	["Base.Vinegar"] = 1,
	["Base.Wire"] = 1,
	["Base.Sugar"] = 1,
	["Base.Twine"] = 2,
	["Base.Cigarettes"] = 2,
	["Base.Notebook"] = 1,
	["Base.Coffee2"] = 1,
	["Base.CocoaPowder"] = 1,
	["Base.Teabag2"] = 1,
	["Base.KnittingNeedles"] = 1,
	["Base.Needle"] = 1,
	["Base.Fertilizer"] = 2,
	["Base.PlasterPowder"] = 2,
	["farming.BroccoliBagSeed"] = 1,
	["farming.CabbageBagSeed"] = 1,
	["farming.CarrotBagSeed"] = 1,
	["farming.PotatoBagSeed"] = 1,
	["farming.RedRadishBagSeed"] = 1,
	["farming.StrewberrieBagSeed"] = 1,
	["farming.TomatoBagSeed"] = 1,
	["farming.HandShovel"] = 1,
	["Base.GardenHoe"] = 1,
	["Base.GardenFork"] = 1,
	["Base.Pencil"] = 1,
	["Base.Eraser"] = 1,
	["Base.TrapCage"] = 1,

	-- Cans
	["Base.TinnedBeans"] = 1,
	["Base.CannedCarrots"] = 1,
	["Base.CannedCorn"] = 1,
	["Base.CannedCornedBeef"] = 1,
	["Base.CannedFruitCocktail"] = 1,
	["Base.CannedMushroomSoup"] = 1,
	["Base.CannedPeaches"] = 1,
	["Base.CannedPeas"] = 1,
	["Base.CannedPineapple"] = 1,
	["Base.CannedPotato2"] = 1,
	["Base.CannedSardines"] = 2,
	["Base.TinnedSoup"] = 1,
	["Base.CannedBolognese"] = 1,
	["Base.CannedTomato2"] = 1,
	["Base.TunaTin"] = 1,
	["Base.Dogfood"] = 1,

	-- Jars
	["Base.CannedBellPepper"] = 1,
	["Base.CannedBroccoli"] = 1,
	["Base.CannedCabbage"] = 1,
	["Base.CannedCarrots"] = 1,
	["Base.CannedEggplant"] = 1,
	["Base.CannedLeek"] = 2,
	["Base.CannedPotato"] = 1,
	["Base.CannedRedRadish"] = 1,
	["Base.CannedTomato"] = 1,

	["Base.Bleach"] = 3,
	["Base.CleaningLiquid"] = 3,
	["Base.Mop"] = 1,
	["Base.Hammer"] = 1,
	["Base.Saw"] = 1,
	["Base.Screwdriver"] = 1,
	["Base.WoodAxe"] = 1,
	["Base.Tarp"] = 1,
	["Base.Doll"] = 1,
	["Base.WaterDish"] = 1,
	["Base.DogChew"] = 1,

	-- Paint
	["Base.PaintBlack"] = 1,
	["Base.PaintBlack"] = 1,
	["Base.PaintBlue"] = 1,
	["Base.PaintBrown"] = 1,
	["Base.Paintbrush"] = 1,
	["Base.PaintCyan"] = 1,
	["Base.PaintGreen"] = 1,
	["Base.PaintGrey"] = 1,
	["Base.PaintLightBlue"] = 1,
	["Base.PaintLightBrown"] = 1,
	["Base.PaintOrange"] = 1,
	["Base.PaintPink"] = 1,
	["Base.PaintPurple"] = 1,
	["Base.PaintRed"] = 1,
	["Base.PaintTurquoise"] = 1,
	["Base.PaintWhite"] = 1,
	["Base.PaintYellow"] = 1,

}, {
	{ perk = Perks.Electricity, amount = 25, weighting = 1 },
	{ perk = Perks.Maintenance, amount = 30, weighting = 2 },
})

function WorkplaceGeneralStore:doSpeedyTicks() return true end

if getActivatedMods():contains("SGarden-Homestead") then
	WorkplaceGeneralStore:appendLootTable({
		["Sprout.PearBagSeed"] = 1,
		["Sprout.WheatBagSeed"] = 1,
		["Sprout.ZucchiniBagSeed"] = 1,
		["Sprout.AppleBagSeed"] = 1,
		["Sprout.CherryBagSeed"] = 1,
		["Sprout.CornBagSeed"] = 1,
		["Sprout.GingerBagSeed"] = 1,
		["Sprout.GinsengBagSeed"] = 1,
		["Sprout.LeekBagSeed"] = 1,
		["Sprout.LettuceBagSeed"] = 1,
		["Sprout.OnionBagSeed"] = 1,
		["Sprout.PumpkinBagSeed"] = 1,
		["Sprout.SoyBeanBagSeed"] = 1,
	})
end

if getActivatedMods():contains("sapphcooking") then
	WorkplaceGeneralStore:appendLootTable({
		["SapphCooking.MetalWhisk"] = 1,
		["SapphCooking.DoughnutCutter"] = 1,
		["SapphCooking.PipingBags"] = 1,
		["SapphCooking.BakingMolds"] = 1,
		["SapphCooking.ClothFilter"] = 1,
		["SapphCooking.WoodenSkewers"] = 1,
		["SapphCooking.MessTray"] = 1,
		["SapphCooking.PizzaCutter"] = 1,
		["SapphCooking.Laddle"] = 1,
		["SapphCooking.MeatTenderizer"] = 1,
		["SapphCooking.WokPan"] = 1,
		["SapphCooking.EmptyThermos"] = 1,
		["SapphCooking.PlasticFilterHolder"] = 1,
		["SapphCooking.CoffeeGrinder"] = 1,
	})
end


WorkplaceConstruction = WWP_WorkplaceType:new("construction_services", "Construction Services", 30, {
	["Base.Vest_HighViz"] = 1,
	["Base.Hat_HardHat"] = 1,
	["Base.Toolbox"] = 1,
	["Base.Hammer"] = 1,
	["Base.Saw"] = 1,
	["Base.Screwdriver"] = 1,
	["Base.NailsBox"] = 2,
	["Base.Nails"] = 15,
	["Base.Screws"] = 8,
	["Base.Hinge"] = 4,
	["Base.Doorknob"] = 2,
	["Base.SheetMetal"] = 1,
	["Base.SmallSheetMetal"] = 1,
	["Base.PlasterPowder"] = 4,
	["Base.ConcretePowder"] = 1,
	["Base.Gravelbag"] = 8,
	["Base.BarbedWire"] = 3,
	["Base.Woodglue"] = 1,
	["Base.WeldingRods"] = 1,

	-- Paint
	["Base.PaintBlack"] = 1,
	["Base.PaintBlack"] = 1,
	["Base.PaintBlue"] = 1,
	["Base.PaintBrown"] = 1,
	["Base.Paintbrush"] = 1,
	["Base.PaintCyan"] = 1,
	["Base.PaintGreen"] = 1,
	["Base.PaintGrey"] = 1,
	["Base.PaintLightBlue"] = 1,
	["Base.PaintLightBrown"] = 1,
	["Base.PaintOrange"] = 1,
	["Base.PaintPink"] = 1,
	["Base.PaintPurple"] = 1,
	["Base.PaintRed"] = 1,
	["Base.PaintTurquoise"] = 1,
	["Base.PaintWhite"] = 1,
	["Base.PaintYellow"] = 1,
}, {
	{ perk = Perks.Woodwork, amount = 50, weighting = 10 },
	{ perk = Perks.MetalWelding, amount = 40, weighting = 5 },
	{ perk = Perks.Maintenance, amount = 20, weighting = 3 },
})

function WorkplaceConstruction:doSpeedyTicks() return true end

WorkplaceMechanicShop = WWP_WorkplaceType:new("mechanic_shop", "Mechanic Shop", 50, {
	["Base.Wrench"] = 1,
	["Base.Jack"] = 1,
	["Base.LugWrench"] = 1,
	["Base.CarBatteryCharger"] = 1,
	["Base.TirePump"] = 1,
	["Base.BlowTorch"] = 1,
	["Base.EmptyPetrolCan"] = 1,
	["Base.LightBulb"] = 6,
	["Base.Screwdriver"] = 1,
	["Base.DuctTape"] = 3,
	["Base.ScrapMetal"] = 5,
	["Base.SheetMetal"] = 3,
	["Base.SmallSheetMetal"] = 8,
	["Base.MetalBar"] = 4,
	["Base.MetalPipe"] = 4,
	["Base.Hinge"] = 15,
	["Base.Screws"] = 30,
	["Base.EngineParts"] = 10,
	["Base.NormalSuspension1"] = 1,
	["Base.NormalSuspension2"] = 1,
	["Base.NormalSuspension3"] = 1,
	["Base.ModernSuspension1"] = 1,
	["Base.ModernSuspension2"] = 1,
	["Base.ModernSuspension3"] = 1,
	["Base.NormalCarMuffler1"] = 1,
	["Base.NormalCarMuffler2"] = 1,
	["Base.NormalCarMuffler3"] = 1,
	["Base.ModernCarMuffler1"] = 1,
	["Base.ModernCarMuffler2"] = 1,
	["Base.ModernCarMuffler3"] = 1,
	["Base.SmallGasTank1"] = 1,
	["Base.SmallGasTank2"] = 1,
	["Base.SmallGasTank3"] = 1,
	["Base.NormalGasTank1"] = 1,
	["Base.NormalGasTank2"] = 1,
	["Base.NormalGasTank3"] = 1,
	["Base.BigGasTank1"] = 1,
	["Base.BigGasTank2"] = 1,
	["Base.BigGasTank3"] = 1,
}, {
	{ perk = Perks.Mechanics, amount = 70, weighting = 10 },
	{ perk = Perks.Maintenance, amount = 20, weighting = 1 },
})

function WorkplaceMechanicShop:getWorkPercent() return 20 end

if getActivatedMods():contains("ImprovisedGlass-Wasteland") then
	WorkplaceMechanicShop:appendLootTable({
		["ImprovisedGlass.GlassPane"] = 3,
	})
end

if getActivatedMods():contains("WastelandCarParts") then
	WorkplaceMechanicShop:appendLootTable({
		["Base.CuredRubber"] = 3,
	})
end


WorkplaceFarm = WWP_WorkplaceType:new("farm", "Farm", 35, {
	["farming.BroccoliSeed"] = 1,
	["farming.CabbageSeed"] = 1,
	["farming.CarrotSeed"] = 1,
	["farming.PotatoSeed"] = 1,
	["farming.RedRadishSeed"] = 1,
	["farming.StrewberrieSeed"] = 1,
	["farming.TomatoSeed"] = 1,
	["farming.Bacon"] = 2,
	["Base.Milk"] = 5,
	["Base.Butter"] = 1,
	["Base.Fertilizer"] = 3,
	["Base.Egg"] = 15,
	["Base.Chicken"] = 2,
	["Base.ChickenFoot"] = 2,
}, {
	{ perk = Perks.Farming, amount = 70, weighting = 10 },
	{ perk = Perks.Spear, amount = 10, weighting = 1 },
})

if getActivatedMods():contains("SGarden-Homestead") then
	WorkplaceFarm:appendLootTable({
		["Sprout.PearSeeds"] = 1,
		["Sprout.CommonMallowSeeds"] = 1,
		["Sprout.PlantainSeeds"] = 1,
		["Sprout.ComfreySeeds"] = 1,
		["Sprout.GarlicSeeds"] = 1,
		["Sprout.BlackSageSeeds"] = 1,
		["Sprout.AppleSeed"] = 1,
		["Sprout.AvocadoSeed"] = 1,
		["Sprout.BananaSeed"] = 1,
		["Sprout.BellPepperSeed"] = 1,
		["Sprout.BerryBlackSeed"] = 1,
		["Sprout.BerryBlueSeed"] = 1,
		["Sprout.CherrySeed"] = 1,
		["Sprout.CornSeed"] = 1,
		["Sprout.EggplantSeed"] = 1,
		["Sprout.GingerSeed"] = 1,
		["Sprout.GinsengSeed"] = 1,
		["Sprout.GrapefruitSeed"] = 1,
		["Sprout.GrapeSeed"] = 1,
		["Sprout.LeekSeed"] = 1,
		["Sprout.LemongrassSeed"] = 1,
		["Sprout.LemonSeed"] = 1,
		["Sprout.LettuceSeed"] = 1,
		["Sprout.LimeSeed"] = 1,
		["Sprout.MangoSeed"] = 1,
		["Sprout.MushroomSpores"] = 1,
		["Sprout.OliveSeed"] = 1,
		["Sprout.OnionSeed"] = 1,
		["Sprout.OrangeSeed"] = 1,
		["Sprout.PeachSeed"] = 1,
		["Sprout.PineappleSeed"] = 1,
		["Sprout.PumpkinSeed"] = 1,
		["Sprout.SoyBeanSeed"] = 1,
		["Sprout.SugarCaneSeed"] = 1,
		["Sprout.WatermelonSeed"] = 1,
		["Sprout.WheatSeed"] = 1,
		["Sprout.ZucchiniSeed"] = 1,
		["Sprout.RiceSeed"] = 1,
		["Sprout.PepperPlantSeed"] = 1,
		["Sprout.CottonSeed"] = 1,
		["Sprout.HopsSeed"] = 1,
		["Sprout.TeaSeed"] = 1,
		["Sprout.CoffeeSeed"] = 1,
		["Sprout.RubberSeed"] = 1,
	})
end

function WorkplaceFarm:doSpeedyTicks() return true end
