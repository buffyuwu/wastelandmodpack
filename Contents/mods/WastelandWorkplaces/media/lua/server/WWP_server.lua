if not isServer() then return end

local Json = require "json"

--- @type WWP_WorkplaceZone[]
WWP_WorkplaceZones = WWP_WorkplaceZones or {}
local wereZonesLoaded = false

local function loadFromDisk()
    print("Loading WWP from disk")
    wereZonesLoaded = true
    local fileReaderObj = getFileReader("WastelandWorkplaces.json", true)
    local json = ""
    local line = fileReaderObj:readLine()
    while line ~= nil do
        json = json .. line
        line = fileReaderObj:readLine()
    end
    fileReaderObj:close()
    if json and json ~= "" then
        local decoded = Json.Decode(json)
        if decoded then
            WWP_WorkplaceZones = decoded
        end
    end
end

local function writeToDisk()
    local fileWriterObj = getFileWriter("WastelandWorkplaces.json", true, false)
    local json = Json.Encode(WWP_WorkplaceZones)
    fileWriterObj:write(json)
    fileWriterObj:close()
end

local function loadIfNeeded()
    if not wereZonesLoaded then
        loadFromDisk()
    end
end

local function sendZonesToClient(player)
    sendServerCommand(player, "WastelandWorkplaces", "SyncZones", WWP_WorkplaceZones)
end

local function sendZoneToAll(zoneId)
    sendServerCommand("WastelandWorkplaces", "SyncZone", WWP_WorkplaceZones[zoneId])
end

local function sendZonesToAll()
    sendServerCommand("WastelandWorkplaces", "SyncZones", WWP_WorkplaceZones)
end

local Commands = {}

function Commands.SetZone(player, args)
    loadIfNeeded()
    local zoneId = args.id
    WWP_WorkplaceZones[zoneId] = args
    sendZoneToAll(zoneId)
    writeToDisk()
end

function Commands.DeleteZone(player, args)
    loadIfNeeded()
    local zoneId = args.id
    if not zoneId then return end
    WWP_WorkplaceZones[zoneId] = nil
    sendZonesToAll()
    writeToDisk()
end

function Commands.GetZones(player, args)
    loadIfNeeded()
    sendZonesToClient(player)
end

local function processClientCommand(module, command, player, args)
    if module ~= "WastelandWorkplaces" then return end
    if not Commands[command] then return end
    Commands[command](player, args)
end

Events.OnClientCommand.Add(processClientCommand)
