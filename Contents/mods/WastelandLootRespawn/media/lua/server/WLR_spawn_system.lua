WLR_SpawnSystem = {}
WLR_SpawnSystem.instances = {}

local MAX_SQUARE_SIZE = 100

function WLR_SpawnSystem:init(player, params)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o:setup(player, params)
    WLR_SpawnSystem.instances[player] = o
    sendServerCommand(player, "WLR", "Start", {})
    return o
end

function WLR_SpawnSystem:setup(player, params)
    -- config
    self.player = player
    self.centerX = params.centerX
    self.centerY = params.centerY
    self.rangeX = params.rangeX
    self.rangeY = params.rangeY
    self.containerRatio = params.containerRatio
    self.fillRatio = params.fillRatio
    self.checkLimit = params.checkLimit
    self.onlyExplored = params.onlyExplored
    self.allowedCategories = params.allowedCategories
    self.itemsSpawned = {
        {
            "CenterX: " .. tostring(self.centerX),
            "CenterY: " .. tostring(self.centerY),
            "RangeX: " .. tostring(self.rangeX),
            "RangeY: " .. tostring(self.rangeY),
            "ContainerRatio: " .. tostring(self.containerRatio),
            "FillRatio: " .. tostring(self.fillRatio),
            "CheckLimit: " .. tostring(self.checkLimit),
            "OnlyExplored: " .. tostring(self.onlyExplored),
        },
        {},
        {}
    }

    -- setup
    self:calcSpawnSize()
    self:logStarted()

    -- running variables
    self:nextAction("movePlayer", 1)
end

function WLR_SpawnSystem:tick()
    if self.tickTimeout < 0 then
        return
    end

    if self.tickTimeout > 0 then
        self.tickTimeout = self.tickTimeout - 1
        return
    end

    if self.tickTimeout == 0 then
        self.tickTimeout = -1
        if self[self.currentStep] then
            self[self.currentStep](self)
        end
    end
end

-- actions

function WLR_SpawnSystem:nextAction(action, ticks)
    self.currentStep = action
    self.tickTimeout = ticks
end

function WLR_SpawnSystem:movePlayer()
    self:teleportPlayer()
    self.squareCheckTries = 0
    self:nextAction("waitSquaresLoaded", 5)
end

function WLR_SpawnSystem:waitSquaresLoaded()
    if self:areSquaresLoaded() then
        self:nextAction("doRespawnAndQueueNext", 1)
    else
        self.squareCheckTries = self.squareCheckTries + 1
        if self.squareCheckTries > 6 then
            self:nextAction("doRespawnAndQueueNext", 1)
        else
            self:nextAction("waitSquaresLoaded", 2)
        end
    end
end

function WLR_SpawnSystem:doRespawnAndQueueNext()
    self:doRespawn()
    self:updateIndexesToNextSquare()
    self:nextAction("movePlayer", 1)
end

function WLR_SpawnSystem:done()
    WLR_SpawnSystem.instances[self.player] = nil

    sendServerCommand(self.player, "WLR", "Done", {})

    local playerName = self.player:getUsername()
    local writer = getFileWriter("LootRespawn_" .. playerName .. "_" .. tostring(getTimestamp()) .. ".csv", true, false)
    for _, line in ipairs(self.itemsSpawned) do
        local str = table.concat(line, ",")
        writer:writeln(str)
    end
    writer:close()
end

function WLR_SpawnSystem:stop()
    self:done()
end

-- utilities

function WLR_SpawnSystem:doRespawn()
    local containers = self:getContainers()
    for _, container in ipairs(containers) do
        if ZombRand(0, 100) < self.containerRatio then
            self:refillContainer(container)
            self:pruneContainer(container)
            self:logItemsOf(container)
        end
    end
end

function WLR_SpawnSystem:logStarted()
    local logStr =  "Wasteland Loot Respawn Started | " .. self.player:getUsername() ..
                    " [" .. tostring(self.centerX) .. ", " .. tostring(self.centerY) .. "] " ..
                    " (" .. tostring(self.rangeX) .. ", " .. tostring(self.rangeY) .. ") " ..
                    " ContainerRatio: " .. tostring(self.containerRatio) ..
                    " FillRatio: " .. tostring(self.fillRatio) ..
                    " CheckLimit: " .. tostring(self.checkLimit) ..
                    " OnlyExplored: " .. tostring(self.onlyExplored) ..
                    " AllowedCategories: "

    for cat, allowed in pairs(self.allowedCategories) do
        if allowed then
            table.insert(self.itemsSpawned[2], cat)
            logStr = logStr .. " " .. cat
        end
    end
    writeLog("admin", logStr)
end

function WLR_SpawnSystem:logItemsOf(container)
    local items = container:getItems()
    local name = container:getType()
    local x = container:getParent():getX()
    local y = container:getParent():getY()
    local z = container:getParent():getZ()
    local numItems = items:size()
    if items and numItems > 0 then
        for i=0, numItems-1 do
            local item = items:get(i)
            table.insert(self.itemsSpawned, {
                x,
                y,
                z,
                name,
                item:getFullType(),
            })
        end
    end
end

function WLR_SpawnSystem:calcSpawnSize()
    local numChunksX = math.ceil((self.rangeX * 2) / MAX_SQUARE_SIZE)
    local numChunksY = math.ceil((self.rangeY * 2) / MAX_SQUARE_SIZE)
    local sizeChunkX = math.ceil((self.rangeX * 2) / numChunksX)
    local sizeChunkY = math.ceil((self.rangeY * 2) / numChunksY)
    self.topLeftX = self.centerX - self.rangeX
    self.topLeftY = self.centerY - self.rangeY
    self.bottomRightX = self.centerX + self.rangeX
    self.bottomRightY = self.centerY + self.rangeY
    self.chunkSizeX = sizeChunkX
    self.chunkSizeY = sizeChunkY
    self.midPointX = sizeChunkX / 2
    self.midPointY = sizeChunkY / 2
    self.currentXIndex = 0
    self.currentYIndex = 0
    self.maxXIndex = numChunksX - 1
    self.maxYIndex = numChunksY - 1
end

function WLR_SpawnSystem:updateIndexesToNextSquare()
    self.currentXIndex = self.currentXIndex + 1
    if self.currentXIndex > self.maxXIndex then
        self.currentXIndex = 0
        self.currentYIndex = self.currentYIndex + 1
        if self.currentYIndex > self.maxYIndex then
            self:done()
        end
    end
end

function WLR_SpawnSystem:teleportPlayer()
    local x = self.topLeftX + self.currentXIndex * self.chunkSizeX + self.midPointX
    local y = self.topLeftY + self.currentYIndex * self.chunkSizeY + self.midPointY
    sendServerCommand(self.player, "WLR", "Teleport", {
        x=x,
        y=y,
        sizeX=self.chunkSizeX,
        sizeY=self.chunkSizeY,
    })
end

function WLR_SpawnSystem:getRange()
    local sx = self.topLeftX + self.currentXIndex * self.chunkSizeX
    local sy = self.topLeftY + self.currentYIndex * self.chunkSizeY
    local ex = math.min(self.bottomRightX, sx + self.chunkSizeX)
    local ey = math.min(self.bottomRightY, sy + self.chunkSizeY)
    return sx, sy, ex, ey
end

function WLR_SpawnSystem:areSquaresLoaded()
    local sx, sy, ex, ey = self:getRange()
    for x = sx,ex,1 do
        for y = sy,ey,1 do
            if not getSquare(x, y, 0) then
                return false
            end
        end
    end
    return true
end

function WLR_SpawnSystem:getContainers()
    local containers = {}
    local sx, sy, ex, ey = self:getRange()
    for x = sx,ex,1 do
    for y = sy,ey,1 do
    for z = 0, 5, 1 do
        local iSquare = getSquare(x, y, z)
        if z == 0 and not iSquare then print("WLR: Missing Square: " .. tostring(x) .. ", " .. tostring(y) .. ", " .. tostring(z)) end
        if iSquare and
           iSquare:getRoom() and
           iSquare:getRoom():getRoomDef() and
           iSquare:getRoom():getRoomDef():getProceduralSpawnedContainer()
        then
            local objects = iSquare:getObjects()
            for j = 0, objects:size() - 1, 1 do
                local object = objects:get(j)
                local cnt = object:getContainerCount()-1
                for k=0, cnt do
                    local container = object:getContainerByIndex(k)
                    if container and
                       (not self.onlyExplored or container:isExplored()) and
                       not SafeHouse.getSafeHouse(container:getSourceGrid()) and
                       container:getItems():size() <= self.checkLimit
                    then
                        table.insert(containers, container)
                    end
                end
            end
        end
    end end end

    return containers
end

function WLR_SpawnSystem:refillContainer(container)
    container:getSourceGrid():getRoom():getRoomDef():getProceduralSpawnedContainer():clear()
    container:removeItemsFromProcessItems()
    container:clear()
    ItemPicker.fillContainer(container, self.player)
    if container:getParent() then
        ItemPicker.updateOverlaySprite(container:getParent())
    end
    container:setExplored(true)
end

function WLR_SpawnSystem:pruneContainer(container)
    local toRemove = {}
    local items = container:getItems()
    local numItems = items:size()
    if items and numItems > 0 then
        for i=0, numItems-1 do
            local item = items:get(i)
            if not self.allowedCategories[item:getDisplayCategory()] or ZombRand(0, 100) > self.fillRatio then
                table.insert(toRemove, item)
            end
        end
    end
    for _, v in ipairs(items) do
        container:DoRemoveItem(v)
    end
end

function WLR_SpawnSystem.onTick()
    for _, instance in pairs(WLR_SpawnSystem.instances) do
        instance:tick()
    end
end

Events.OnTick.Add(WLR_SpawnSystem.onTick)