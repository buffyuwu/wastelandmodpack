require "WLR_gui"

local WLR_menu = {}

WLR_menu.doMenu = function(player, context)
  if not isClient() or isAdmin() then
    context:addOption("Loot Respawner", player, WLR_menu.onRefillGUI)
  end
end

function WLR_menu.onRefillGUI(player)
    local modal = WLR_gui:new()
    modal:initialise()
end

Events.OnFillWorldObjectContextMenu.Add(WLR_menu.doMenu)