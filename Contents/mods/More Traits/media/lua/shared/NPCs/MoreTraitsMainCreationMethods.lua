local addTrait = ProfessionFramework.addTrait

addTrait("natural", {
    name = "UI_trait_natural",
    description = "UI_trait_naturaldesc",
    cost = 4,
    xp = {
        [Perks.Cooking] = 1,
        [Perks.PlantScavenging] = 1,
    },
})

addTrait("bladetwirl", {
    name = "UI_trait_bladetwirl",
    description = "UI_trait_bladetwirldesc",
    cost = 5,
    xp = {
        [Perks.LongBlade] = 1,
        [Perks.SmallBlade] = 1,
    },
})

addTrait("blunttwirl", {
    name = "UI_trait_blunttwirl",
    description = "UI_trait_blunttwirldesc",
    cost = 5,
    xp = {
        [Perks.Blunt] = 1,
        [Perks.SmallBlunt] = 1,
    },
})

addTrait("anemic", {
    name = "UI_trait_anemic",
    description = "UI_trait_anemicdesc",
    cost = -4,
})

addTrait("blissful", {
    name = "UI_trait_blissful",
    description = "UI_trait_blissfuldesc",
    cost = 4,
	exclude = { "depressive", "depressive2", "selfdestructive", "selfdestructive2", "Smoker" },
})

-- addTrait("broke", {
--     name = "UI_trait_broke",
--     description = "UI_trait_brokedesc",
--     cost = 0,
-- })

-- addTrait("burned", {
--     name = "UI_trait_burned",
--     description = "UI_trait_burneddesc",
--     cost = 0,
-- 	exclude = {"broke", "injured" },
-- })

-- addTrait("injured", {
--     name = "UI_trait_injured",
--     description = "UI_trait_injureddesc",
--     cost = 0,
-- })

addTrait("butterfingers", {
    name = "UI_trait_butterfingers",
    description = "UI_trait_butterfingersdesc",
    cost = -10,
})

addTrait("depressive", {
    name = "UI_trait_depressive",
    description = "UI_trait_depressivedesc",
    cost = -2,
	exclude = { "depressive2" }
})

addTrait("depressive2", {
    name = "UI_trait_depressive",
    description = "UI_trait_depressivedesc",
    profession = true,
})

addTrait("immunocompromised", {
    name = "UI_trait_immunocompromised",
    description = "UI_trait_immunocompromiseddesc",
    cost = -2,
	exclude = { "Resilient" },
})

-- addTrait("paranoia", {
--     name = "UI_trait_paranoia",
--     description = "UI_trait_paranoiadesc",
--     cost = -4,
-- 	exclude = { "paranoia2" }
-- })

-- addTrait("paranoia2", {
--     name = "UI_trait_paranoia",
--     description = "UI_trait_paranoiadesc",
--     profession = true,
-- })

addTrait("selfdestructive", {
    name = "UI_trait_selfdestructive",
    description = "UI_trait_selfdestructivedesc",
    cost = -4,
	exclude = { "selfdestructive2" }
})

addTrait("selfdestructive2", {
    name = "UI_trait_selfdestructive",
    description = "UI_trait_selfdestructivedesc",
    profession = true,
})

addTrait("superimmune", {
    name = "UI_trait_superimmune",
    description = "UI_trait_superimmunedesc",
    cost = 8,
	exclude = { "Resilient", "immunocompromised", "ProneToIllness" },
})

addTrait("thickblood", {
    name = "UI_trait_thickblood",
    description = "UI_trait_thickblooddesc",
    cost = 4,
	exclude = { "anemic" },
})

addTrait("noodlelegs", {
    name = "UI_trait_noodlelegs",
    description = "UI_trait_noodlelegsdesc",
    cost = -6,
})